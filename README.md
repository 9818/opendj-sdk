# opendj-sdk

#### 介绍

由于京东到家没有提供专用的SDK,开发难度较大,京东到家还不定时的出现加载异常,请重试的错误,真的很头疼~  
这是一个非官方京东到家开放平台sdk,一个提供自动重试,限流,封装了请求对象和响应对象的京东到家开放平台的工具包,大大提供了开发效率

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2020/0918/193230_8a7f1c98_557705.png "京东到家SDK说明.png")



#### 安装教程

1.  下载源码
2.  使用mvn package
3.  将打包好的jar上传到自己的本地仓库,或者云上仓库
4.  项目中引入jar,即可获得完整功能

#### 使用说明
例子:  
* 获取订单:  
使用默认的client请求:
```
@Test
public void test_query_order_request(){
    final DefaultOpenDjClient defaultOpenDjClient = new DefaultOpenDjClient("appKey", "secret", "token");
    final OrderInfoRequest orderInfoRequest = new OrderInfoRequest();
    orderInfoRequest.setOrderId(123456L);
    final OrderInfoListResponse execute = defaultOpenDjClient.execute(orderInfoRequest);
    final OrderInfoListResponse.OrderInfoListData orderInfoListData = execute.getOrderInfoListData();
    final List<OrderInfoListResponse.OrderInfoDTO> orderInfoDTOS = orderInfoListData.extractListData();
    System.out.println(orderInfoDTOS);
}
```
使用可自动重试的client请求:
```
@Test
public void test_query_order_request(){
    final AutoRetryOpenDjClient autoRetryOpenDjClient= new AutoRetryOpenDjClient("appKey", "secret", "token");
    final OrderInfoRequest orderInfoRequest = new OrderInfoRequest();
    orderInfoRequest.setOrderId(123456L);
    final OrderInfoListResponse execute = autoRetryOpenDjClient.execute(orderInfoRequest);
    final OrderInfoListResponse.OrderInfoListData orderInfoListData = execute.getOrderInfoListData();
    final List<OrderInfoListResponse.OrderInfoDTO> orderInfoDTOS = orderInfoListData.extractListData();
    System.out.println(orderInfoDTOS);
}
```
可以发现只是换了一个client对象,就可以实现自动重试,

#### 参考文献
* 淘宝SDK
* 京东到家开放平台-开发文档

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
