package com.jd.opendj.api.exception;

/**
 * @author by DFF
 * @date 8/18/2020 11:10 PM
 */
public class LimitedException extends FailedRequestException {

    public LimitedException(String message) {
        super(message);
    }

    public LimitedException(String message, Throwable cause) {
        super(message, cause);
    }
}
