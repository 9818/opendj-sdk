package com.jd.opendj.api.exception;

/**
 * @author by DFF
 * @date 8/18/2020 11:10 PM
 */
public class RetryException extends FailedRequestException {

    public RetryException(String message) {
        super(message);
    }

    public RetryException(String message, Throwable cause) {
        super(message, cause);
    }
}
