package com.jd.opendj.api.exception;

/**
 * @author by DFF
 * @date 8/18/2020 11:10 PM
 */
public class FailedRequestException extends RuntimeException {

    public FailedRequestException(String message) {
        super(message);
    }

    public FailedRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
