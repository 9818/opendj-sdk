package com.jd.opendj.api;




import com.google.gson.Gson;
import org.apache.commons.lang3.BooleanUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 邓峰峰
 * @date 2019/11/12 16:37
 * <p>京东到家响应</p>
 */


public abstract class OpenDjResponse {

    private static final Pattern pattern = Pattern.compile("(?<=UUID\\[).*?(?=])");


    protected String code;
    protected String msg;
    protected String data;
    protected Boolean success;

    public abstract void toJavaObject();


    @Override
    public String toString() {
        return "OpenDjResponse{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data='" + data + '\'' +
                ", success=" + success +
                '}';
    }

    /**
     * 获取msg中的uuid
     * @return
     */
    public String getUuid(){
        final Matcher matcher = pattern.matcher(msg);
        if(matcher.find()){
            return matcher.group();
        }
        return "";
    }

    public boolean ok() {
        return BooleanUtils.isTrue(success);
    }

    public boolean isSuccess() {
        return success ;
    }

    public boolean isFail(){
        return !"0".equals(code);
    }


    public String toJsonString(){
        final Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }



}
