package com.jd.opendj.api;

/**
 * <p>OpenDjRequest</p>
 * description
 *
 * @author 邓峰峰
 * @date 9/2/2020 7:59 PM
 */
public interface OpenDjRequest<T extends OpenDjResponse> extends AbstractParam {

    /**
     * api名称
     * @return
     */
    OpenDjApi api();


    /**
     * 获取具体响应实现类的定义。
     */
    Class<T> getResponseClass();

}
