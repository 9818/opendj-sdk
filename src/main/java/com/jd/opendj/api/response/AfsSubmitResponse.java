package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class AfsSubmitResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.afsSubmitData = gson.fromJson(data, AfsSubmitData.class);
    }

    private AfsSubmitData afsSubmitData;

    public AfsSubmitData getAfsSubmitData() {
        return afsSubmitData;
    }

    public void setAfsSubmitData(AfsSubmitData afsSubmitData) {
        this.afsSubmitData = afsSubmitData;
    }

    public static class AfsSubmitData implements Data<VenderAfsSubmitResponse> {
        /**
         * 示例值:1021
         * 描述:1001-参数不合法！；1002-业务异常！；1021-无法查询到订单！；0-成功；-1-失败；-3-系统错误；1022-非商超，美食，开放仓订单，不允许商家申请售后;1023-只支持已完成订单进行售后申请;1024-订单信息参数不合法;1025-该订单过期不支持售后(超过订单完成48小时);1026-该订单没有可申请售后的商品；1027-该订单不支持申请售后
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:测试文案
         * 描述:返回文案
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:返回数据
         */
        private VenderAfsSubmitResponse result;

        public VenderAfsSubmitResponse getResult() {
            return result;
        }

        public void setResult(VenderAfsSubmitResponse result) {
            this.result = result;
        }

		@Override
		public VenderAfsSubmitResponse extractData() {
			return result;
		}

		@Override
		public boolean isSuccess() {
			return "0".equals(code);
		}
	}

    public static class VenderAfsSubmitResponse {
    	private String serviceOrder;

		public String getServiceOrder() {
			return serviceOrder;
		}

		public void setServiceOrder(String serviceOrder) {
			this.serviceOrder = serviceOrder;
		}
	}
} 
