package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.util.List;

/**
 * @author dff
 * @date 2020-09-17
 */
public class HandleReportRecordResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.handleReportRecordData = gson.fromJson(data, HandleReportRecordData.class);
    }

    private HandleReportRecordData handleReportRecordData;

    public HandleReportRecordData getHandleReportRecordData() {
        return handleReportRecordData;
    }

    public void setHandleReportRecordData(HandleReportRecordData handleReportRecordData) {
        this.handleReportRecordData = handleReportRecordData;
    }

    public static class HandleReportRecordData implements ListData<ExceptionReportRecordDTO> {
        /**
         * 示例值:0
         * 描述:1：参数错误，4001003：越权操作订单，0：成功，-1:失败，-3：系统错误
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:
         * 描述:msg
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:返回的实体类
         */
        private List<ExceptionReportRecordDTO> result;

        public List<ExceptionReportRecordDTO> getResult() {
            return result;
        }

        public void setResult(List<ExceptionReportRecordDTO> result) {
            this.result = result;
        }

        @Override
        public List<ExceptionReportRecordDTO> extractListData() {
            return result;
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }

    public static class ExceptionReportRecordDTO {
        /**
         * 运单状态
         */
        private String deliveryOrderStatus;

        /**
         * 	取消原因
         */
        private String reasonName;

        /**
         * 运单号
         */
        private Long deliveryOrderId;

        /**
         * 配送员姓名
         */
        private String transporterName;

        /**
         * 配送员电话
         */
        private String transporterPhone;

        /**
         * 商家上传图片列表
         */
        private List<String> extra;

        public String getDeliveryOrderStatus() {
            return deliveryOrderStatus;
        }

        public void setDeliveryOrderStatus(String deliveryOrderStatus) {
            this.deliveryOrderStatus = deliveryOrderStatus;
        }

        public String getReasonName() {
            return reasonName;
        }

        public void setReasonName(String reasonName) {
            this.reasonName = reasonName;
        }

        public Long getDeliveryOrderId() {
            return deliveryOrderId;
        }

        public void setDeliveryOrderId(Long deliveryOrderId) {
            this.deliveryOrderId = deliveryOrderId;
        }

        public String getTransporterName() {
            return transporterName;
        }

        public void setTransporterName(String transporterName) {
            this.transporterName = transporterName;
        }

        public String getTransporterPhone() {
            return transporterPhone;
        }

        public void setTransporterPhone(String transporterPhone) {
            this.transporterPhone = transporterPhone;
        }

        public List<String> getExtra() {
            return extra;
        }

        public void setExtra(List<String> extra) {
            this.extra = extra;
        }
    }
} 
