package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>BillData</p>
 * description
 *
 * @author 邓峰峰
 * @date 2019/12/16 14:46
 */

public class CheckBillResponse extends OpenDjResponse {

    private BillListData billListData;

    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.billListData = gson.fromJson(data, BillListData.class);
    }

    public BillListData getBillListData() {
        return billListData;
    }

    public void setBillListData(BillListData billListData) {
        this.billListData = billListData;
    }

    public static class BillListData  implements ListData<BillSearchResult>{
       private Integer code;
       private String msg;
       private Long totalCount;
       private List<BillSearchResult> result;

       public Integer getCode() {
           return code;
       }

       public void setCode(Integer code) {
           this.code = code;
       }

       public String getMsg() {
           return msg;
       }

       public void setMsg(String msg) {
           this.msg = msg;
       }

       public Long getTotalCount() {
           return totalCount;
       }

       public void setTotalCount(Long totalCount) {
           this.totalCount = totalCount;
       }

       public List<BillSearchResult> getResult() {
           return result;
       }

       public void setResult(List<BillSearchResult> result) {
           this.result = result;
       }

       @Override
       public List<BillSearchResult> extractListData() {
           return result;
       }

       @Override
       public boolean isSuccess() {
           return code!=null && code ==0;
       }


   }



    /**
     * @author dengfengfeng
     * @date 2019-11-13
     */

    public static class BillSearchResult {
        /**
         *示例值:2278
         *描述:订单号
         */
        private Long orderId ;
        /**
         *示例值:0
         *描述:订单货款结算状态0未计费、1计费未结算、2已结算
         */
        private Integer googsSettlementStatus ;
        /**
         *示例值:100
         *描述:货款(商品实际支付金额)
         */
        private Long goodsBill ;
        /**
         *示例值:-20
         *描述:货款佣金=(用户支付的sku的实际金额+到家承担的市场费+餐盒费)*扣点
         */
        private Long goodsCommissionBill ;
        /**
         *示例值:10
         *描述:运费=商家自配送费-商家支付小费-商家支付远距离运费-商家承担运费补贴
         */
        private Long freightBill ;
        /**
         *示例值:-3
         *描述:运费佣金（如果是达达配送则没有运费佣金，否则是运费（不含取件服务费）*扣点）
         */
        private Long freightCommissionBill ;
        /**
         *示例值:-3
         *描述:保底佣金补差（单位：元）
         */
        private BigDecimal diffLessCommision ;
        /**
         *示例值:1
         *描述:餐盒费
         */
        private Long packageBill ;
        /**
         *示例值:0
         *描述:市场费结算状态0未计费、1计费未结算、2已结算,3无市场费
         */
        private Integer marketSettlementStatus ;
        /**
         *示例值:1
         *描述:市场费
         */
        private Long marketBill ;
        /**
         *示例值:
         *描述:营销服务费用
         */
        private Long marketingServiceFee ;
        /**
         *示例值:
         *描述:营销服务费用状态
         */
        private Integer marketingServiceFeeStatus ;
        /**
         *示例值:
         *描述:对账单类型，5006货款对账单5008市场费对账单
         */
        private Integer billOrderType ;



        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Integer getGoogsSettlementStatus() {
            return googsSettlementStatus;
        }

        public void setGoogsSettlementStatus(Integer googsSettlementStatus) {
            this.googsSettlementStatus = googsSettlementStatus;
        }

        public Long getGoodsBill() {
            return goodsBill;
        }

        public void setGoodsBill(Long goodsBill) {
            this.goodsBill = goodsBill;
        }

        public Long getGoodsCommissionBill() {
            return goodsCommissionBill;
        }

        public void setGoodsCommissionBill(Long goodsCommissionBill) {
            this.goodsCommissionBill = goodsCommissionBill;
        }

        public Long getFreightBill() {
            return freightBill;
        }

        public void setFreightBill(Long freightBill) {
            this.freightBill = freightBill;
        }

        public Long getFreightCommissionBill() {
            return freightCommissionBill;
        }

        public void setFreightCommissionBill(Long freightCommissionBill) {
            this.freightCommissionBill = freightCommissionBill;
        }

        public BigDecimal getDiffLessCommision() {
            return diffLessCommision;
        }

        public void setDiffLessCommision(BigDecimal diffLessCommision) {
            this.diffLessCommision = diffLessCommision;
        }

        public Long getPackageBill() {
            return packageBill;
        }

        public void setPackageBill(Long packageBill) {
            this.packageBill = packageBill;
        }

        public Integer getMarketSettlementStatus() {
            return marketSettlementStatus;
        }

        public void setMarketSettlementStatus(Integer marketSettlementStatus) {
            this.marketSettlementStatus = marketSettlementStatus;
        }

        public Long getMarketBill() {
            return marketBill;
        }

        public void setMarketBill(Long marketBill) {
            this.marketBill = marketBill;
        }

        public Long getMarketingServiceFee() {
            return marketingServiceFee;
        }

        public void setMarketingServiceFee(Long marketingServiceFee) {
            this.marketingServiceFee = marketingServiceFee;
        }

        public Integer getMarketingServiceFeeStatus() {
            return marketingServiceFeeStatus;
        }

        public void setMarketingServiceFeeStatus(Integer marketingServiceFeeStatus) {
            this.marketingServiceFeeStatus = marketingServiceFeeStatus;
        }

        public Integer getBillOrderType() {
            return billOrderType;
        }

        public void setBillOrderType(Integer billOrderType) {
            this.billOrderType = billOrderType;
        }
    }

}
