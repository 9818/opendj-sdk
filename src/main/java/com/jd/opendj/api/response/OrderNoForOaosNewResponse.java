package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.util.List;

/** 
 * @author dengfengfeng
 * @date 2019-11-13
 * @apiNote
 * 查询订单的各流程节点状态变化日志。
 * 订单标签编码与标签名称( 注：sonTagList只会用到70,75,85 )
 * tagCode	codeName	msgContent
 * 0	物流信息	您的订单已交由${thirdName}配送，运单号：${shipId}
 * 1	订单提交成功	下单成功，订单号:${orderId}
 * 2	需求单提交成功	下单成功，需求单号:${orderId}
 * 5	待支付	请尽快支付该订单，超时将会自动取消订单
 * 10	已支付	订单支付成功
 * 20	未到服务时间	订单未到服务时间，请耐心等待
 * 29	药师审核处方中	药师将在15分钟内完成处方审核
 * 30	等待商家接单	订单已通知商家，商家正在处理中
 * 31	等待商家接单	商家15分钟内不接单，系统将为您自动取消
 * 32	等待商家接单	30分钟商家未接单，订单将自动取消
 * 33	已通知门店	订单已到达门店，请尽快处理
 * 34	处方审核通过	商家已接单，开始拣货（准备商品）
 * 35	商家已接单	商家已确认订单，正在加急为您准备
 * 36	预约成功	商家已确认您的预约服务订单
 * 37	商家已接单	正在为您准备商品
 * 38	商家已接单	开始为您拣货
 * 39	开始拣货	该订单由拣货员(${pickerName },编号：${ pickerPin })为您拣货
 * 40	拣货完成	商家已完成拣货，等待配送
 * 41	商品准备完成	您的商品已准备完成，等待配送
 * 42	需求调配成功	商家已完成拣货，等待配送
 * 45	上门服务中	正在上门服务中
 * 47	承运商变更	暂时无配送员，将由商家为您配送
 * 48	预计送达时间变更	您的预约时间已调整为${day} ${startTime} — ${endTime}
 * 49	商家加小费	商家为该订单加小费¥${tips}元
 * 50	配送中	请您保持电话畅通，我们会尽快送达
 * 51	配送中	请您保持电话畅通，我们会尽快送达
 * 52	配送中	商品在配送中
 * 55	服务完成	服务已结束
 * 60	待抢单	订单已通知达达,配送员将于${inputTime}开始抢单
 * 63	待抢单	用户为该订单追加¥${tips}元小费，已通知达达配送员尽快接单
 * 64	已催单	用户催单，达达系统自动为该订单加¥${tips}元调度费加急配送
 * 65	召唤配送失败	商家召唤同城送配送员失败。失败原因：${failReason}
 * 70	配送员已抢单	配送员已抢单，正在前往商家，配送员：${deliveryManName}，电话：${deliveryManMobile}，#类型：${type}
 * 73	更换配送员	您的配送员已经更换为：${deliveryManName}，电话：${deliveryManMobile}，#类型：${type}
 * 75	取消抢单	配送员取消抢单，等待重新抢单
 * 78	订单已打印	订单已打印，商家正在拣货
 * 79	已到店	配送员已到店，配送员：${deliveryManName}，电话：${deliveryManMobile}
 * 80	配送员已取货	配送员已取货，正在向您奔去，配送员：${deliveryManName}，电话：${deliveryManMobile}，#类型：${type}
 * 81	待审核	达达配送员申请取消配送（原因：${failReason}），请商家及时审核。
 * 82	继续配送	商家已拒绝达达的取消配送申请，请等待达达配送员到店取货。
 * 85	取货失败	配送员取货失败，等待系统确认。取货失败原因：${failReason}
 * 90	订单已完成	订单配送完成
 * 91	需求已完成	订单配送完成
 * 100	订单商品调整	订单修改成功
 * 101	需求单调整	订单修改成功
 * 104	需求取消申请审核中	用户申请取消订单，等待商家审核，超过15分钟未审核将自动取消订单。
 * 105	订单取消申请审核中	商家将在15分钟内处理您的订单取消申请，请耐心等待。
 * 106	订单取消成功	商家已同意您的取消订单申请
 * 107	订单取消失败	您的订单取消申请被商家驳回，该订单将会正常运转。驳回原因：${venderProcessRemark}
 * 108	需求取消失败	商家拒绝了客户的取消申请（原因：${venderProcessRemark}）
 * 110	订单已锁定	订单已锁定，请等待系统审核。
 * 111	需求已锁定	商家拒绝了客户的取消申请（原因：${venderProcessRemark}）
 * 115	超时未支付取消	订单超时未支付，已自动取消
 * 119	需求已取消	您的订单已取消成功。
 * 120	订单已取消	您的订单已取消成功。
 * 121	超时未提货取消	订单超时未提货，已自动取消。
 * 124	需求已取消	${cancelReason}
 * 125	订单已取消	${cancelReason}
 * 130	取消失败	订单取消失败，订单继续流转
 * 140	投递失败	订单投递失败，失败原因：${failReason}
 * 141	投递失败	订单投递失败，失败原因：${failReason}
 * 145	退款中	您的退款${refundAmount}元预计15个工作日内退回至您的支付账号，请耐心等待。
 * 146	退款中	订单中删除商品的退款${refundAmount}元（不包括优惠部分），预计15个工作日内退回至您的支付账号，请耐心等待。
 * 150	已退款	您的退款请求已成功提交到银行，预计1-15个工作日到账，请注意查收
 * 151	退款到账	订单中删除商品的退款${refundAmount}元（不包括优惠部分）已退回至您的支付账户，请查收。
 * 152	退款到账	您的退款${refundAmount}元已退回至您的支付账户，请查收。
 * 153	退款到账	退款${refundAmount}元（不包括优惠部分）已退回至用户的支付账户
 * 160	退款异常	退款将会在3个工作日内以手动汇款的方式汇入您的账户中
 * 165	订单支持超时赔付	超过${payOutTime}送达可享受${payOutMoney}元超时优惠券赔付
 * 170	超时赔付优惠券	很抱歉订单未能按时为您送达，现赔付${totalMoney}元超时优惠券，感谢您的耐心等待
 * 171	沿用出现情况时间点的节点(超时赔付优惠券)	订单未能按时送达，现赔付${totalMoney}元超时优惠券给用户
 * 180	等待到店自提	商家已完成拣货，请于${selfPickEndTime}前到门店内提货，超时未提货订单将自动取消并退款
 * 190	催单回复	配送员回复了您的催单：${feedbackContent}
 * 195	订单奖励鲜豆	恭喜您获得${point}鲜豆
 */

public class OrderNoForOaosNewResponse extends OpenDjResponse {

	private QueryOrderTrackData queryOrderTrackData;

	public QueryOrderTrackData getQueryOrderTrackData() {
		return queryOrderTrackData;
	}

	public void setQueryOrderTrackData(QueryOrderTrackData queryOrderTrackData) {
		this.queryOrderTrackData = queryOrderTrackData;
	}

	@Override
	public void toJavaObject() {
		Gson gson = new Gson();
		this.queryOrderTrackData = gson.fromJson(data, QueryOrderTrackData.class);
	}

	public static class QueryOrderTrackData implements Data<QueryOrderTrackResult>{
		/**
		 * 状态码(0-成功; 3011094-未知错误;3011093-系统错误;3011092-警告;3011091-失败;3011001-参数错误;3011002-方法不存在;3011003-用户未登录;3011004-参数%s不存在;3011005-参数%s不可用;3011006-重复记录;3011007-记录被引用;3011009-非法的操作;3011010-重复提交;3011011-无此操作权限;3011012-IP已变更;3011013-session失效)
		 */
		private String code;
		private String msg;
		private String detail;
		private QueryOrderTrackResult result;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public String getDetail() {
			return detail;
		}

		public void setDetail(String detail) {
			this.detail = detail;
		}

		public QueryOrderTrackResult getResult() {
			return result;
		}

		public void setResult(QueryOrderTrackResult result) {
			this.result = result;
		}

		@Override
		public QueryOrderTrackResult extractData() {
			return result;
		}

		@Override
		public boolean isSuccess() {
			return "0".equals(code);
		}
	}

	public static class QueryOrderTrackResult{
		/**
		 *示例值:110001002033234
		 *描述:订单Id
		 */
		private String orderId;
		/**
		 *示例值:
		 *描述:订单跟踪列表
		 */
		private List<OrderTrackDTO> orderTrackList;

		public String getOrderId() {
			return orderId;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public List<OrderTrackDTO> getOrderTrackList() {
			return orderTrackList;
		}

		public void setOrderTrackList(List<OrderTrackDTO> orderTrackList) {
			this.orderTrackList = orderTrackList;
		}
	}




	/**
	 * @author dengfengfeng
	 * @date 2019-11-13
	 */

	public static class SonTagDTO {
		/**
		 *示例值:2016-08-2412:49:22
		 *描述:mq消息处理时间
		 */
		private String mqProcessTime;
		/**
		 *示例值:2016-08-2412:49:22
		 *描述:操作时间
		 */
		private String operTime;
		/**
		 *示例值:1
		 *描述:标签编码
		 */
		private Integer tagCode;
		/**
		 *示例值:订单提交成功
		 *描述:标签名称
		 */
		private String codeName;
		/**
		 *示例值:JD_38at20d99872a
		 *描述:操作人账号
		 */
		private String operPin;
		/**
		 *示例值:陈
		 *描述:操作人名称
		 */
		private String operName;
		/**
		 *示例值:下单成功，订单号:620436563000121
		 *描述:消息内容
		 */
		private String msgContent;

		public String getMqProcessTime() {
			return mqProcessTime;
		}

		public void setMqProcessTime(String mqProcessTime) {
			this.mqProcessTime = mqProcessTime;
		}

		public String getOperTime() {
			return operTime;
		}

		public void setOperTime(String operTime) {
			this.operTime = operTime;
		}

		public Integer getTagCode() {
			return tagCode;
		}

		public void setTagCode(Integer tagCode) {
			this.tagCode = tagCode;
		}

		public String getCodeName() {
			return codeName;
		}

		public void setCodeName(String codeName) {
			this.codeName = codeName;
		}

		public String getOperPin() {
			return operPin;
		}

		public void setOperPin(String operPin) {
			this.operPin = operPin;
		}

		public String getOperName() {
			return operName;
		}

		public void setOperName(String operName) {
			this.operName = operName;
		}

		public String getMsgContent() {
			return msgContent;
		}

		public void setMsgContent(String msgContent) {
			this.msgContent = msgContent;
		}
	}

	/**
	 * @author dengfengfeng
	 * @date 2019-11-13
	 */

	public static class OrderTrackDTO {
		/**
		 * 示例值:2016-08-2412:49:22
		 * 描述:mq消息处理时间
		 */
		private String mqProcessTime;
		/**
		 * 示例值:2016-08-2412:49:22
		 * 描述:操作时间
		 */
		private String operTime;
		/**
		 * 示例值:1
		 * 描述:标签编码
		 */
		private Integer tagCode;
		/**
		 * 示例值:订单提交成功
		 * 描述:标签名称
		 */
		private String codeName;
		/**
		 * 示例值:JD_38at20d99872a
		 * 描述:操作人账号
		 */
		private String operPin;
		/**
		 * 示例值:陈
		 * 描述:操作人名称
		 */
		private String operName;
		/**
		 * 示例值:下单成功，订单号:620436563000121
		 * 描述:消息内容
		 */
		private String msgContent;
		/**
		 * 示例值:开放平台
		 * 描述:操作来源
		 */
		private String operFrom;
		/**
		 * 示例值:
		 * 描述:子节点列表
		 */
		private List<SonTagDTO> sonTagList;

		public String getMqProcessTime() {
			return mqProcessTime;
		}

		public void setMqProcessTime(String mqProcessTime) {
			this.mqProcessTime = mqProcessTime;
		}

		public String getOperTime() {
			return operTime;
		}

		public void setOperTime(String operTime) {
			this.operTime = operTime;
		}

		public Integer getTagCode() {
			return tagCode;
		}

		public void setTagCode(Integer tagCode) {
			this.tagCode = tagCode;
		}

		public String getCodeName() {
			return codeName;
		}

		public void setCodeName(String codeName) {
			this.codeName = codeName;
		}

		public String getOperPin() {
			return operPin;
		}

		public void setOperPin(String operPin) {
			this.operPin = operPin;
		}

		public String getOperName() {
			return operName;
		}

		public void setOperName(String operName) {
			this.operName = operName;
		}

		public String getMsgContent() {
			return msgContent;
		}

		public void setMsgContent(String msgContent) {
			this.msgContent = msgContent;
		}

		public String getOperFrom() {
			return operFrom;
		}

		public void setOperFrom(String operFrom) {
			this.operFrom = operFrom;
		}

		public List<SonTagDTO> getSonTagList() {
			return sonTagList;
		}

		public void setSonTagList(List<SonTagDTO> sonTagList) {
			this.sonTagList = sonTagList;
		}
	}

}
