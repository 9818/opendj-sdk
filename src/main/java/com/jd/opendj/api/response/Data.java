package com.jd.opendj.api.response;

/**
 * <p>Data</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/1/19 16:23
 */
public interface Data<T> {

    /**
     * 提取数据
     * @return 提取出来的业务数据
     */
    T extractData();

    /**
     * 是否请求成功
     * @return 是否成功
     */
    boolean isSuccess();
}
