package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class CheckSelfPickCodeResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.checkSelfPickCodeData = gson.fromJson(data, CheckSelfPickCodeData.class);
    }

    private CheckSelfPickCodeData checkSelfPickCodeData;

    public CheckSelfPickCodeData getCheckSelfPickCodeData() {
        return checkSelfPickCodeData;
    }

    public void setCheckSelfPickCodeData(CheckSelfPickCodeData checkSelfPickCodeData) {
        this.checkSelfPickCodeData = checkSelfPickCodeData;
    }

    public static class CheckSelfPickCodeData implements Data<Void>{
        /**
         * 示例值:0
         * 描述:返回码（-1:失败;0:成功;1:参数错误;9:订单已取消，自提码校验失败；10:订单已完成，自提码校验失败）
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:调用失败，msg,detail返回错误信息
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:调用失败，msg,detail返回错误信息
         */
        private String detail;

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
