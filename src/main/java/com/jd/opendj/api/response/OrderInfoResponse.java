package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.opendj.api.OpenDjResponse;
import com.jd.opendj.api.exception.FailedRequestException;
import com.jd.opendj.api.response.adapter.DateAdapter;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * <p>OrderInfoData</p>
 * description
 *
 * @author 邓峰峰
 * @date 2019/12/16 14:45
 */

public class OrderInfoResponse extends OpenDjResponse {


    private OrderInfoListData orderInfoListData;

    /**
     * 拿到第一条数据
     * @return
     */
    public OrderInfoDTO findFirst(){
        return orderInfoListData.extractData().getResultList().get(0);
    }

    public OrderInfoListData getOrderInfoListData() {
        return orderInfoListData;
    }

    public void setOrderInfoListData(OrderInfoListData orderInfoListData) {
        this.orderInfoListData = orderInfoListData;
    }

    @Override
    public void toJavaObject() {
        final Gson gson = new Gson();
        this.orderInfoListData = gson.fromJson(data, OrderInfoListData.class);
    }

    /**
     * @author dengfengfeng
     * @date 2020-01-07
     */

    public static class PrescriptionDTO {
        /**
         *示例值:张三
         *描述:用药人姓名
         */
        private String useDrugName ;
        /**
         *示例值:1
         *描述:0男1女
         */
        private Integer sex ;
        /**
         *示例值:123456789012345678
         *描述:身份证号码
         */
        private String identityNumber ;
        /**
         *示例值:1989-02-05
         *描述:出生日期
         */
        private String birthday ;
        /**
         *示例值:123456789012345678
         *描述:电话号码
         */
        private String phoneNumber ;
        /**
         *示例值:jfs/607/1.jpg,jfs/607/2.jpg
         *描述:用户复诊凭证等信息（非可信赖的处方信息，互联网处方信息请用specialServiceTag字段），图片大小不固定，商家如对图片大小有特殊要求，可根据需求自行拼装图片地址前缀，获取固定尺寸图片（拼装前缀统一为：https://img30.360buyimg.com/myorderpdj/s20x250_，其中20x250为图片大小，商家可自行调节，注意前缀最后的下划线）；如对图片无特殊要求，直接根据picUrlList获取图片即可。当有多张图片时，Url逗号隔开。
         */
        private String picUrl ;
        /**
         *示例值:
         *描述:用户上传图片，固定大小960*1280像素
         */
        private List<String> picUrlList ;

        public String getUseDrugName() {
            return useDrugName;
        }

        public void setUseDrugName(String useDrugName) {
            this.useDrugName = useDrugName;
        }

        public Integer getSex() {
            return sex;
        }

        public void setSex(Integer sex) {
            this.sex = sex;
        }

        public String getIdentityNumber() {
            return identityNumber;
        }

        public void setIdentityNumber(String identityNumber) {
            this.identityNumber = identityNumber;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public List<String> getPicUrlList() {
            return picUrlList;
        }

        public void setPicUrlList(List<String> picUrlList) {
            this.picUrlList = picUrlList;
        }

        @Override
        public String toString() {
            return "PrescriptionDTO{" +
                    "useDrugName='" + useDrugName + '\'' +
                    ", sex=" + sex +
                    ", identityNumber='" + identityNumber + '\'' +
                    ", birthday='" + birthday + '\'' +
                    ", phoneNumber='" + phoneNumber + '\'' +
                    ", picUrl='" + picUrl + '\'' +
                    ", picUrlList=" + picUrlList +
                    '}';
        }
    }

    /**
     * @author dengfengfeng
     * @date 2020-01-07
     */

    public static class OrderProductDTO {
        /**
         *示例值:100001036354906
         *描述:订单号
         */
        private Long orderId ;
        /**
         *示例值:深蓝色;XS
         *描述:商品规格，多规格之间用英文分号;分隔
         */
        private String skuCostumeProperty ;
        /**
         *示例值:1000010248591821
         *描述:调整单记录id（0:原单商品明细;非0:调整单id或者确认单id)
         */
        private Long adjustId ;
        /**
         *示例值:2002445142
         *描述:到家商品编码
         */
        private Long skuId ;
        /**
         *示例值:Yakult/养乐多活菌型乳酸菌乳饮品100ml*5瓶
         *描述:商品的名称
         */
        private String skuName ;
        /**
         *示例值:SD10002
         *描述:商家商品编码
         */
        private String skuIdIsv ;
        /**
         *示例值:600
         *描述:到家商品销售价
         */
        private Long skuJdPrice ;
        /**
         *示例值:1
         *描述:下单数量
         */
        private Integer skuCount ;
        /**
         *示例值:0
         *描述:调整方式(0:默认值，没调整，原订单明细;1:新增;2:删除;3:修改数量)
         */
        private Integer adjustMode ;
        /**
         *示例值:2302480000005
         *描述:商品upc码
         */
        private String upcCode ;
        /**
         *示例值:690
         *描述:到家商品门店价
         */
        private Long skuStorePrice ;
        /**
         *示例值:590
         *描述:到家商品成本价
         */
        private Long skuCostPrice ;
        /**
         *示例值:1
         *描述:商品级别促销类型(1、无优惠;2、秒杀(已经下线);3、单品直降;4、限时抢购;1202、加价购;1203、满赠(标识商品);6、买赠(买A送B，标识B);9999、表示一个普通商品参与捆绑促销，设置的捆绑类型;9998、表示一个商品参与了捆绑促销，并且还参与了其他促销类型;9997、表示一个商品参与了捆绑促销，但是金额拆分不尽,9996:组合购,8001:商家会员价,8:第二件N折,9:拼团促销)
         */
        private Integer promotionType ;
        /**
         *示例值:
         *描述:税率
         */
        private String skuTaxRate ;
        /**
         *示例值:140828
         *描述:促销活动编码
         */
        private Long promotionId ;
        /**
         *示例值:2023746783
         *描述:赠品关联的主商品信息，多个商品之间英文逗号分隔
         */
        private String relatedSkus ;
        /**
         *示例值:0.67
         *描述:商品重量（千克）
         */
        private Double skuWeight ;
        /**
         *示例值:200
         *描述:餐盒费（商品的总餐盒费）
         */
        private Long canteenMoney ;
        /**
         *示例值:
         *描述:商品扩展信息(当有返回值时为轻松购电子秤商品)
         */
        private Object productExtendInfoMap ;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public String getSkuCostumeProperty() {
            return skuCostumeProperty;
        }

        public void setSkuCostumeProperty(String skuCostumeProperty) {
            this.skuCostumeProperty = skuCostumeProperty;
        }

        public Long getAdjustId() {
            return adjustId;
        }

        public void setAdjustId(Long adjustId) {
            this.adjustId = adjustId;
        }

        public Long getSkuId() {
            return skuId;
        }

        public void setSkuId(Long skuId) {
            this.skuId = skuId;
        }

        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
        }

        public String getSkuIdIsv() {
            return skuIdIsv;
        }

        public void setSkuIdIsv(String skuIdIsv) {
            this.skuIdIsv = skuIdIsv;
        }

        public Long getSkuJdPrice() {
            return skuJdPrice;
        }

        public void setSkuJdPrice(Long skuJdPrice) {
            this.skuJdPrice = skuJdPrice;
        }

        public Integer getSkuCount() {
            return skuCount;
        }

        public void setSkuCount(Integer skuCount) {
            this.skuCount = skuCount;
        }

        public Integer getAdjustMode() {
            return adjustMode;
        }

        public void setAdjustMode(Integer adjustMode) {
            this.adjustMode = adjustMode;
        }

        public String getUpcCode() {
            return upcCode;
        }

        public void setUpcCode(String upcCode) {
            this.upcCode = upcCode;
        }

        public Long getSkuStorePrice() {
            return skuStorePrice;
        }

        public void setSkuStorePrice(Long skuStorePrice) {
            this.skuStorePrice = skuStorePrice;
        }

        public Long getSkuCostPrice() {
            return skuCostPrice;
        }

        public void setSkuCostPrice(Long skuCostPrice) {
            this.skuCostPrice = skuCostPrice;
        }

        public Integer getPromotionType() {
            return promotionType;
        }

        public void setPromotionType(Integer promotionType) {
            this.promotionType = promotionType;
        }

        public String getSkuTaxRate() {
            return skuTaxRate;
        }

        public void setSkuTaxRate(String skuTaxRate) {
            this.skuTaxRate = skuTaxRate;
        }

        public Long getPromotionId() {
            return promotionId;
        }

        public void setPromotionId(Long promotionId) {
            this.promotionId = promotionId;
        }

        public String getRelatedSkus() {
            return relatedSkus;
        }

        public void setRelatedSkus(String relatedSkus) {
            this.relatedSkus = relatedSkus;
        }

        public Double getSkuWeight() {
            return skuWeight;
        }

        public void setSkuWeight(Double skuWeight) {
            this.skuWeight = skuWeight;
        }

        public Long getCanteenMoney() {
            return canteenMoney;
        }

        public void setCanteenMoney(Long canteenMoney) {
            this.canteenMoney = canteenMoney;
        }

        public Object getProductExtendInfoMap() {
            return productExtendInfoMap;
        }

        public void setProductExtendInfoMap(Object productExtendInfoMap) {
            this.productExtendInfoMap = productExtendInfoMap;
        }

        @Override
        public String toString() {
            return "OrderProductDTO{" +
                    "orderId=" + orderId +
                    ", skuCostumeProperty='" + skuCostumeProperty + '\'' +
                    ", adjustId=" + adjustId +
                    ", skuId=" + skuId +
                    ", skuName='" + skuName + '\'' +
                    ", skuIdIsv='" + skuIdIsv + '\'' +
                    ", skuJdPrice=" + skuJdPrice +
                    ", skuCount=" + skuCount +
                    ", adjustMode=" + adjustMode +
                    ", upcCode='" + upcCode + '\'' +
                    ", skuStorePrice=" + skuStorePrice +
                    ", skuCostPrice=" + skuCostPrice +
                    ", promotionType=" + promotionType +
                    ", skuTaxRate='" + skuTaxRate + '\'' +
                    ", promotionId=" + promotionId +
                    ", relatedSkus='" + relatedSkus + '\'' +
                    ", skuWeight=" + skuWeight +
                    ", canteenMoney=" + canteenMoney +
                    ", productExtendInfoMap=" + productExtendInfoMap +
                    '}';
        }
    }

    /**
     * @author dengfengfeng
     * @date 2020-01-07
     */

    public static class OrderDiscountDTO {
        /**
         *示例值:100001036354906
         *描述:订单主表订单id
         */
        private Long orderId ;
        /**
         *示例值:1000010248591821
         *描述:调整单记录id（0:原单商品明细;非0:调整单id或者确认单id)
         */
        private Long adjustId ;
        /**
         *示例值:2000830029,2002355309
         *描述:记录参加活动的sku数组
         */
        private String skuIds ;
        /**
         *示例值:1
         *描述:优惠类型(1:优惠码;3:优惠劵;4:满减;5:满折;6:首单优惠;7:VIP免运费;8:商家满免运费;10:满件减;11:满件折;12:首单地推满免运费;15:运费券)
         */
        private Integer discountType ;
        /**
         *示例值:1
         *描述:小优惠类型(优惠码(1:满减;2:立减;3:满折);优惠券(1:满减;2:立减;5满折);满件减(1206:满件减);满件折(1207:满件折);运费券（1：满减，2：立减）)
         */
        private Integer discountDetailType ;
        /**
         *示例值:201808091-101470919#1738452236
         *描述:用户领券编号，用户领取优惠券的标识，返回值中，-和#中间的字符串为对应优惠券实际编码，不同用户领券标识不同
         */
        private String discountCode ;
        /**
         *示例值:602
         *描述:优惠金额
         */
        private Long discountPrice ;
        /**
         *示例值:400
         *描述:商家承担金额（有免运费券时才会有值，其他优惠无值）
         */
        private Long venderPayMoney ;
        /**
         *示例值:100
         *描述:平台承担金额（有免运费券时才会有值，其他优惠无值）
         */
        private Long platPayMoney ;

        /**
         * 活动名称
         */
        private String discountName;

        /**
         * 实际分摊比率数据
         */
        private String orderShareRatioData;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Long getAdjustId() {
            return adjustId;
        }

        public void setAdjustId(Long adjustId) {
            this.adjustId = adjustId;
        }

        public String getSkuIds() {
            return skuIds;
        }

        public void setSkuIds(String skuIds) {
            this.skuIds = skuIds;
        }

        public Integer getDiscountType() {
            return discountType;
        }

        public void setDiscountType(Integer discountType) {
            this.discountType = discountType;
        }

        public Integer getDiscountDetailType() {
            return discountDetailType;
        }

        public void setDiscountDetailType(Integer discountDetailType) {
            this.discountDetailType = discountDetailType;
        }

        public String getDiscountCode() {
            return discountCode;
        }

        public void setDiscountCode(String discountCode) {
            this.discountCode = discountCode;
        }

        public Long getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(Long discountPrice) {
            this.discountPrice = discountPrice;
        }

        public Long getVenderPayMoney() {
            return venderPayMoney;
        }

        public void setVenderPayMoney(Long venderPayMoney) {
            this.venderPayMoney = venderPayMoney;
        }

        public Long getPlatPayMoney() {
            return platPayMoney;
        }

        public void setPlatPayMoney(Long platPayMoney) {
            this.platPayMoney = platPayMoney;
        }

        public String getDiscountName() {
            return discountName;
        }

        public void setDiscountName(String discountName) {
            this.discountName = discountName;
        }

        public String getOrderShareRatioData() {
            return orderShareRatioData;
        }

        public void setOrderShareRatioData(String orderShareRatioData) {
            this.orderShareRatioData = orderShareRatioData;
        }

        public OrderDiscountDTO.OrderShareRatioData extractOrderShareRatioData(){
            String[] dataSplit = orderShareRatioData.split("&");
            OrderDiscountDTO.OrderShareRatioData orderShareRatioData = new OrderDiscountDTO.OrderShareRatioData();
            for (int j = 0; j < dataSplit.length; j++) {
                String[] ratioData = dataSplit[j].split("=");
                String entryName = ratioData[0];
                String entryValue = ratioData[1];
                // 促销活动ID
                if (StringUtils.equalsIgnoreCase(entryName, "promotionId")) {
                    orderShareRatioData.promotionId = entryValue;
                }
                //商家分摊活动金额

                if (StringUtils.equalsIgnoreCase(entryName, "venderPayMoney")) {
                    orderShareRatioData.venderPayMoney = Double.parseDouble(entryValue);
                }
                // 平台分摊活动金额
                if (StringUtils.equalsIgnoreCase(entryName, "platPayMoney")) {
                    orderShareRatioData.platPayMoney = Double.parseDouble(entryValue);
                }
                if (StringUtils.equalsIgnoreCase(entryName, "venderShareProportion")) {
                    orderShareRatioData.venderShareProportion = Double.parseDouble(entryValue);
                }
                if (StringUtils.equalsIgnoreCase(entryName, "platShareProportion")) {
                    orderShareRatioData.platShareProportion = Double.parseDouble(entryValue);
                }
                if (StringUtils.equalsIgnoreCase(entryName, "createPin")) {
                    orderShareRatioData.createPin = entryValue;
                }
                if (StringUtils.equalsIgnoreCase(entryName, "sortId")) {
                    orderShareRatioData.sortId = entryValue;
                }

            }
            return orderShareRatioData;

        }

        /**
         * 实际分摊比率数据
         */
        public static class OrderShareRatioData {
            private String promotionId;
            private double venderShareProportion;
            private double platShareProportion;
            private double venderPayMoney;
            private double platPayMoney;
            private String createPin;
            private String sortId;

            public String getPromotionId() {
                return promotionId;
            }

            public void setPromotionId(String promotionId) {
                this.promotionId = promotionId;
            }

            public double getVenderShareProportion() {
                return venderShareProportion;
            }

            public void setVenderShareProportion(double venderShareProportion) {
                this.venderShareProportion = venderShareProportion;
            }

            public double getPlatShareProportion() {
                return platShareProportion;
            }

            public void setPlatShareProportion(double platShareProportion) {
                this.platShareProportion = platShareProportion;
            }

            public double getVenderPayMoney() {
                return venderPayMoney;
            }

            public void setVenderPayMoney(double venderPayMoney) {
                this.venderPayMoney = venderPayMoney;
            }

            public double getPlatPayMoney() {
                return platPayMoney;
            }

            public void setPlatPayMoney(double platPayMoney) {
                this.platPayMoney = platPayMoney;
            }

            public String getCreatePin() {
                return createPin;
            }

            public void setCreatePin(String createPin) {
                this.createPin = createPin;
            }

            public String getSortId() {
                return sortId;
            }

            public void setSortId(String sortId) {
                this.sortId = sortId;
            }

            @Override
            public String toString() {
                return "OrderShareRatioData{" +
                        "promotionId='" + promotionId + '\'' +
                        ", venderShareProportion=" + venderShareProportion +
                        ", platShareProportion=" + platShareProportion +
                        ", venderPayMoney=" + venderPayMoney +
                        ", platPayMoney=" + platPayMoney +
                        ", createPin='" + createPin + '\'' +
                        ", sortId='" + sortId + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "OrderDiscountDTO{" +
                    "orderId=" + orderId +
                    ", adjustId=" + adjustId +
                    ", skuIds='" + skuIds + '\'' +
                    ", discountType=" + discountType +
                    ", discountDetailType=" + discountDetailType +
                    ", discountCode='" + discountCode + '\'' +
                    ", discountPrice=" + discountPrice +
                    ", venderPayMoney=" + venderPayMoney +
                    ", platPayMoney=" + platPayMoney +
                    ", discountName='" + discountName + '\'' +
                    ", orderShareRatioData='" + orderShareRatioData + '\'' +
                    '}';
        }
    }

    /**
     * @author dengfengfeng
     */

    public static class OrderInfoDTO {
        /**
         *示例值:100001036354906
         *描述:订单号
         */
        private Long orderId ;
        /**
         *示例值:0
         *描述:订单来源类型(0:原订单，10:退款单，20:补货单，30:直赔商品，40:退货)
         */
        private Integer srcInnerType ;
        /**
         *示例值:10000
         *描述:订单类型（10000：从门店出的订单）
         */
        private Integer orderType ;
        /**
         *示例值:20010
         *描述:订单状态（20010:锁定，20020:订单取消，20030:订单取消申请，20040:超时未支付系统取消，20060:系统撤销订单，20050:暂停，31000:等待付款，31020:已付款，41000:待处理，32000:等待出库，33040:配送中，33060:已妥投，34000:京东已收款，90000:订单完成）
         */
        private Integer orderStatus ;
        /**
         *示例值:2016-07-0918:34:07
         *描述:订单状态最新更改时间
         */
        private Date orderStatusTime ;
        /**
         *示例值:2016-07-0915:40:30
         *描述:下单时间
         */
        private Date orderStartTime ;
        /**
         *示例值:2016-07-0915:40:50
         *描述:订单成交时间(在线支付类型订单的付款完成时间)
         */
        private Date orderPurchaseTime ;
        /**
         *示例值:12
         *描述:订单时效类型(0:无时效;2:自定义时间;1:次日达;27:七小时达;24:六小时达;21:五小时达;18:四小时达;15:三小时达;12:两小时达;9:一小时达;6:半小时达;)
         */
        private Integer orderAgingType ;
        /**
         *示例值:2016-07-0917:41:00
         *描述:预计送达开始时间
         */
        private Date orderPreStartDeliveryTime ;
        /**
         *示例值:2016-07-0917:41:00
         *描述:预计送达结束时间
         */
        private Date orderPreEndDeliveryTime ;
        /**
         *示例值:2016-07-0917:41:00
         *描述:商家最晚拣货完成时间
         */
        private Date pickDeadline ;
        /**
         *示例值:2016-07-0918:34:07
         *描述:订单取消时间
         */
        private Date orderCancelTime ;
        /**
         *示例值:其它
         *描述:订单取消备注
         */
        private String orderCancelRemark ;
        /**
         *示例值:71948
         *描述:商家编码
         */
        private String orgCode ;
        /**
         *示例值:王小明
         *描述:收货人名称
         */
        private String buyerFullName ;
        /**
         *示例值:上海市徐汇区乐山路19号广元西路乐山路，乐山路19号
         *描述:收货人地址
         */
        private String buyerFullAddress ;
        /**
         *示例值:18816912316
         *描述:收货人电话
         */
        private String buyerTelephone ;
        /**
         *示例值:18816912316
         *描述:收货人手机号
         */
        private String buyerMobile ;
        /**
         *示例值:8367
         *描述:收货人真实手机号后四位
         */
        private String lastFourDigitsOfBuyerMobile ;
        /**
         *示例值:11000478
         *描述:到家配送门店编码
         */
        private String deliveryStationNo ;
        /**
         *示例值:10100478
         *描述:商家门店编码
         */
        private String deliveryStationNoIsv ;
        /**
         *示例值:SH031永和大王-广元店
         *描述:配送门店名称
         */
        private String deliveryStationName ;
        /**
         *示例值:9966
         *描述:承运商编号(9966:京东众包;2938:商家自送;1130:达达同城送;9999:到店自提)
         */
        private String deliveryCarrierNo ;
        /**
         *示例值:众包配送
         *描述:承运商名称
         */
        private String deliveryCarrierName ;
        /**
         *示例值:1000001
         *描述:承运单号，通常情况下和订单号一致
         */
        private String deliveryBillNo ;
        /**
         *示例值:0.10000000149011612
         *描述:包裹重量（单位：kg）
         */
        private Double deliveryPackageWeight ;
        /**
         *示例值:2016-07-0918:34:07
         *描述:妥投时间
         */
        private Date deliveryConfirmTime ;
        /**
         *示例值:4
         *描述:订单支付类型(1：货到付款，4:在线支付;)
         */
        private Integer orderPayType ;
        /**
         *示例值:8001
         *描述:订单支付渠道，8001：微信支付；8002：微信免密代扣；8003：微信找人代付；9000：京东支付（无法判断具体京东支付子类型）；9002：京东银行卡支付；9004：京东白条支付；9012：京东余额支付；9022：京东小金库支付
         */
        private Integer payChannel ;
        /**
         *示例值:2500
         *描述:订单商品销售价总金额，等于sum（京东到家销售价skuJdPrice*商品下单数量skuCount）
         */
        private Long orderTotalMoney ;
        /**
         *示例值:0
         *描述:订单级别优惠商品金额：(不含单品促销类优惠金额及运费相关优惠金额)，等于OrderDiscountlist表中，除优惠类型7，8，12，15外的优惠金额discountPrice累加和
         */
        private Long orderDiscountMoney ;
        /**
         *示例值:300
         *描述:用户支付的实际订单运费：订单应收运费（orderReceivableFreight）-运费优惠（OrderDiscountlist表中，优惠类型7，8，12，15的优惠金额。运费优惠大于应收运费时，实际支付为0
         */
        private Long orderFreightMoney ;
        /**
         *示例值:500
         *描述:达达同城送运费(单位：分)
         */
        private Integer localDeliveryMoney ;
        /**
         *示例值:100
         *描述:商家支付远距离运费(单位：分)。达达配送默认只能服务半径2公里内的用户，商家可与到家运营沟通开通远距离服务，超过2公里后每1公里加收2元运费。费用承担方为商家
         */
        private Long merchantPaymentDistanceFreightMoney ;
        /**
         *示例值:300
         *描述:订单应收运费：用户应该支付的订单运费，即未优惠前应付运费(不计满免运费，运费优惠券，VIP免基础运费等优惠)，包含用户小费。订单对应门店配送方式为商家自送，则订单应收运费为设置的门店自送运费；订单对应门店配送方式为达达配送，则订单应收运费为用户支付给达达的配送费（平台规则统一设置，如基础运费、重量阶梯运费、距离阶梯运费、夜间或天气等因素的附加运费）
         */
        private Long orderReceivableFreight ;
        /**
         *示例值:100
         *描述:用户积分抵扣金额
         */
        private Long platformPointsDeductionMoney ;
        /**
         *示例值:2800
         *描述:用户应付金额（单位为分）=商品销售价总金额orderTotalMoney-订单优惠总金额orderDiscountMoney+实际订单运费orderFreightMoney+包装金额packagingMoney-用户积分抵扣金额platformPointsDeductionMoney
         */
        private Long orderBuyerPayableMoney ;
        /**
         *示例值:300
         *描述:包装金额
         */
        private Long packagingMoney ;
        /**
         *示例值:200
         *描述:商家给配送员加的小费，原订单配送方式为达达配送，在运单状态为待抢单且超时5分钟后，商家可以转成自己配送，转自送后，如果订单商家有小费，则商家小费清零。到家系统会下发订单转自送消息，商家需订阅转自送消息，再次调用订单列表查询接口获取订单信息
         */
        private Long tips ;
        /**
         *示例值:false
         *描述:订单商品缺货时，可以调用订单调整接口调整订单，调整之后需再次调用订单查询接口查询订单详情。该字段表示是否存在订单调整(false:否;true:是)
         */
        private Boolean adjustIsExists ;
        /**
         *示例值:0
         *描述:调整单编号
         */
        private Long adjustId ;
        /**
         *示例值:false
         *描述:是否拼团订单(false:否;true:是)
         */
        private Boolean isGroupon ;
        /**
         *示例值:2
         *描述:收货人地址定位类型（buyerCoordType值为空或为1时，定位类型为gps，如为其他值时，定位类型为非gps类型。)
         */
        private Integer buyerCoordType ;
        /**
         *示例值:121.4332
         *描述:收货人地址腾讯坐标经度
         */
        private Double buyerLng ;
        /**
         *示例值:31.19627
         *描述:收货人地址腾讯坐标纬度
         */
        private Double buyerLat ;
        /**
         *示例值:1
         *描述:收货人市ID
         */
        private String buyerCity ;
        /**
         *示例值:北京市
         *描述:收货人市名称
         */
        private String buyerCityName ;
        /**
         *示例值:72
         *描述:收货人县(区)ID
         */
        private String buyerCountry ;
        /**
         *示例值:朝阳区
         *描述:收货人县(区)名称
         */
        private String buyerCountryName ;
        /**
         *示例值:所购商品如遇缺货，您需要：其它商品继续配送（缺货商品退款）
         *描述:订单买家备注
         */
        private String orderBuyerRemark ;
        /**
         *示例值:dj_new_cashier;one_dingshida;picking_up;
         *描述:业务标识，用英文分号分隔（订单打标写入此字段，如one_dingshida定时达，dj_aging_nextday隔夜达，dj_aging_immediately立即达，picking_up拣货完成，pay_off支付完成,lengcang冷藏,lengdong冷冻,changwen常温,yisui易碎,yeti液体,yiyao医药,dj_prescription_order处方药订单,printed已打印,first_order平台首单,user_urge_order用户催单,dj_timeout_pay享受超时赔付,dj_giftcard_consume订单使用礼品卡,dj_vender_vip商家会员,easy_first_order轻松购首单用户,dj_vender_sam山姆会员店,dj_money_ice结算页购买冰袋,dj_purchase_vip结算页购买vip,dj_prepare_sale_order预售订单）。新时效：包含有”dj_new_promise_v3”或“dj_new_promise_v4”；旧时效：不包含”dj_new_promise_v3”或“dj_new_promise_v4”；
         */
        private String businessTag ;
        /**
         *示例值:e6008a0091df6ef9bb4ab499804dc487
         *描述:设备id
         */
        private String equipmentId ;
        /**
         *示例值:东大街东里社区
         *描述:收货人POI信息
         */
        private String buyerPoi ;
        /**
         *示例值:
         *描述:订购人姓名(此字段针对鲜花业务)
         */
        private String ordererName ;
        /**
         *示例值:
         *描述:订购人电话(此字段针对鲜花业务)
         */
        private String ordererMobile ;
        /**
         *示例值:1
         *描述:当天门店订单序号
         */
        private Long orderNum ;
        /**
         *示例值:10
         *描述:用户小费（用户给配送员加小费）
         */
        private Long userTip ;
        /**
         *示例值:2016-07-0917:41:00
         *描述:收货人电话中间号有效期
         */
        private Date middleNumBindingTime ;
        /**
         *示例值:2016-07-0918:41:00
         *描述:订单抛入达达抢单池时间
         */
        private Date deliverInputTime ;
        /**
         *示例值:1
         *描述:订单业务类型(1：京东到家商超，2：京东到家美食，4：京东到家开放仓，5：哥伦布店内订单，6：货柜订单，8：轻松购订单，9：是自助收银，10：超级会员码)
         */
        private Integer businessType ;
        /**
         *示例值:A100001
         *描述:VIP卡号
         */
        private String venderVipCardId ;
        /**
         *示例值:2
         *描述:订单开发票标识
         */
        private Integer orderInvoiceOpenMark ;
        /**
         *示例值:https://img30.360buyimg.com/myorderpdj/s960x1280_jfs/t2345/1.jpg
         *描述:处方订单处方单图片地址，当处方单未开具时，不会返回该字段信息
         */
        private String specialServiceTag ;
        /**
         *示例值:
         *描述:发票具体信息
         */
        private OrderInvoice orderInvoice ;
        /**
         *示例值:
         *描述:包含需要查询订单的商品List列表
         */
        private List<OrderProductDTO> product ;
        /**
         *示例值:
         *描述:包含需要查询订单的优惠List列表
         */
        private List<OrderDiscountDTO> discount ;
        /**
         *示例值:
         *描述:处方药详情信息
         */
        private PrescriptionDTO prescriptionDTO ;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Integer getSrcInnerType() {
            return srcInnerType;
        }

        public void setSrcInnerType(Integer srcInnerType) {
            this.srcInnerType = srcInnerType;
        }

        public Integer getOrderType() {
            return orderType;
        }

        public void setOrderType(Integer orderType) {
            this.orderType = orderType;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public Date getOrderStatusTime() {
            return orderStatusTime;
        }

        public void setOrderStatusTime(Date orderStatusTime) {
            this.orderStatusTime = orderStatusTime;
        }

        public Date getOrderStartTime() {
            return orderStartTime;
        }

        public void setOrderStartTime(Date orderStartTime) {
            this.orderStartTime = orderStartTime;
        }

        public Date getOrderPurchaseTime() {
            return orderPurchaseTime;
        }

        public void setOrderPurchaseTime(Date orderPurchaseTime) {
            this.orderPurchaseTime = orderPurchaseTime;
        }

        public Integer getOrderAgingType() {
            return orderAgingType;
        }

        public void setOrderAgingType(Integer orderAgingType) {
            this.orderAgingType = orderAgingType;
        }

        public Date getOrderPreStartDeliveryTime() {
            return orderPreStartDeliveryTime;
        }

        public void setOrderPreStartDeliveryTime(Date orderPreStartDeliveryTime) {
            this.orderPreStartDeliveryTime = orderPreStartDeliveryTime;
        }

        public Date getOrderPreEndDeliveryTime() {
            return orderPreEndDeliveryTime;
        }

        public void setOrderPreEndDeliveryTime(Date orderPreEndDeliveryTime) {
            this.orderPreEndDeliveryTime = orderPreEndDeliveryTime;
        }

        public Date getPickDeadline() {
            return pickDeadline;
        }

        public void setPickDeadline(Date pickDeadline) {
            this.pickDeadline = pickDeadline;
        }

        public Date getOrderCancelTime() {
            return orderCancelTime;
        }

        public void setOrderCancelTime(Date orderCancelTime) {
            this.orderCancelTime = orderCancelTime;
        }

        public String getOrderCancelRemark() {
            return orderCancelRemark;
        }

        public void setOrderCancelRemark(String orderCancelRemark) {
            this.orderCancelRemark = orderCancelRemark;
        }

        public String getOrgCode() {
            return orgCode;
        }

        public void setOrgCode(String orgCode) {
            this.orgCode = orgCode;
        }

        public String getBuyerFullName() {
            return buyerFullName;
        }

        public void setBuyerFullName(String buyerFullName) {
            this.buyerFullName = buyerFullName;
        }

        public String getBuyerFullAddress() {
            return buyerFullAddress;
        }

        public void setBuyerFullAddress(String buyerFullAddress) {
            this.buyerFullAddress = buyerFullAddress;
        }

        public String getBuyerTelephone() {
            return buyerTelephone;
        }

        public void setBuyerTelephone(String buyerTelephone) {
            this.buyerTelephone = buyerTelephone;
        }

        public String getBuyerMobile() {
            return buyerMobile;
        }

        public void setBuyerMobile(String buyerMobile) {
            this.buyerMobile = buyerMobile;
        }

        public String getLastFourDigitsOfBuyerMobile() {
            return lastFourDigitsOfBuyerMobile;
        }

        public void setLastFourDigitsOfBuyerMobile(String lastFourDigitsOfBuyerMobile) {
            this.lastFourDigitsOfBuyerMobile = lastFourDigitsOfBuyerMobile;
        }

        public String getDeliveryStationNo() {
            return deliveryStationNo;
        }

        public void setDeliveryStationNo(String deliveryStationNo) {
            this.deliveryStationNo = deliveryStationNo;
        }

        public String getDeliveryStationNoIsv() {
            return deliveryStationNoIsv;
        }

        public void setDeliveryStationNoIsv(String deliveryStationNoIsv) {
            this.deliveryStationNoIsv = deliveryStationNoIsv;
        }

        public String getDeliveryStationName() {
            return deliveryStationName;
        }

        public void setDeliveryStationName(String deliveryStationName) {
            this.deliveryStationName = deliveryStationName;
        }

        public String getDeliveryCarrierNo() {
            return deliveryCarrierNo;
        }

        public void setDeliveryCarrierNo(String deliveryCarrierNo) {
            this.deliveryCarrierNo = deliveryCarrierNo;
        }

        public String getDeliveryCarrierName() {
            return deliveryCarrierName;
        }

        public void setDeliveryCarrierName(String deliveryCarrierName) {
            this.deliveryCarrierName = deliveryCarrierName;
        }

        public String getDeliveryBillNo() {
            return deliveryBillNo;
        }

        public void setDeliveryBillNo(String deliveryBillNo) {
            this.deliveryBillNo = deliveryBillNo;
        }

        public Double getDeliveryPackageWeight() {
            return deliveryPackageWeight;
        }

        public void setDeliveryPackageWeight(Double deliveryPackageWeight) {
            this.deliveryPackageWeight = deliveryPackageWeight;
        }

        public Date getDeliveryConfirmTime() {
            return deliveryConfirmTime;
        }

        public void setDeliveryConfirmTime(Date deliveryConfirmTime) {
            this.deliveryConfirmTime = deliveryConfirmTime;
        }

        public Integer getOrderPayType() {
            return orderPayType;
        }

        public void setOrderPayType(Integer orderPayType) {
            this.orderPayType = orderPayType;
        }

        public Integer getPayChannel() {
            return payChannel;
        }

        public void setPayChannel(Integer payChannel) {
            this.payChannel = payChannel;
        }

        public Long getOrderTotalMoney() {
            return orderTotalMoney;
        }

        public void setOrderTotalMoney(Long orderTotalMoney) {
            this.orderTotalMoney = orderTotalMoney;
        }

        public Long getOrderDiscountMoney() {
            return orderDiscountMoney;
        }

        public void setOrderDiscountMoney(Long orderDiscountMoney) {
            this.orderDiscountMoney = orderDiscountMoney;
        }

        public Long getOrderFreightMoney() {
            return orderFreightMoney;
        }

        public void setOrderFreightMoney(Long orderFreightMoney) {
            this.orderFreightMoney = orderFreightMoney;
        }

        public Integer getLocalDeliveryMoney() {
            return localDeliveryMoney;
        }

        public void setLocalDeliveryMoney(Integer localDeliveryMoney) {
            this.localDeliveryMoney = localDeliveryMoney;
        }

        public Long getMerchantPaymentDistanceFreightMoney() {
            return merchantPaymentDistanceFreightMoney;
        }

        public void setMerchantPaymentDistanceFreightMoney(Long merchantPaymentDistanceFreightMoney) {
            this.merchantPaymentDistanceFreightMoney = merchantPaymentDistanceFreightMoney;
        }

        public Long getOrderReceivableFreight() {
            return orderReceivableFreight;
        }

        public void setOrderReceivableFreight(Long orderReceivableFreight) {
            this.orderReceivableFreight = orderReceivableFreight;
        }

        public Long getPlatformPointsDeductionMoney() {
            return platformPointsDeductionMoney;
        }

        public void setPlatformPointsDeductionMoney(Long platformPointsDeductionMoney) {
            this.platformPointsDeductionMoney = platformPointsDeductionMoney;
        }

        public Long getOrderBuyerPayableMoney() {
            return orderBuyerPayableMoney;
        }

        public void setOrderBuyerPayableMoney(Long orderBuyerPayableMoney) {
            this.orderBuyerPayableMoney = orderBuyerPayableMoney;
        }

        public Long getPackagingMoney() {
            return packagingMoney;
        }

        public void setPackagingMoney(Long packagingMoney) {
            this.packagingMoney = packagingMoney;
        }

        public Long getTips() {
            return tips;
        }

        public void setTips(Long tips) {
            this.tips = tips;
        }

        public Boolean getAdjustIsExists() {
            return adjustIsExists;
        }

        public void setAdjustIsExists(Boolean adjustIsExists) {
            this.adjustIsExists = adjustIsExists;
        }

        public Long getAdjustId() {
            return adjustId;
        }

        public void setAdjustId(Long adjustId) {
            this.adjustId = adjustId;
        }

        public Boolean getGroupon() {
            return isGroupon;
        }

        public void setGroupon(Boolean groupon) {
            isGroupon = groupon;
        }

        public Integer getBuyerCoordType() {
            return buyerCoordType;
        }

        public void setBuyerCoordType(Integer buyerCoordType) {
            this.buyerCoordType = buyerCoordType;
        }

        public Double getBuyerLng() {
            return buyerLng;
        }

        public void setBuyerLng(Double buyerLng) {
            this.buyerLng = buyerLng;
        }

        public Double getBuyerLat() {
            return buyerLat;
        }

        public void setBuyerLat(Double buyerLat) {
            this.buyerLat = buyerLat;
        }

        public String getBuyerCity() {
            return buyerCity;
        }

        public void setBuyerCity(String buyerCity) {
            this.buyerCity = buyerCity;
        }

        public String getBuyerCityName() {
            return buyerCityName;
        }

        public void setBuyerCityName(String buyerCityName) {
            this.buyerCityName = buyerCityName;
        }

        public String getBuyerCountry() {
            return buyerCountry;
        }

        public void setBuyerCountry(String buyerCountry) {
            this.buyerCountry = buyerCountry;
        }

        public String getBuyerCountryName() {
            return buyerCountryName;
        }

        public void setBuyerCountryName(String buyerCountryName) {
            this.buyerCountryName = buyerCountryName;
        }

        public String getOrderBuyerRemark() {
            return orderBuyerRemark;
        }

        public void setOrderBuyerRemark(String orderBuyerRemark) {
            this.orderBuyerRemark = orderBuyerRemark;
        }

        public String getBusinessTag() {
            return businessTag;
        }

        public void setBusinessTag(String businessTag) {
            this.businessTag = businessTag;
        }

        public String getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(String equipmentId) {
            this.equipmentId = equipmentId;
        }

        public String getBuyerPoi() {
            return buyerPoi;
        }

        public void setBuyerPoi(String buyerPoi) {
            this.buyerPoi = buyerPoi;
        }

        public String getOrdererName() {
            return ordererName;
        }

        public void setOrdererName(String ordererName) {
            this.ordererName = ordererName;
        }

        public String getOrdererMobile() {
            return ordererMobile;
        }

        public void setOrdererMobile(String ordererMobile) {
            this.ordererMobile = ordererMobile;
        }

        public Long getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(Long orderNum) {
            this.orderNum = orderNum;
        }

        public Long getUserTip() {
            return userTip;
        }

        public void setUserTip(Long userTip) {
            this.userTip = userTip;
        }

        public Date getMiddleNumBindingTime() {
            return middleNumBindingTime;
        }

        public void setMiddleNumBindingTime(Date middleNumBindingTime) {
            this.middleNumBindingTime = middleNumBindingTime;
        }

        public Date getDeliverInputTime() {
            return deliverInputTime;
        }

        public void setDeliverInputTime(Date deliverInputTime) {
            this.deliverInputTime = deliverInputTime;
        }

        public Integer getBusinessType() {
            return businessType;
        }

        public void setBusinessType(Integer businessType) {
            this.businessType = businessType;
        }

        public String getVenderVipCardId() {
            return venderVipCardId;
        }

        public void setVenderVipCardId(String venderVipCardId) {
            this.venderVipCardId = venderVipCardId;
        }

        public Integer getOrderInvoiceOpenMark() {
            return orderInvoiceOpenMark;
        }

        public void setOrderInvoiceOpenMark(Integer orderInvoiceOpenMark) {
            this.orderInvoiceOpenMark = orderInvoiceOpenMark;
        }

        public String getSpecialServiceTag() {
            return specialServiceTag;
        }

        public void setSpecialServiceTag(String specialServiceTag) {
            this.specialServiceTag = specialServiceTag;
        }

        public OrderInvoice getOrderInvoice() {
            return orderInvoice;
        }

        public void setOrderInvoice(OrderInvoice orderInvoice) {
            this.orderInvoice = orderInvoice;
        }

        public List<OrderProductDTO> getProduct() {
            return product;
        }

        public void setProduct(List<OrderProductDTO> product) {
            this.product = product;
        }

        public List<OrderDiscountDTO> getDiscount() {
            return discount;
        }

        public void setDiscount(List<OrderDiscountDTO> discount) {
            this.discount = discount;
        }

        public PrescriptionDTO getPrescriptionDTO() {
            return prescriptionDTO;
        }

        public void setPrescriptionDTO(PrescriptionDTO prescriptionDTO) {
            this.prescriptionDTO = prescriptionDTO;
        }

        @Override
        public String toString() {
            return "OrderInfoDTO{" +
                    "orderId=" + orderId +
                    ", srcInnerType=" + srcInnerType +
                    ", orderType=" + orderType +
                    ", orderStatus=" + orderStatus +
                    ", orderStatusTime=" + orderStatusTime +
                    ", orderStartTime=" + orderStartTime +
                    ", orderPurchaseTime=" + orderPurchaseTime +
                    ", orderAgingType=" + orderAgingType +
                    ", orderPreStartDeliveryTime=" + orderPreStartDeliveryTime +
                    ", orderPreEndDeliveryTime=" + orderPreEndDeliveryTime +
                    ", pickDeadline=" + pickDeadline +
                    ", orderCancelTime=" + orderCancelTime +
                    ", orderCancelRemark='" + orderCancelRemark + '\'' +
                    ", orgCode='" + orgCode + '\'' +
                    ", buyerFullName='" + buyerFullName + '\'' +
                    ", buyerFullAddress='" + buyerFullAddress + '\'' +
                    ", buyerTelephone='" + buyerTelephone + '\'' +
                    ", buyerMobile='" + buyerMobile + '\'' +
                    ", lastFourDigitsOfBuyerMobile='" + lastFourDigitsOfBuyerMobile + '\'' +
                    ", deliveryStationNo='" + deliveryStationNo + '\'' +
                    ", deliveryStationNoIsv='" + deliveryStationNoIsv + '\'' +
                    ", deliveryStationName='" + deliveryStationName + '\'' +
                    ", deliveryCarrierNo='" + deliveryCarrierNo + '\'' +
                    ", deliveryCarrierName='" + deliveryCarrierName + '\'' +
                    ", deliveryBillNo='" + deliveryBillNo + '\'' +
                    ", deliveryPackageWeight=" + deliveryPackageWeight +
                    ", deliveryConfirmTime=" + deliveryConfirmTime +
                    ", orderPayType=" + orderPayType +
                    ", payChannel=" + payChannel +
                    ", orderTotalMoney=" + orderTotalMoney +
                    ", orderDiscountMoney=" + orderDiscountMoney +
                    ", orderFreightMoney=" + orderFreightMoney +
                    ", localDeliveryMoney=" + localDeliveryMoney +
                    ", merchantPaymentDistanceFreightMoney=" + merchantPaymentDistanceFreightMoney +
                    ", orderReceivableFreight=" + orderReceivableFreight +
                    ", platformPointsDeductionMoney=" + platformPointsDeductionMoney +
                    ", orderBuyerPayableMoney=" + orderBuyerPayableMoney +
                    ", packagingMoney=" + packagingMoney +
                    ", tips=" + tips +
                    ", adjustIsExists=" + adjustIsExists +
                    ", adjustId=" + adjustId +
                    ", isGroupon=" + isGroupon +
                    ", buyerCoordType=" + buyerCoordType +
                    ", buyerLng=" + buyerLng +
                    ", buyerLat=" + buyerLat +
                    ", buyerCity='" + buyerCity + '\'' +
                    ", buyerCityName='" + buyerCityName + '\'' +
                    ", buyerCountry='" + buyerCountry + '\'' +
                    ", buyerCountryName='" + buyerCountryName + '\'' +
                    ", orderBuyerRemark='" + orderBuyerRemark + '\'' +
                    ", businessTag='" + businessTag + '\'' +
                    ", equipmentId='" + equipmentId + '\'' +
                    ", buyerPoi='" + buyerPoi + '\'' +
                    ", ordererName='" + ordererName + '\'' +
                    ", ordererMobile='" + ordererMobile + '\'' +
                    ", orderNum=" + orderNum +
                    ", userTip=" + userTip +
                    ", middleNumBindingTime=" + middleNumBindingTime +
                    ", deliverInputTime=" + deliverInputTime +
                    ", businessType=" + businessType +
                    ", venderVipCardId='" + venderVipCardId + '\'' +
                    ", orderInvoiceOpenMark=" + orderInvoiceOpenMark +
                    ", specialServiceTag='" + specialServiceTag + '\'' +
                    ", orderInvoice=" + orderInvoice +
                    ", product=" + product +
                    ", discount=" + discount +
                    ", prescriptionDTO=" + prescriptionDTO +
                    '}';
        }
    }

    /**
     * @author dff
     * @date 2020-01-07
     */

    public static class OrderInvoice {
        /**
         *示例值:1
         *描述:发票类型：0.纸质发票1.电子发票
         */
        private Integer invoiceFormType ;
        /**
         *示例值:京东
         *描述:发票抬头
         */
        private String invoiceTitle ;
        /**
         *示例值:112233
         *描述:发票税号
         */
        private String invoiceDutyNo ;
        /**
         *示例值:jd@jd.com
         *描述:发票邮箱地址
         */
        private String invoiceMail ;
        /**
         *示例值:100
         *描述:发票金额
         */
        private Long invoiceMoney ;
        /**
         *示例值:1
         *描述:发票抬头类型(0：个人、1：企业普票、2：企业专票)
         */
        private Integer invoiceType ;
        /**
         *示例值:描述
         *描述:发票金额描述
         */
        private String invoiceMoneyDetail ;
        /**
         *示例值:地址
         *描述:公司注册地址
         */
        private String invoiceAddress ;
        /**
         *示例值:432444
         *描述:公司注册电话
         */
        private String invoiceTelNo ;
        /**
         *示例值:建设银行支行
         *描述:公司开户银行名称
         */
        private String invoiceBankName ;
        /**
         *示例值:432444
         *描述:公司开户银行账户
         */
        private String invoiceAccountNo ;
        /**
         *示例值:餐饮
         *描述:发票内容
         */
        private String invoiceContent ;

        public Integer getInvoiceFormType() {
            return invoiceFormType;
        }

        public void setInvoiceFormType(Integer invoiceFormType) {
            this.invoiceFormType = invoiceFormType;
        }

        public String getInvoiceTitle() {
            return invoiceTitle;
        }

        public void setInvoiceTitle(String invoiceTitle) {
            this.invoiceTitle = invoiceTitle;
        }

        public String getInvoiceDutyNo() {
            return invoiceDutyNo;
        }

        public void setInvoiceDutyNo(String invoiceDutyNo) {
            this.invoiceDutyNo = invoiceDutyNo;
        }

        public String getInvoiceMail() {
            return invoiceMail;
        }

        public void setInvoiceMail(String invoiceMail) {
            this.invoiceMail = invoiceMail;
        }

        public Long getInvoiceMoney() {
            return invoiceMoney;
        }

        public void setInvoiceMoney(Long invoiceMoney) {
            this.invoiceMoney = invoiceMoney;
        }

        public Integer getInvoiceType() {
            return invoiceType;
        }

        public void setInvoiceType(Integer invoiceType) {
            this.invoiceType = invoiceType;
        }

        public String getInvoiceMoneyDetail() {
            return invoiceMoneyDetail;
        }

        public void setInvoiceMoneyDetail(String invoiceMoneyDetail) {
            this.invoiceMoneyDetail = invoiceMoneyDetail;
        }

        public String getInvoiceAddress() {
            return invoiceAddress;
        }

        public void setInvoiceAddress(String invoiceAddress) {
            this.invoiceAddress = invoiceAddress;
        }

        public String getInvoiceTelNo() {
            return invoiceTelNo;
        }

        public void setInvoiceTelNo(String invoiceTelNo) {
            this.invoiceTelNo = invoiceTelNo;
        }

        public String getInvoiceBankName() {
            return invoiceBankName;
        }

        public void setInvoiceBankName(String invoiceBankName) {
            this.invoiceBankName = invoiceBankName;
        }

        public String getInvoiceAccountNo() {
            return invoiceAccountNo;
        }

        public void setInvoiceAccountNo(String invoiceAccountNo) {
            this.invoiceAccountNo = invoiceAccountNo;
        }

        public String getInvoiceContent() {
            return invoiceContent;
        }

        public void setInvoiceContent(String invoiceContent) {
            this.invoiceContent = invoiceContent;
        }

        @Override
        public String toString() {
            return "OrderInvoice{" +
                    "invoiceFormType=" + invoiceFormType +
                    ", invoiceTitle='" + invoiceTitle + '\'' +
                    ", invoiceDutyNo='" + invoiceDutyNo + '\'' +
                    ", invoiceMail='" + invoiceMail + '\'' +
                    ", invoiceMoney=" + invoiceMoney +
                    ", invoiceType=" + invoiceType +
                    ", invoiceMoneyDetail='" + invoiceMoneyDetail + '\'' +
                    ", invoiceAddress='" + invoiceAddress + '\'' +
                    ", invoiceTelNo='" + invoiceTelNo + '\'' +
                    ", invoiceBankName='" + invoiceBankName + '\'' +
                    ", invoiceAccountNo='" + invoiceAccountNo + '\'' +
                    ", invoiceContent='" + invoiceContent + '\'' +
                    '}';
        }
    }


    /**
     * <p>Page</p>
     * description
     *
     * @author 邓峰峰
     * @date 2019/12/16 15:13
     */


    public static class Page {
        private Integer pageNo;
        private Integer pageSize;
        private Integer maxPageSize;
        private Integer totalCount;
        private List<OrderInfoDTO> resultList;


        public Integer getPageNo() {
            return pageNo;
        }

        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        public Integer getMaxPageSize() {
            return maxPageSize;
        }

        public void setMaxPageSize(Integer maxPageSize) {
            this.maxPageSize = maxPageSize;
        }

        public Integer getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(Integer totalCount) {
            this.totalCount = totalCount;
        }

        public List<OrderInfoDTO> getResultList() {
            return resultList;
        }

        public void setResultList(List<OrderInfoDTO> resultList) {
            this.resultList = resultList;
        }

        @Override
        public String toString() {
            return "Page{" +
                    "pageNo=" + pageNo +
                    ", pageSize=" + pageSize +
                    ", maxPageSize=" + maxPageSize +
                    ", totalCount=" + totalCount +
                    ", resultList=" + resultList +
                    '}';
        }
    }

    public static class OrderInfoListData implements Data<Page> {
        private String code;
        private String msg;
        private String result;
        private String detail;
        private Boolean success;

        /**
         * 订单列表分页数据
         */
        private Page pageData;

        @Override
        public Page extractData() {
            if(pageData!=null){
                return pageData;
            }
            if(result==null){
                throw new FailedRequestException(this.detail);
            }
            final Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class,new DateAdapter())
                    .create();
            final Page page = gson.fromJson(result, Page.class);
            this.pageData = page;
            return page;
        }


        @Override
        public boolean isSuccess(){
            return "0".equals(code);
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }


        public Page getPageData() {
            return extractData();
        }

        public void setPageData(Page pageData) {
            this.pageData = pageData;
        }

        @Override
        public String toString() {
            return "OrderInfoListData{" +
                    "code='" + code + '\'' +
                    ", msg='" + msg + '\'' +
                    ", result='" + result + '\'' +
                    ", detail='" + detail + '\'' +
                    ", success=" + success +
                    ", pageData=" + pageData +
                    '}';
        }
    }



}
