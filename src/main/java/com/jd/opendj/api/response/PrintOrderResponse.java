package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */ 
public class PrintOrderResponse extends OpenDjResponse {
	private PrintOrderData printOrderData;

	public PrintOrderData getPrintOrderData() {
		return printOrderData;
	}

	public void setPrintOrderData(PrintOrderData printOrderData) {
		this.printOrderData = printOrderData;
	}

	@Override
	public void toJavaObject() {
		final Gson gson = new Gson();
		this.printOrderData = gson.fromJson(data, PrintOrderData.class);
	}

	public static class PrintOrderData implements Data<Void>{
		/**
		 *示例值:0
		 *描述:4001001：输入参数为空，4001002：订单信息不存在，4001003：越权操作订单，4001004：订单状态异常0：成功
		 */
		private String code;
		/**
		 *示例值:
		 *描述:msg
		 */
		private String msg;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		@Override
		public Void extractData() {
			throw new UnsupportedOperationException("不支持提取数据");
		}

		@Override
		public boolean isSuccess() {
			return "0".equals(code);
		}
	}

} 
