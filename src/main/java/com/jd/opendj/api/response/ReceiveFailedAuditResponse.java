package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class ReceiveFailedAuditResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.receiveFailedAuditData = gson.fromJson(data, ReceiveFailedAuditData.class);
    }

    private ReceiveFailedAuditData receiveFailedAuditData;

    public ReceiveFailedAuditData getReceiveFailedAuditData() {
        return receiveFailedAuditData;
    }

    public void setReceiveFailedAuditData(ReceiveFailedAuditData receiveFailedAuditData) {
        this.receiveFailedAuditData = receiveFailedAuditData;
    }

    public static class ReceiveFailedAuditData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:返回结果(0:操作成功-1:操作失败)
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:返回结果描述
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
