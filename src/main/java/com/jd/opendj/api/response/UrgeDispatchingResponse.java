package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class UrgeDispatchingResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.urgeDispatchingData = gson.fromJson(data, UrgeDispatchingData.class);
    }

    private UrgeDispatchingData urgeDispatchingData;

    public UrgeDispatchingData getUrgeDispatchingData() {
        return urgeDispatchingData;
    }

    public void setUrgeDispatchingData(UrgeDispatchingData urgeDispatchingData) {
        this.urgeDispatchingData = urgeDispatchingData;
    }

    public static class UrgeDispatchingData implements Data<Void>{
        /**
         * 示例值:0
         * 描述:返回码（1:参数错误-1:失败0:成功-3:系统错误20133：非营业时间不能抛单）
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:返回错误信息
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:详情
         */
        private String detail;

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
