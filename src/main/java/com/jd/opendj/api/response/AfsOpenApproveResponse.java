package com.jd.opendj.api.response;


import com.jd.opendj.api.OpenDjResponse;
import org.apache.commons.lang3.BooleanUtils;

/**
 * <p>AfsOpenApproveResponse</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/1/19 17:59
 */


public class AfsOpenApproveResponse extends OpenDjResponse {

    private AfsOpenApproveData afsOpenApproveData;

    public AfsOpenApproveData getAfsOpenApproveData() {
        return afsOpenApproveData;
    }

    public void setAfsOpenApproveData(AfsOpenApproveData afsOpenApproveData) {
        this.afsOpenApproveData = afsOpenApproveData;
    }

    public static class AfsOpenApproveData implements Data<Void> {
        private Integer code;
        private String msg;
        private Boolean success;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("无法提取数据");
        }

        @Override
        public boolean isSuccess() {
            return BooleanUtils.toBoolean(success);
        }
    }


    @Override
    public void toJavaObject() {

    }
}
