package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;
import org.apache.commons.lang3.BooleanUtils;

import java.util.List;

/**
 * <p>BillData</p>
 * description
 *
 * @author 邓峰峰
 * @date 2019/12/16 14:46
 */

public class OassBussMoneyResponse extends OpenDjResponse {

    private OassBussinessSkusNewListData oassBussinessSkusNewListData;

    public OassBussinessSkusNewListData getOassBussinessSkusNewListData() {
        return oassBussinessSkusNewListData;
    }

    public void setOassBussinessSkusNewListData(OassBussinessSkusNewListData oassBussinessSkusNewListData) {
        this.oassBussinessSkusNewListData = oassBussinessSkusNewListData;
    }

    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.oassBussinessSkusNewListData = gson.fromJson(data, OassBussinessSkusNewListData.class);
    }

    public static class OassBussinessSkusNewListData implements ListData<OassBussMoneyResponse.OassBussinessSkusNew> {
        private Boolean success;
        private String errorCode;
        private String errorInfos;
        private List<OassBussinessSkusNew> data;

        @Override
        public List<OassBussinessSkusNew> extractListData() {
            return data;
        }

        @Override
        public boolean isSuccess() {
            return BooleanUtils.toBoolean(success);
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorInfos() {
            return errorInfos;
        }

        public void setErrorInfos(String errorInfos) {
            this.errorInfos = errorInfos;
        }

        public List<OassBussinessSkusNew> getData() {
            return data;
        }

        public void setData(List<OassBussinessSkusNew> data) {
            this.data = data;
        }
    }


    /**
     * @author dengfengfeng
     * @date 2019-11-13
     */

    public static class OassBussinessSkusNew {
        /**
         * 示例值:100001036354906
         * 描述:订单号
         */
        private Long orderId;
        /**
         * 示例值:2002445142
         * 描述:到家商品编码
         */
        private Long skuId;
        /**
         * 示例值:1
         * 描述:购买数量
         */
        private Integer skuCount;
        /**
         * 示例值:100
         * 描述:销售价(优惠后的单价)
         */
        private Integer promotionPrice;
        /**
         * 示例值:0
         * 描述:原价
         */
        private Integer pdjPrice;
        /**
         * 示例值:0
         * 描述:成本价(优惠类型为非单品促销(第二件N折除外)时，此价格为0)
         */
        private Integer costPrice;
        /**
         * 示例值:0
         * 描述:商品优惠后实付金额(商品金额-分摊到商品优惠金额)
         */
        private Long skuPayMoney;
        /**
         * 示例值:0
         * 描述:平台承担比例，如促销类型为组合购9996时，承担比例为空，需以平台承担金额为准。
         */
        private Integer costRadio;
        /**
         * 示例值:0
         * 描述:商家承担比例，如促销类型为组合购9996时，承担比例为空，需以商家承担金额为准。
         */
        private Integer saleRadio;
        /**
         * 示例值:0
         * 描述:平台承担金额
         */
        private Long costMoney;
        /**
         * 示例值:0
         * 描述:商家承担金额
         */
        private Long saleMoney;
        /**
         * 示例值:100
         * 描述:平台积分（鲜豆）金额
         */
        private Long platformIntegralDeductMoney;
        /**
         * 示例值:1
         * 描述:商品级别促销类型(1、无优惠;2、秒杀(已经下线);3、单品直降;4、限时抢购;1202、加价购;9999、表示一个普通商品参与捆绑促销，设置的捆绑类型;9998、表示一个商品参与了捆绑促销，并且还参与了其他促销类型;9997、表示一个商品参与了捆绑促销，但是金额拆分不尽,9996:组合购,8001:商家会员价,8:第二件N折,9:拼团促销)
         */
        private Integer promotionType;
        /**
         * 示例值:512555
         * 描述:外部活动编号
         */
        private String outActivityId;
        /**
         * 示例值:
         * 描述:订单级别促销优惠列表
         */
        private List<OrderBussiDiscountMoney> discountlist;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Long getSkuId() {
            return skuId;
        }

        public void setSkuId(Long skuId) {
            this.skuId = skuId;
        }

        public Integer getSkuCount() {
            return skuCount;
        }

        public void setSkuCount(Integer skuCount) {
            this.skuCount = skuCount;
        }

        public Integer getPromotionPrice() {
            return promotionPrice;
        }

        public void setPromotionPrice(Integer promotionPrice) {
            this.promotionPrice = promotionPrice;
        }

        public Integer getPdjPrice() {
            return pdjPrice;
        }

        public void setPdjPrice(Integer pdjPrice) {
            this.pdjPrice = pdjPrice;
        }

        public Integer getCostPrice() {
            return costPrice;
        }

        public void setCostPrice(Integer costPrice) {
            this.costPrice = costPrice;
        }

        public Long getSkuPayMoney() {
            return skuPayMoney;
        }

        public void setSkuPayMoney(Long skuPayMoney) {
            this.skuPayMoney = skuPayMoney;
        }

        public Integer getCostRadio() {
            return costRadio;
        }

        public void setCostRadio(Integer costRadio) {
            this.costRadio = costRadio;
        }

        public Integer getSaleRadio() {
            return saleRadio;
        }

        public void setSaleRadio(Integer saleRadio) {
            this.saleRadio = saleRadio;
        }

        public Long getCostMoney() {
            return costMoney;
        }

        public void setCostMoney(Long costMoney) {
            this.costMoney = costMoney;
        }

        public Long getSaleMoney() {
            return saleMoney;
        }

        public void setSaleMoney(Long saleMoney) {
            this.saleMoney = saleMoney;
        }

        public Long getPlatformIntegralDeductMoney() {
            return platformIntegralDeductMoney;
        }

        public void setPlatformIntegralDeductMoney(Long platformIntegralDeductMoney) {
            this.platformIntegralDeductMoney = platformIntegralDeductMoney;
        }

        public Integer getPromotionType() {
            return promotionType;
        }

        public void setPromotionType(Integer promotionType) {
            this.promotionType = promotionType;
        }

        public String getOutActivityId() {
            return outActivityId;
        }

        public void setOutActivityId(String outActivityId) {
            this.outActivityId = outActivityId;
        }

        public List<OrderBussiDiscountMoney> getDiscountlist() {
            return discountlist;
        }

        public void setDiscountlist(List<OrderBussiDiscountMoney> discountlist) {
            this.discountlist = discountlist;
        }
    }


    /**
     * @author dengfengfeng
     * @date 2019-11-13
     */

    public static class OrderBussiDiscountMoney {
        /**
         * 示例值:1
         * 描述:优惠类型(1:优惠码;3:优惠劵;4:满减;5:满折;6:首单优惠;10:满件减;11:满件折;)
         */
        private Integer promotionType;
        /**
         * 示例值:1
         * 描述:优惠二级类型(优惠码(1:满减;2:立减;3:满折;);优惠券(1:满减;2:立减;5:满折);满件减(1206:满件减);满件折(1207:满件折))
         */
        private Integer promotionDetailType;
        /**
         * 示例值:100
         * 描述:优惠金额
         */
        private Long skuDiscountMoney;
        /**
         * 示例值:0
         * 描述:商家承担比例
         */
        private Integer saleRadio;
        /**
         * 示例值:0
         * 描述:平台承担金额
         */
        private Long costMoney;
        /**
         * 示例值:0
         * 描述:商家承担金额
         */
        private Long saleMoney;
        /**
         * 示例值:512555
         * 描述:促销号
         */
        private String promotionCode;
        /**
         * 示例值:512555
         * 描述:外部活动编号
         */
        private String outActivityId;

        public Integer getPromotionType() {
            return promotionType;
        }

        public void setPromotionType(Integer promotionType) {
            this.promotionType = promotionType;
        }

        public Integer getPromotionDetailType() {
            return promotionDetailType;
        }

        public void setPromotionDetailType(Integer promotionDetailType) {
            this.promotionDetailType = promotionDetailType;
        }

        public Long getSkuDiscountMoney() {
            return skuDiscountMoney;
        }

        public void setSkuDiscountMoney(Long skuDiscountMoney) {
            this.skuDiscountMoney = skuDiscountMoney;
        }

        public Integer getSaleRadio() {
            return saleRadio;
        }

        public void setSaleRadio(Integer saleRadio) {
            this.saleRadio = saleRadio;
        }

        public Long getCostMoney() {
            return costMoney;
        }

        public void setCostMoney(Long costMoney) {
            this.costMoney = costMoney;
        }

        public Long getSaleMoney() {
            return saleMoney;
        }

        public void setSaleMoney(Long saleMoney) {
            this.saleMoney = saleMoney;
        }

        public String getPromotionCode() {
            return promotionCode;
        }

        public void setPromotionCode(String promotionCode) {
            this.promotionCode = promotionCode;
        }

        public String getOutActivityId() {
            return outActivityId;
        }

        public void setOutActivityId(String outActivityId) {
            this.outActivityId = outActivityId;
        }
    }

}
