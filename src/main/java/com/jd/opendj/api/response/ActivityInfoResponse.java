package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.opendj.api.OpenDjResponse;
import com.jd.opendj.api.response.adapter.DateAdapter;
import org.apache.commons.lang3.BooleanUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author dengfengfeng
 * @date 2019-11-13
 */

public class ActivityInfoResponse extends OpenDjResponse {

	private OpenPlatActivityQResponseData openPlatActivityQResponseData;

	public OpenPlatActivityQResponseData getOpenPlatActivityQResponseData() {
		return openPlatActivityQResponseData;
	}

	public void setOpenPlatActivityQResponseData(OpenPlatActivityQResponseData openPlatActivityQResponseData) {
		this.openPlatActivityQResponseData = openPlatActivityQResponseData;
	}

	@Override
	public void toJavaObject() {
		final Gson gson = new GsonBuilder()
				.registerTypeAdapter(Date.class,new DateAdapter())
				.create();
		this.openPlatActivityQResponseData = gson.fromJson(data, OpenPlatActivityQResponseData.class);
	}

	/**
	 * @author dengfengfeng
	 * @date 2019-11-13
	 */

	public static class OpenPlatActivitySku {
		/**
		 *sample value:
		 *商品ID
		 */
		private Long skuId ;
		/**
		 *sample value:
		 *商品名称
		 */
		private String skuName ;

		public Long getSkuId() {
			return skuId;
		}

		public void setSkuId(Long skuId) {
			this.skuId = skuId;
		}

		public String getSkuName() {
			return skuName;
		}

		public void setSkuName(String skuName) {
			this.skuName = skuName;
		}
	}

	public static class OpenPlatActivityQResponseData implements Data<ActivityInfoResponse.OpenPlatActivity>{
		private String errorCode;
		private String errorInfos;
		private Boolean success;
		private OpenPlatActivity data;

		@Override
		public OpenPlatActivity extractData() {
			return data;
		}

		@Override
		public boolean isSuccess() {
			return BooleanUtils.toBoolean(success);
		}

		public String getErrorCode() {
			return errorCode;
		}

		public void setErrorCode(String errorCode) {
			this.errorCode = errorCode;
		}

		public String getErrorInfos() {
			return errorInfos;
		}

		public void setErrorInfos(String errorInfos) {
			this.errorInfos = errorInfos;
		}

		public Boolean getSuccess() {
			return success;
		}

		public void setSuccess(Boolean success) {
			this.success = success;
		}


		public OpenPlatActivity getData() {
			return data;
		}

		public void setData(OpenPlatActivity data) {
			this.data = data;
		}


	}

	/**
	 * @author dengfengfeng
	 * @date 2019-11-13
	 */

	public static class OpenPlatActivity {
		/**
		 *示例值:4128
		 *描述:活动ID
		 */
		private Long id;
		/**
		 *示例值:xxx活动
		 *描述:活动名称
		 */
		private String promotionName;
		/**
		 *示例值:xxx广告
		 *描述:广告词
		 */
		private String awords;
		/**
		 *示例值:1
		 *描述:0:运营,1:商家自建,2:提报,3:开放平台
		 */
		private Integer isMerchant;
		/**
		 *示例值:10元3件
		 *描述:促销规则
		 */
		private String orderLadder;
		/**
		 *示例值:100
		 *描述:承担比
		 */
		private Integer costRadios;
		/**
		 *示例值:2017-10-1012:00:00实际为java.util.Date的JSON格式字符串
		 *描述:活动开始时间
		 */
		private Date beginDate;
		/**
		 *示例值:2017-10-1112:00:00实际为java.util.Date的JSON格式字符串
		 *描述:活动结束时间
		 */
		private Date endDate;
		/**
		 *示例值:
		 *描述:活动对应的sku集合
		 */
		private List<OpenPlatActivitySku> skuBeanList;
		/**
		 *示例值:
		 *描述:活动对应的门店(station)集合
		 */
		private List<OpenPlatActivitySku> stationBeanList;
		/**
		 *示例值:
		 *描述:促销规则阶梯
		 */
		private List<OpenPlatOrderDiscountLimitResponse> addLadderList;

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getPromotionName() {
			return promotionName;
		}

		public void setPromotionName(String promotionName) {
			this.promotionName = promotionName;
		}

		public String getAwords() {
			return awords;
		}

		public void setAwords(String awords) {
			this.awords = awords;
		}

		public Integer getIsMerchant() {
			return isMerchant;
		}

		public void setIsMerchant(Integer isMerchant) {
			this.isMerchant = isMerchant;
		}

		public String getOrderLadder() {
			return orderLadder;
		}

		public void setOrderLadder(String orderLadder) {
			this.orderLadder = orderLadder;
		}

		public Integer getCostRadios() {
			return costRadios;
		}

		public void setCostRadios(Integer costRadios) {
			this.costRadios = costRadios;
		}

		public Date getBeginDate() {
			return beginDate;
		}

		public void setBeginDate(Date beginDate) {
			this.beginDate = beginDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}

		public List<OpenPlatActivitySku> getSkuBeanList() {
			return skuBeanList;
		}

		public void setSkuBeanList(List<OpenPlatActivitySku> skuBeanList) {
			this.skuBeanList = skuBeanList;
		}

		public List<OpenPlatActivitySku> getStationBeanList() {
			return stationBeanList;
		}

		public void setStationBeanList(List<OpenPlatActivitySku> stationBeanList) {
			this.stationBeanList = stationBeanList;
		}

		public List<OpenPlatOrderDiscountLimitResponse> getAddLadderList() {
			return addLadderList;
		}

		public void setAddLadderList(List<OpenPlatOrderDiscountLimitResponse> addLadderList) {
			this.addLadderList = addLadderList;
		}
	}


	/**
	 * @author dengfengfeng
	 * @date 2019-11-13
	 */

	public static class OpenPlatOrderDiscountLimitResponse {
		/**
		 *示例值:
		 *描述:促销最低金额：满减的“满”金额使用（单位：分）
		 */
		private Long lowMoney;
		/**
		 *示例值:
		 *描述:优惠金额（满减，满件减，捆绑）使用（单位：分）
		 */
		private Long discountAmount;
		/**
		 *示例值:
		 *描述:促销件数（满件减，满减折，捆绑）
		 */
		private Integer lowerLimitCount;
		/**
		 *示例值:
		 *描述:折扣，（满件折）使用
		 */
		private BigDecimal discountRate;
		/**
		 *示例值:
		 *描述:促销规则描述
		 */
		private String orderLadder;
		/**
		 *示例值:
		 *描述:每满减最高优惠次数
		 */
		private Integer benefitMaxCount;

		public Long getLowMoney() {
			return lowMoney;
		}

		public void setLowMoney(Long lowMoney) {
			this.lowMoney = lowMoney;
		}

		public Long getDiscountAmount() {
			return discountAmount;
		}

		public void setDiscountAmount(Long discountAmount) {
			this.discountAmount = discountAmount;
		}

		public Integer getLowerLimitCount() {
			return lowerLimitCount;
		}

		public void setLowerLimitCount(Integer lowerLimitCount) {
			this.lowerLimitCount = lowerLimitCount;
		}

		public BigDecimal getDiscountRate() {
			return discountRate;
		}

		public void setDiscountRate(BigDecimal discountRate) {
			this.discountRate = discountRate;
		}

		public String getOrderLadder() {
			return orderLadder;
		}

		public void setOrderLadder(String orderLadder) {
			this.orderLadder = orderLadder;
		}

		public Integer getBenefitMaxCount() {
			return benefitMaxCount;
		}

		public void setBenefitMaxCount(Integer benefitMaxCount) {
			this.benefitMaxCount = benefitMaxCount;
		}
	}



}
