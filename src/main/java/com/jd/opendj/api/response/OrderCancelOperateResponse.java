package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class OrderCancelOperateResponse extends OpenDjResponse {
    private OrderCancelOperateData orderCancelOperateData;

    public OrderCancelOperateData getOrderCancelOperateData() {
        return orderCancelOperateData;
    }

    public void setOrderCancelOperateData(OrderCancelOperateData orderCancelOperateData) {
        this.orderCancelOperateData = orderCancelOperateData;
    }

    @Override
    public void toJavaObject() {
        final Gson gson = new Gson();
        this.orderCancelOperateData = gson.fromJson(data, OrderCancelOperateData.class);
    }

    public static class OrderCancelOperateData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:code，1:参数错误-1:失败0:成功
         */
        private String code;
        /**
         * 示例值:订单取消已审核
         * 描述:msg
         */
        private String msg;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
