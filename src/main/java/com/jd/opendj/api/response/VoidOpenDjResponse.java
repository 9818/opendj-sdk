package com.jd.opendj.api.response;

import com.jd.opendj.api.OpenDjResponse;

/**
 * <p>VoidOpenDjResponse</p>
 * description
 *
 * @author 邓峰峰
 * @date 9/2/2020 8:40 PM
 */
public class VoidOpenDjResponse extends OpenDjResponse {


    @Override
    public void toJavaObject() {

    }
}
