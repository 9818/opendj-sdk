package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class ConfirmReceiptResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.confirmReceiptData = gson.fromJson(data, ConfirmReceiptData.class);
    }

    private ConfirmReceiptData confirmReceiptData;

    public ConfirmReceiptData getConfirmReceiptData() {
        return confirmReceiptData;
    }

    public void setConfirmReceiptData(ConfirmReceiptData confirmReceiptData) {
        this.confirmReceiptData = confirmReceiptData;
    }

    public static class ConfirmReceiptData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:状态码，11001-该售后单已确认收货，不能重复确认收货；0-成功；-1-失败；-3-系统错误；9-非法的操作
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:状态码描述
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:处理结果
         */
        private String data;

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
