package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.util.List;

/**
 * @author dff
 * @date 2020-09-16
 */
public class FinalAfsServiceListResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.finalAfsServiceListData = gson.fromJson(data, FinalAfsServiceListData.class);
    }

    private FinalAfsServiceListData finalAfsServiceListData;

    public FinalAfsServiceListData getFinalAfsServiceListData() {
        return finalAfsServiceListData;
    }

    public void setFinalAfsServiceListData(FinalAfsServiceListData finalAfsServiceListData) {
        this.finalAfsServiceListData = finalAfsServiceListData;
    }

    public static class FinalAfsServiceListData implements Data<FinalAfsServicePage> {
        /**
         * 示例值:0
         * 描述:0-成功；-1-失败；-3-系统错误；
         */
        private Integer code;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        /**
         * 示例值:
         * 描述:返回数据（售后单分页列表）
         */
        private FinalAfsServicePage result;

        public FinalAfsServicePage getResult() {
            return result;
        }

        public void setResult(FinalAfsServicePage result) {
            this.result = result;
        }

        @Override
        public FinalAfsServicePage extractData() {
            return result;
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }

    public static class FinalAfsServicePage {
        /**
         * 示例值:10
         * 描述:总页数
         */
        private Integer totalPage;

        public Integer getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(Integer totalPage) {
            this.totalPage = totalPage;
        }

        /**
         * 示例值:1
         * 描述:当前页
         */
        private Integer currentPage;

        public Integer getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(Integer currentPage) {
            this.currentPage = currentPage;
        }

        /**
         * 示例值:500
         * 描述:总条数
         */
        private Integer count;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        /**
         * 示例值:50
         * 描述:分页size
         */
        private Integer pageSize;

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         * 示例值:
         * 描述:售后单列表
         */
        private List<FinalAfsServiceList> data;

        public List<FinalAfsServiceList> getData() {
            return data;
        }

        public void setData(List<FinalAfsServiceList> data) {
            this.data = data;
        }
    }

    public static class FinalAfsServiceList {
        /**
         * 示例值:100001035999127
         * 描述:订单号
         */
        private String orderId;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        /**
         * 示例值:1
         * 描述:订单业务类型(1：京东到家商超，2：京东到家美食，4：京东到家开放仓，5：哥伦布店内订单，6：货柜订单，8：轻松购订单，9：是自助收银，10：超级会员码)
         */
        private Integer businessType;

        public Integer getBusinessType() {
            return businessType;
        }

        public void setBusinessType(Integer businessType) {
            this.businessType = businessType;
        }

        /**
         * 示例值:20043683
         * 描述:售后单号
         */
        private String afsServiceOrder;

        public String getAfsServiceOrder() {
            return afsServiceOrder;
        }

        public void setAfsServiceOrder(String afsServiceOrder) {
            this.afsServiceOrder = afsServiceOrder;
        }

        /**
         * 示例值:10
         * 描述:售后单状态（10:待审核,20:待取件,30:退款处理中,31:待商家收货审核,32:退款成功,33:退款失败,40:审核不通过-驳回,50:客户取消,60:商家收货审核不通过,70:已解决,91:直赔,92:直赔成功,93:直赔失败,90:待赔付,110:待退货,111:取货成功,112:退货成功-待退款,113:退货失败,114:退货成功）
         */
        private String afsServiceState;

        public String getAfsServiceState() {
            return afsServiceState;
        }

        public void setAfsServiceState(String afsServiceState) {
            this.afsServiceState = afsServiceState;
        }

        /**
         * 示例值:201
         * 描述:申请售后原因（201:商品质量问题,202:送错货,203:缺件少件,207:缺斤少两,208:包装脏污有破损，210：商家通知我缺货，402:不想要了，502:未在时效内送达）
         */
        private Integer questionTypeCid;

        public Integer getQuestionTypeCid() {
            return questionTypeCid;
        }

        public void setQuestionTypeCid(Integer questionTypeCid) {
            this.questionTypeCid = questionTypeCid;
        }

        /**
         * 示例值:货物坏掉了
         * 描述:
         */
        private String questionDesc;

        public String getQuestionDesc() {
            return questionDesc;
        }

        public void setQuestionDesc(String questionDesc) {
            this.questionDesc = questionDesc;
        }

        /**
         * 示例值:1572594152000
         * 描述:审核日期
         */
        private Long approvedDate;

        public Long getApprovedDate() {
            return approvedDate;
        }

        public void setApprovedDate(Long approvedDate) {
            this.approvedDate = approvedDate;
        }

        /**
         * 示例值:erp_yangliu25
         * 描述:
         */
        private String approvePin;

        public String getApprovePin() {
            return approvePin;
        }

        public void setApprovePin(String approvePin) {
            this.approvePin = approvePin;
        }

        /**
         * 示例值:SJ_24dm180e8aea0
         * 描述:下单顾客账号（到家账号）
         */
        private String customerPin;

        public String getCustomerPin() {
            return customerPin;
        }

        public void setCustomerPin(String customerPin) {
            this.customerPin = customerPin;
        }

        /**
         * 示例值:陈宇
         * 描述:客户姓名
         */
        private String customerName;

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        /**
         * 示例值:13990691000
         * 描述:客户电话
         */
        private String customerMobilePhone;

        public String getCustomerMobilePhone() {
            return customerMobilePhone;
        }

        public void setCustomerMobilePhone(String customerMobilePhone) {
            this.customerMobilePhone = customerMobilePhone;
        }

        /**
         * 示例值:10004047
         * 描述:到家门店编号
         */
        private String stationId;

        public String getStationId() {
            return stationId;
        }

        public void setStationId(String stationId) {
            this.stationId = stationId;
        }

        /**
         * 示例值:八达岭便利店
         * 描述:到家门店名称
         */
        private String stationName;

        public String getStationName() {
            return stationName;
        }

        public void setStationName(String stationName) {
            this.stationName = stationName;
        }

        /**
         * 示例值:
         * 描述:运费(退货取件费)；在售后单为达达同城送订单时，该字段为0
         */
        private Long afsFreight;

        public Long getAfsFreight() {
            return afsFreight;
        }

        public void setAfsFreight(Long afsFreight) {
            this.afsFreight = afsFreight;
        }

        /**
         * 示例值:1300
         * 描述:应退商品金额+应退积分金额（单位为分，售后单原始总金额-京豆金额-优惠总金额）
         */
        private Long afsMoney;

        public Long getAfsMoney() {
            return afsMoney;
        }

        public void setAfsMoney(Long afsMoney) {
            this.afsMoney = afsMoney;
        }

        /**
         * 示例值:40
         * 描述:售后单类型（10:仅退款,30:直赔，40:退货退款），仅退款/退货退款由用户发起，直赔由商家发起
         */
        private String applyDeal;

        public String getApplyDeal() {
            return applyDeal;
        }

        public void setApplyDeal(String applyDeal) {
            this.applyDeal = applyDeal;
        }

        /**
         * 示例值:
         * 描述:商家名称
         */
        private String venderName;

        public String getVenderName() {
            return venderName;
        }

        public void setVenderName(String venderName) {
            this.venderName = venderName;
        }

        /**
         * 示例值:1572594152000
         * 描述:创建时间
         */
        private Long createTime;

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        /**
         * 示例值:1572594152000
         * 描述:更新时间
         */
        private Long updateTime;

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }
    }
} 
