package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class ModifySellerDeliveryResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.modifySellerDeliveryData = gson.fromJson(data, ModifySellerDeliveryData.class);
    }

    private ModifySellerDeliveryData modifySellerDeliveryData;

    public ModifySellerDeliveryData getModifySellerDeliveryData() {
        return modifySellerDeliveryData;
    }

    public void setModifySellerDeliveryData(ModifySellerDeliveryData modifySellerDeliveryData) {
        this.modifySellerDeliveryData = modifySellerDeliveryData;
    }

    public static class ModifySellerDeliveryData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:状态码，1:参数错误-1:失败0:成功
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:操作成功
         * 描述:返回描述信息
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:详情
         */
        private String detail;

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
