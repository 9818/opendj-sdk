package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.math.BigDecimal;

/**
 * @author dff
 * @date 2020-09-16
 */
public class SettleOrderDetailResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.settleOrderDetailData = gson.fromJson(data, SettleOrderDetailData.class);
    }

    private SettleOrderDetailData settleOrderDetailData;

    public SettleOrderDetailData getSettleOrderDetailData() {
        return settleOrderDetailData;
    }

    public void setSettleOrderDetailData(SettleOrderDetailData settleOrderDetailData) {
        this.settleOrderDetailData = settleOrderDetailData;
    }

    public static class SettleOrderDetailData implements Data<SettleOrderDetailContent> {
        /**
         * 示例值:200
         * 描述:状态码
         */
        private Integer code;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        /**
         * 示例值:正常
         * 描述:描述信息
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:返回结果
         */
        private SettleOrderDetailContent content;

        public SettleOrderDetailContent getContent() {
            return content;
        }

        public void setContent(SettleOrderDetailContent content) {
            this.content = content;
        }

        @Override
        public SettleOrderDetailContent extractData() {
            return content;
        }

        @Override
        public boolean isSuccess() {
            return "200".equals(code);
        }
    }

    public static class SettleOrderDetailContent {
        /**
         * 示例值:
         * 描述:结算单详情参
         */
        private SettleOrderBo settleOrderBo;

        public SettleOrderBo getSettleOrderBo() {
            return settleOrderBo;
        }

        public void setSettleOrderBo(SettleOrderBo settleOrderBo) {
            this.settleOrderBo = settleOrderBo;
        }

        /**
         * 示例值:10
         * 描述:总补贴(平台+商家承担补贴)：平台承担所有补贴+商家承担所有补贴（单位：元）
         */
        private BigDecimal summarySubsidy;

        public BigDecimal getSummarySubsidy() {
            return summarySubsidy;
        }

        public void setSummarySubsidy(BigDecimal summarySubsidy) {
            this.summarySubsidy = summarySubsidy;
        }

        /**
         * 示例值:0
         * 描述:总应付金额（单位：元）
         */
        private BigDecimal summaryAmount;

        public BigDecimal getSummaryAmount() {
            return summaryAmount;
        }

        public void setSummaryAmount(BigDecimal summaryAmount) {
            this.summaryAmount = summaryAmount;
        }

        /**
         * 示例值:0
         * 描述:总货款（单位：元）
         */
        private BigDecimal summaryGoodsBill;

        public BigDecimal getSummaryGoodsBill() {
            return summaryGoodsBill;
        }

        public void setSummaryGoodsBill(BigDecimal summaryGoodsBill) {
            this.summaryGoodsBill = summaryGoodsBill;
        }

        /**
         * 示例值:
         * 描述:返回结果
         */
        private SettleOrderPage settleDetails;

        public SettleOrderPage getSettleDetails() {
            return settleDetails;
        }

        public void setSettleDetails(SettleOrderPage settleDetails) {
            this.settleDetails = settleDetails;
        }
    }

    public static class SettleOrderBo {
        /**
         * 示例值:122111
         * 描述:结算单编码
         */
        private Long settleOrderId;

        public Long getSettleOrderId() {
            return settleOrderId;
        }

        public void setSettleOrderId(Long settleOrderId) {
            this.settleOrderId = settleOrderId;
        }

        /**
         * 示例值:2342
         * 描述:商家编码
         */
        private Long orgCode;

        public Long getOrgCode() {
            return orgCode;
        }

        public void setOrgCode(Long orgCode) {
            this.orgCode = orgCode;
        }

        /**
         * 示例值:商家名称
         * 描述:商家名称
         */
        private String orgName;

        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        /**
         * 示例值:123
         * 描述:到家门店编码
         */
        private Long stationId;

        public Long getStationId() {
            return stationId;
        }

        public void setStationId(Long stationId) {
            this.stationId = stationId;
        }

        /**
         * 示例值:门店名称
         * 描述:门店名称
         */
        private String stationName;

        public String getStationName() {
            return stationName;
        }

        public void setStationName(String stationName) {
            this.stationName = stationName;
        }

        /**
         * 示例值:201701
         * 描述:账期开始时间时间戳
         */
        private Long accountTimeStart;

        public Long getAccountTimeStart() {
            return accountTimeStart;
        }

        public void setAccountTimeStart(Long accountTimeStart) {
            this.accountTimeStart = accountTimeStart;
        }

        /**
         * 示例值:201701
         * 描述:账期结束时间
         */
        private Long accountTimeEnd;

        public Long getAccountTimeEnd() {
            return accountTimeEnd;
        }

        public void setAccountTimeEnd(Long accountTimeEnd) {
            this.accountTimeEnd = accountTimeEnd;
        }

        /**
         * 示例值:100
         * 描述:结算金额（单位：元）
         */
        private BigDecimal settleMoney;

        public BigDecimal getSettleMoney() {
            return settleMoney;
        }

        public void setSettleMoney(BigDecimal settleMoney) {
            this.settleMoney = settleMoney;
        }

        /**
         * 示例值:
         * 描述:结算单类型
         */
        private String settleType;

        public String getSettleType() {
            return settleType;
        }

        public void setSettleType(String settleType) {
            this.settleType = settleType;
        }

        /**
         * 示例值:1
         * 描述:结算单类型代码1货款2市场费5006新货款
         */
        private Integer settleTypeCode;

        public Integer getSettleTypeCode() {
            return settleTypeCode;
        }

        public void setSettleTypeCode(Integer settleTypeCode) {
            this.settleTypeCode = settleTypeCode;
        }

        /**
         * 示例值:
         * 描述:结算状态
         */
        private String settleStatus;

        public String getSettleStatus() {
            return settleStatus;
        }

        public void setSettleStatus(String settleStatus) {
            this.settleStatus = settleStatus;
        }

        /**
         * 示例值:2002
         * 描述:结算状态代码待打款(20002)、结算成功(20003)、结算失败(20004,20011)、驳回(20005)、打款失败(20009)、冻结中（20014）、部分结清（20015）
         */
        private Integer settleStatusCode;

        public Integer getSettleStatusCode() {
            return settleStatusCode;
        }

        public void setSettleStatusCode(Integer settleStatusCode) {
            this.settleStatusCode = settleStatusCode;
        }

        /**
         * 示例值:
         * 描述:失败信息包含打款失败信息
         */
        private String memo;

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        /**
         * 示例值:201701
         * 描述:结算完成时间
         */
        private Long finishTime;

        public Long getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(Long finishTime) {
            this.finishTime = finishTime;
        }

        /**
         * 示例值:201701
         * 描述:最近更新时间
         */
        private Long updateTime;

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        /**
         * 示例值:
         * 描述:打款方式
         */
        private String payMethod;

        public String getPayMethod() {
            return payMethod;
        }

        public void setPayMethod(String payMethod) {
            this.payMethod = payMethod;
        }

        /**
         * 示例值:1
         * 描述:打款方式代码(1支付宝2银行卡3京东钱包)
         */
        private Integer payMethodCode;

        public Integer getPayMethodCode() {
            return payMethodCode;
        }

        public void setPayMethodCode(Integer payMethodCode) {
            this.payMethodCode = payMethodCode;
        }

        /**
         * 示例值:2313
         * 描述:收付款单号
         */
        private String outFlowId;

        public String getOutFlowId() {
            return outFlowId;
        }

        public void setOutFlowId(String outFlowId) {
            this.outFlowId = outFlowId;
        }

        /**
         * 示例值:
         * 描述:账户信息
         */
        private SettleOrderAccountContent accountContent;

        public SettleOrderAccountContent getAccountContent() {
            return accountContent;
        }

        public void setAccountContent(SettleOrderAccountContent accountContent) {
            this.accountContent = accountContent;
        }
    }

    public static class SettleOrderAccountContent {
        /**
         * 示例值:北京
         * 描述:银行所在城市
         */
        private String bankCity;

        public String getBankCity() {
            return bankCity;
        }

        public void setBankCity(String bankCity) {
            this.bankCity = bankCity;
        }

        /**
         * 示例值:北京
         * 描述:银行所在省份
         */
        private String bankProvince;

        public String getBankProvince() {
            return bankProvince;
        }

        public void setBankProvince(String bankProvince) {
            this.bankProvince = bankProvince;
        }

        /**
         * 示例值:北京银行
         * 描述:支行名称
         */
        private String bankSubBranch;

        public String getBankSubBranch() {
            return bankSubBranch;
        }

        public void setBankSubBranch(String bankSubBranch) {
            this.bankSubBranch = bankSubBranch;
        }

        /**
         * 示例值:
         * 描述:银行类型
         */
        private String bankType;

        public String getBankType() {
            return bankType;
        }

        public void setBankType(String bankType) {
            this.bankType = bankType;
        }

        /**
         * 示例值:1111222
         * 描述:银行联行号
         */
        private String bankAssociatedCode;

        public String getBankAssociatedCode() {
            return bankAssociatedCode;
        }

        public void setBankAssociatedCode(String bankAssociatedCode) {
            this.bankAssociatedCode = bankAssociatedCode;
        }

        /**
         * 示例值:P
         * 描述:账户类型P-对私C-对公
         */
        private String bankAccountType;

        public String getBankAccountType() {
            return bankAccountType;
        }

        public void setBankAccountType(String bankAccountType) {
            this.bankAccountType = bankAccountType;
        }

        /**
         * 示例值:jd111
         * 描述:账号
         */
        private String accountNo;

        public String getAccountNo() {
            return accountNo;
        }

        public void setAccountNo(String accountNo) {
            this.accountNo = accountNo;
        }

        /**
         * 示例值:skr
         * 描述:收款人
         */
        private String payeeName;

        public String getPayeeName() {
            return payeeName;
        }

        public void setPayeeName(String payeeName) {
            this.payeeName = payeeName;
        }
    }

    public static class SettleOrderPage {
        /**
         * 示例值:1
         * 描述:页码
         */
        private Long pageNum;

        public Long getPageNum() {
            return pageNum;
        }

        public void setPageNum(Long pageNum) {
            this.pageNum = pageNum;
        }

        /**
         * 示例值:10
         * 描述:每页条数
         */
        private Integer pageSize;

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         * 示例值:10
         * 描述:总条数
         */
        private Long total;

        public Long getTotal() {
            return total;
        }

        public void setTotal(Long total) {
            this.total = total;
        }

        /**
         * 示例值:1
         * 描述:开始行
         */
        private Integer startRow;

        public Integer getStartRow() {
            return startRow;
        }

        public void setStartRow(Integer startRow) {
            this.startRow = startRow;
        }

        /**
         * 示例值:10
         * 描述:结束行
         */
        private Integer endRow;

        public Integer getEndRow() {
            return endRow;
        }

        public void setEndRow(Integer endRow) {
            this.endRow = endRow;
        }

        /**
         * 示例值:10
         * 描述:总页数
         */
        private Integer pages;

        public Integer getPages() {
            return pages;
        }

        public void setPages(Integer pages) {
            this.pages = pages;
        }

        /**
         * 示例值:
         * 描述:结算单信息
         */
        private SettleDetailDaoJiaDailyDTO result;

        public SettleDetailDaoJiaDailyDTO getResult() {
            return result;
        }

        public void setResult(SettleDetailDaoJiaDailyDTO result) {
            this.result = result;
        }
    }

    public static class SettleDetailDaoJiaDailyDTO {
        /**
         * 示例值:122111
         * 描述:结算单编码
         */
        private Long settleOrderId;

        public Long getSettleOrderId() {
            return settleOrderId;
        }

        public void setSettleOrderId(Long settleOrderId) {
            this.settleOrderId = settleOrderId;
        }

        /**
         * 示例值:11111
         * 描述:市场费结算单号
         */
        private Long marketSettleOrderId;

        public Long getMarketSettleOrderId() {
            return marketSettleOrderId;
        }

        public void setMarketSettleOrderId(Long marketSettleOrderId) {
            this.marketSettleOrderId = marketSettleOrderId;
        }

        /**
         * 示例值:210701
         * 描述:账期
         */
        private Long accountTime;

        public Long getAccountTime() {
            return accountTime;
        }

        public void setAccountTime(Long accountTime) {
            this.accountTime = accountTime;
        }

        /**
         * 示例值:4210701
         * 描述:当天门店订单序号
         */
        private Integer serialNo;

        public Integer getSerialNo() {
            return serialNo;
        }

        public void setSerialNo(Integer serialNo) {
            this.serialNo = serialNo;
        }

        /**
         * 示例值:43210701
         * 描述:正向订单为订单号；售后单为售后单号
         */
        private Long orderId;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        /**
         * 示例值:1
         * 描述:异常调整=1;正向订单=1001;售后退货=1002;售后退款=1003;售后退货申述=1004;售后退款申述=1005;售后换新申述=1006;售后换新=1016;售后直赔=1017;
         */
        private Integer orderTypeCode;

        public Integer getOrderTypeCode() {
            return orderTypeCode;
        }

        public void setOrderTypeCode(Integer orderTypeCode) {
            this.orderTypeCode = orderTypeCode;
        }

        /**
         * 示例值:售后单
         * 描述:订单类型(正向订单/售后退款/售后退货/售后退款申诉/售后退货申诉/售后换新申诉)
         */
        private String orderType;

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        /**
         * 示例值:沃尔玛
         * 描述:商家名称
         */
        private String orgName;

        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        /**
         * 示例值:6424
         * 描述:商家编码
         */
        private Long orgCode;

        public Long getOrgCode() {
            return orgCode;
        }

        public void setOrgCode(Long orgCode) {
            this.orgCode = orgCode;
        }

        /**
         * 示例值:北京店
         * 描述:门店名称
         */
        private String stationName;

        public String getStationName() {
            return stationName;
        }

        public void setStationName(String stationName) {
            this.stationName = stationName;
        }

        /**
         * 示例值:16424
         * 描述:到家门店编码
         */
        private Long stationId;

        public Long getStationId() {
            return stationId;
        }

        public void setStationId(Long stationId) {
            this.stationId = stationId;
        }

        /**
         * 示例值:1
         * 描述:订单状态（已完成:1）
         */
        private Integer status;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        /**
         * 示例值:1
         * 描述:支付方式(在线支付:1）
         */
        private Integer payType;

        public Integer getPayType() {
            return payType;
        }

        public void setPayType(Integer payType) {
            this.payType = payType;
        }

        /**
         * 示例值:9966
         * 描述:配送方式（9966-达达配送2938-商家自送1130-达达同城送）
         */
        private Integer deliveryType;

        public Integer getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(Integer deliveryType) {
            this.deliveryType = deliveryType;
        }

        /**
         * 示例值:210701
         * 描述:下单时间
         */
        private Long businessStartTime;

        public Long getBusinessStartTime() {
            return businessStartTime;
        }

        public void setBusinessStartTime(Long businessStartTime) {
            this.businessStartTime = businessStartTime;
        }

        /**
         * 示例值:210701
         * 描述:完成时间
         */
        private Long businessFinishTime;

        public Long getBusinessFinishTime() {
            return businessFinishTime;
        }

        public void setBusinessFinishTime(Long businessFinishTime) {
            this.businessFinishTime = businessFinishTime;
        }

        /**
         * 示例值:210701
         * 描述:计费时间
         */
        private Long businessFeeTime;

        public Long getBusinessFeeTime() {
            return businessFeeTime;
        }

        public void setBusinessFeeTime(Long businessFeeTime) {
            this.businessFeeTime = businessFeeTime;
        }

        /**
         * 示例值:10
         * 描述:应结金额：订单原价（originalAmount）+商家承担补贴（storeSubsidy）+总佣金（commissionBill）+商家自送配送费（storeFreightBill）+餐盒费（packageBill）+取件服务费开票（pickUpFeeNeedBill）+取件服务费不开票（pickUpFeeNotBill）（费用自带正负号）（单位：元）
         */
        private BigDecimal waitSettleAmount;

        public BigDecimal getWaitSettleAmount() {
            return waitSettleAmount;
        }

        public void setWaitSettleAmount(BigDecimal waitSettleAmount) {
            this.waitSettleAmount = waitSettleAmount;
        }

        /**
         * 示例值:10
         * 描述:订单原价：用户实际支付货款+平台承担所有补贴+商家货款承担补贴，公式中各字段均取正值（单位：元）
         */
        private BigDecimal originalAmount;

        public BigDecimal getOriginalAmount() {
            return originalAmount;
        }

        public void setOriginalAmount(BigDecimal originalAmount) {
            this.originalAmount = originalAmount;
        }

        /**
         * 示例值:10
         * 描述:在线支付(用户实际支付货款)(结算总金额相加)（单位：元）
         */
        private BigDecimal payOnline;

        public BigDecimal getPayOnline() {
            return payOnline;
        }

        public void setPayOnline(BigDecimal payOnline) {
            this.payOnline = payOnline;
        }

        /**
         * 示例值:10
         * 描述:总补贴(平台+商家承担补贴)：平台承担所有补贴+商家承担所有补贴（单位：元）
         */
        private BigDecimal subsidy;

        public BigDecimal getSubsidy() {
            return subsidy;
        }

        public void setSubsidy(BigDecimal subsidy) {
            this.subsidy = subsidy;
        }

        /**
         * 示例值:18002
         * 描述:平台承担补贴(市场费)：平台承担所有补贴（单位：元）
         */
        private BigDecimal jdSubsidy;

        public BigDecimal getJdSubsidy() {
            return jdSubsidy;
        }

        public void setJdSubsidy(BigDecimal jdSubsidy) {
            this.jdSubsidy = jdSubsidy;
        }

        /**
         * 示例值:181
         * 描述:商家承担补贴（货款+运费）：商家承担所有补贴（单位：元）
         */
        private BigDecimal storeSubsidy;

        public BigDecimal getStoreSubsidy() {
            return storeSubsidy;
        }

        public void setStoreSubsidy(BigDecimal storeSubsidy) {
            this.storeSubsidy = storeSubsidy;
        }

        /**
         * 示例值:11
         * 描述:商家自送配送费：配送方式为商家自送，平台需结算给商家的运费（单位：元）
         */
        private BigDecimal storeFreightBill;

        public BigDecimal getStoreFreightBill() {
            return storeFreightBill;
        }

        public void setStoreFreightBill(BigDecimal storeFreightBill) {
            this.storeFreightBill = storeFreightBill;
        }

        /**
         * 示例值:12
         * 描述:总佣金（货款佣金+运费佣金+餐盒费佣金）（可开票）：运费佣金即自配送佣金（单位：元）
         */
        private BigDecimal commissionBill;

        public BigDecimal getCommissionBill() {
            return commissionBill;
        }

        public void setCommissionBill(BigDecimal commissionBill) {
            this.commissionBill = commissionBill;
        }

        /**
         * 示例值:136
         * 描述:取件服务费（开票）（正向单展示商家远距离运费；售后单则展示达达售后运费）（单位：元）
         */
        private BigDecimal pickUpFeeNeedBill;

        public BigDecimal getPickUpFeeNeedBill() {
            return pickUpFeeNeedBill;
        }

        public void setPickUpFeeNeedBill(BigDecimal pickUpFeeNeedBill) {
            this.pickUpFeeNeedBill = pickUpFeeNeedBill;
        }

        /**
         * 示例值:135
         * 描述:取件服务费(不开票)，展示取件费-商家小费，指达达配送时，商家给配送员的小费（单位：元）
         */
        private BigDecimal pickUpFeeNotBill;

        public BigDecimal getPickUpFeeNotBill() {
            return pickUpFeeNotBill;
        }

        public void setPickUpFeeNotBill(BigDecimal pickUpFeeNotBill) {
            this.pickUpFeeNotBill = pickUpFeeNotBill;
        }

        /**
         * 示例值:12
         * 描述:餐盒费（单位：元）
         */
        private BigDecimal packageBill;

        public BigDecimal getPackageBill() {
            return packageBill;
        }

        public void setPackageBill(BigDecimal packageBill) {
            this.packageBill = packageBill;
        }

        /**
         * 示例值:
         * 描述:运费（单位：元）
         */
        private BigDecimal freightBill;

        public BigDecimal getFreightBill() {
            return freightBill;
        }

        public void setFreightBill(BigDecimal freightBill) {
            this.freightBill = freightBill;
        }

        /**
         * 示例值:
         * 描述:对账单生成时间
         */
        private Long createTime;

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        /**
         * 示例值:
         * 描述:商家承担货款补贴（单位：元）
         */
        private BigDecimal paymentSubsidies;

        public BigDecimal getPaymentSubsidies() {
            return paymentSubsidies;
        }

        public void setPaymentSubsidies(BigDecimal paymentSubsidies) {
            this.paymentSubsidies = paymentSubsidies;
        }

        /**
         * 示例值:
         * 描述:商家承担运费补贴（单位：元）
         */
        private BigDecimal storeFreightSubsidy;

        public BigDecimal getStoreFreightSubsidy() {
            return storeFreightSubsidy;
        }

        public void setStoreFreightSubsidy(BigDecimal storeFreightSubsidy) {
            this.storeFreightSubsidy = storeFreightSubsidy;
        }

        /**
         * 示例值:
         * 描述:商家承担小费（单位：元）
         */
        private BigDecimal pickupServiceAmount;

        public BigDecimal getPickupServiceAmount() {
            return pickupServiceAmount;
        }

        public void setPickupServiceAmount(BigDecimal pickupServiceAmount) {
            this.pickupServiceAmount = pickupServiceAmount;
        }

        /**
         * 示例值:
         * 描述:结算完成时间(时间戳单位为秒)
         */
        private Long settleFinishTime;

        public Long getSettleFinishTime() {
            return settleFinishTime;
        }

        public void setSettleFinishTime(Long settleFinishTime) {
            this.settleFinishTime = settleFinishTime;
        }

        /**
         * 示例值:
         * 描述:收付款单号
         */
        private String withdrawId;

        public String getWithdrawId() {
            return withdrawId;
        }

        public void setWithdrawId(String withdrawId) {
            this.withdrawId = withdrawId;
        }

        /**
         * 示例值:
         * 描述:订单渠道(京东到家,到家h5,微信小程序等)
         */
        private String orderSourceName;

        public String getOrderSourceName() {
            return orderSourceName;
        }

        public void setOrderSourceName(String orderSourceName) {
            this.orderSourceName = orderSourceName;
        }

        /**
         * 示例值:
         * 描述:用户支付方式(货到付款,ApplePay,支付宝支付等)
         */
        private String payChannelName;

        public String getPayChannelName() {
            return payChannelName;
        }

        public void setPayChannelName(String payChannelName) {
            this.payChannelName = payChannelName;
        }

        /**
         * 示例值:
         * 描述:线下收款应结（单位：元）
         */
        private BigDecimal offlineSettlementAmount;

        public BigDecimal getOfflineSettlementAmount() {
            return offlineSettlementAmount;
        }

        public void setOfflineSettlementAmount(BigDecimal offlineSettlementAmount) {
            this.offlineSettlementAmount = offlineSettlementAmount;
        }

        /**
         * 示例值:
         * 描述:关联订单号非正向单时才存在如果此单为售后单，则关联单号为正向单。如果是售后申述单，则关联单号为售后单
         */
        private Long relationId;

        public Long getRelationId() {
            return relationId;
        }

        public void setRelationId(Long relationId) {
            this.relationId = relationId;
        }
    }
} 
