package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class JDZBDeliveryResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.jDZBDeliveryData = gson.fromJson(data, JDZBDeliveryData.class);
    }

    private JDZBDeliveryData jDZBDeliveryData;

    public JDZBDeliveryData getJDZBDeliveryData() {
        return jDZBDeliveryData;
    }

    public void setJDZBDeliveryData(JDZBDeliveryData jDZBDeliveryData) {
        this.jDZBDeliveryData = jDZBDeliveryData;
    }

    public static class JDZBDeliveryData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:状态码，0表示成功，非零均表示失败，其中10138表示订单已(申请)取消，10139表示订单已锁定，10132订单已拣货
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:msg
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:detail
         */
        private String detail;

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
