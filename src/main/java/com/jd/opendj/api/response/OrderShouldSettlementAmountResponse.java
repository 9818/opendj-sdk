package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * <p>OrderShouldSettlementAmountData</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/4/15 15:14
 */

public class OrderShouldSettlementAmountResponse extends OpenDjResponse {

    private OrderShouldSettlementAmountData orderShouldSettlementAmountData;

    public OrderShouldSettlementAmountData getOrderShouldSettlementAmountData() {
        return orderShouldSettlementAmountData;
    }

    public void setOrderShouldSettlementAmountData(OrderShouldSettlementAmountData orderShouldSettlementAmountData) {
        this.orderShouldSettlementAmountData = orderShouldSettlementAmountData;
    }

    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.orderShouldSettlementAmountData = gson.fromJson(data, OrderShouldSettlementAmountData.class);
    }

    public static class OrderShouldSettlementAmountData implements Data<OrderShouldSettlementAmount> {
        private String code;
        private String msg;
        private OrderShouldSettlementAmount result;
        private String detail;

        @Override
        public OrderShouldSettlementAmount extractData() {
            return result;
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public OrderShouldSettlementAmount getResult() {
            return result;
        }

        public void setResult(OrderShouldSettlementAmount result) {
            this.result = result;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }
    }




    /**
     * @author dff
     * @date 2020-04-15
     */


    public static class OrderShouldSettlementAmount {
        /**
         *示例值:800000001
         *描述:计费成功的订单编号
         */
        private Long orderId;
        /**
         *示例值:100
         *描述:订单应该结给商家的金额
         */
        private Long settlementAmount;
        /**
         *示例值:4128
         *描述:商家编码
         */
        private String orgCode;
        /**
         *示例值:1100057
         *描述:到家门店编码
         */
        private String stationNo;
        /**
         *示例值:1572594152000
         *描述:订单完成计费的时间
         */
        private Long billTime;
        /**
         *示例值:
         *描述:用户实际支付货款
         */
        private Long userActualPaidGoodsMoney;
        /**
         *示例值:
         *描述:订单货款优惠平台承担
         */
        private Long platOrderGoodsDiscountMoney;
        /**
         *示例值:
         *描述:单品货款优惠平台承担
         */
        private Long platSkuGoodsDiscountMoney;
        /**
         *示例值:
         *描述:订单货款优惠商家承担(不参与结算，字段仅供参考)
         */
        private Long venderOrderGoodsDiscountMoney;
        /**
         *示例值:
         *描述:单品货款优惠商家承担(不参与结算，字段仅供参考)
         */
        private Long venderSkuGoodsDiscountMoney;
        /**
         *示例值:
         *描述:商家承担运费补贴
         */
        private Long venderFreightDiscountMoney;
        /**
         *示例值:
         *描述:货款佣金
         */
        private Long goodsCommission;
        /**
         *示例值:
         *描述:运费佣金
         */
        private Long freightCommission;
        /**
         *示例值:
         *描述:餐盒费佣金
         */
        private Long packageCommission;
        /**
         *示例值:
         *描述:保底佣金补差
         */
        private Long guaranteedCommission;
        /**
         *示例值:
         *描述:商家自配送运费
         */
        private Long venderDeliveryFreight;
        /**
         *示例值:
         *描述:平台承担运费补贴
         */
        private Long platFreightDiscountMoney;
        /**
         *示例值:
         *描述:餐盒费
         */
        private Long packageMoney;
        /**
         *示例值:
         *描述:远距离运费
         */
        private Long distanceFreightMoney;
        /**
         *示例值:
         *描述:商家付达达小费
         */
        private Long venderPaidTips;
        /**
         *示例值:
         *描述:货到付款
         */
        private Long orderCashOnDeliveryMoney;
        /**
         *示例值:
         *描述:礼品卡(不参与结算，仅有礼品卡业务的商家需要，其他商家可忽略)
         */
        private Long orderGiftCardMoney;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Long getSettlementAmount() {
            return settlementAmount;
        }

        public void setSettlementAmount(Long settlementAmount) {
            this.settlementAmount = settlementAmount;
        }

        public String getOrgCode() {
            return orgCode;
        }

        public void setOrgCode(String orgCode) {
            this.orgCode = orgCode;
        }

        public String getStationNo() {
            return stationNo;
        }

        public void setStationNo(String stationNo) {
            this.stationNo = stationNo;
        }

        public Long getBillTime() {
            return billTime;
        }

        public void setBillTime(Long billTime) {
            this.billTime = billTime;
        }

        public Long getUserActualPaidGoodsMoney() {
            return userActualPaidGoodsMoney;
        }

        public void setUserActualPaidGoodsMoney(Long userActualPaidGoodsMoney) {
            this.userActualPaidGoodsMoney = userActualPaidGoodsMoney;
        }

        public Long getPlatOrderGoodsDiscountMoney() {
            return platOrderGoodsDiscountMoney;
        }

        public void setPlatOrderGoodsDiscountMoney(Long platOrderGoodsDiscountMoney) {
            this.platOrderGoodsDiscountMoney = platOrderGoodsDiscountMoney;
        }

        public Long getPlatSkuGoodsDiscountMoney() {
            return platSkuGoodsDiscountMoney;
        }

        public void setPlatSkuGoodsDiscountMoney(Long platSkuGoodsDiscountMoney) {
            this.platSkuGoodsDiscountMoney = platSkuGoodsDiscountMoney;
        }

        public Long getVenderOrderGoodsDiscountMoney() {
            return venderOrderGoodsDiscountMoney;
        }

        public void setVenderOrderGoodsDiscountMoney(Long venderOrderGoodsDiscountMoney) {
            this.venderOrderGoodsDiscountMoney = venderOrderGoodsDiscountMoney;
        }

        public Long getVenderSkuGoodsDiscountMoney() {
            return venderSkuGoodsDiscountMoney;
        }

        public void setVenderSkuGoodsDiscountMoney(Long venderSkuGoodsDiscountMoney) {
            this.venderSkuGoodsDiscountMoney = venderSkuGoodsDiscountMoney;
        }

        public Long getVenderFreightDiscountMoney() {
            return venderFreightDiscountMoney;
        }

        public void setVenderFreightDiscountMoney(Long venderFreightDiscountMoney) {
            this.venderFreightDiscountMoney = venderFreightDiscountMoney;
        }

        public Long getGoodsCommission() {
            return goodsCommission;
        }

        public void setGoodsCommission(Long goodsCommission) {
            this.goodsCommission = goodsCommission;
        }

        public Long getFreightCommission() {
            return freightCommission;
        }

        public void setFreightCommission(Long freightCommission) {
            this.freightCommission = freightCommission;
        }

        public Long getPackageCommission() {
            return packageCommission;
        }

        public void setPackageCommission(Long packageCommission) {
            this.packageCommission = packageCommission;
        }

        public Long getGuaranteedCommission() {
            return guaranteedCommission;
        }

        public void setGuaranteedCommission(Long guaranteedCommission) {
            this.guaranteedCommission = guaranteedCommission;
        }

        public Long getVenderDeliveryFreight() {
            return venderDeliveryFreight;
        }

        public void setVenderDeliveryFreight(Long venderDeliveryFreight) {
            this.venderDeliveryFreight = venderDeliveryFreight;
        }

        public Long getPlatFreightDiscountMoney() {
            return platFreightDiscountMoney;
        }

        public void setPlatFreightDiscountMoney(Long platFreightDiscountMoney) {
            this.platFreightDiscountMoney = platFreightDiscountMoney;
        }

        public Long getPackageMoney() {
            return packageMoney;
        }

        public void setPackageMoney(Long packageMoney) {
            this.packageMoney = packageMoney;
        }

        public Long getDistanceFreightMoney() {
            return distanceFreightMoney;
        }

        public void setDistanceFreightMoney(Long distanceFreightMoney) {
            this.distanceFreightMoney = distanceFreightMoney;
        }

        public Long getVenderPaidTips() {
            return venderPaidTips;
        }

        public void setVenderPaidTips(Long venderPaidTips) {
            this.venderPaidTips = venderPaidTips;
        }

        public Long getOrderCashOnDeliveryMoney() {
            return orderCashOnDeliveryMoney;
        }

        public void setOrderCashOnDeliveryMoney(Long orderCashOnDeliveryMoney) {
            this.orderCashOnDeliveryMoney = orderCashOnDeliveryMoney;
        }

        public Long getOrderGiftCardMoney() {
            return orderGiftCardMoney;
        }

        public void setOrderGiftCardMoney(Long orderGiftCardMoney) {
            this.orderGiftCardMoney = orderGiftCardMoney;
        }
    }

}
