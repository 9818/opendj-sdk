package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class ImageUploadResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.imageUploadData = gson.fromJson(data, ImageUploadData.class);
    }

    private ImageUploadData imageUploadData;

    public ImageUploadData getImageUploadData() {
        return imageUploadData;
    }

    public void setImageUploadData(ImageUploadData imageUploadData) {
        this.imageUploadData = imageUploadData;
    }

    public static class ImageUploadData implements Data<String> {
        /**
         * 示例值:0
         * 描述:返回码（0：成功，9000001：图片上传异常，9000002：图像数据为空，9000003：上传图片超出最大限制，9000004：上传图片服务器异常，9000005：上传图片格式不支持，9000006：上传图片base64数据有问题）
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:描述
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:上传服务器的图片路径
         */
        private String imageUrl;

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        @Override
        public String extractData() {
            return imageUrl;
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
