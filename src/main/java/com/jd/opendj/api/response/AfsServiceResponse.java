package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.opendj.api.OpenDjResponse;
import com.jd.opendj.api.response.adapter.DateAdapter;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Date;
import java.util.List;

/**
 * <p>AfsServiceData</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/1/19 16:39
 */


public class AfsServiceResponse extends OpenDjResponse {

    private AfsServiceData afsServiceData;

    public AfsServiceData getAfsServiceData() {
        return afsServiceData;
    }

    public void setAfsServiceData(AfsServiceData afsServiceData) {
        this.afsServiceData = afsServiceData;
    }

    @Override
    public void toJavaObject() {
        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class,new DateAdapter())
                .create();
        this.afsServiceData = gson.fromJson(data, AfsServiceData.class);
    }

    public static class AfsServiceData implements Data<AfsServiceResult> {
        private Integer code;
        private String msg;
        private Boolean success;
        private AfsServiceResult result;

        @Override
        public AfsServiceResult extractData() {
            return result;
        }

        @Override
        public boolean isSuccess() {
            return BooleanUtils.toBoolean(success);
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public AfsServiceResult getResult() {
            return result;
        }

        public void setResult(AfsServiceResult result) {
            this.result = result;
        }
    }


    /**
     * @author dengfengfeng
     * @date 2020-01-07
     */

    public static class AfsServiceResult {
        /**
         *示例值:100001035999127
         *描述:原订单号
         */
        private String orderId;
        /**
         *示例值:200001035999127
         *描述:新订单号
         */
        private String newOrderId;
        /**
         *示例值:20043683
         *描述:售后单号
         */
        private String afsServiceOrder;
        /**
         *示例值:10
         *描述:售后单状态（10:待审核,20:待取件,30:退款处理中,31:待商家收货审核,32:退款成功,33:退款失败,40:审核不通过-驳回,50:客户取消,60:商家收货审核不通过,70:已解决,91:直赔,92:直赔成功,93:直赔失败,90:待赔付,110:待退货,111:取货成功,1101取货中,1111退货成功-商品已送至门店,1112退货成功-商家已确认收货112:退货成功-待退款,113:退货失败,114:退货成功）
         */
        private Integer afsServiceState;
        /**
         *示例值:1572594152000
         *描述:审核时间
         */
        private Long approvedDate;
        /**
         *示例值:erp_yangliu25
         *描述:审核人
         */
        private String approvePin;
        /**
         *示例值:201
         *描述:申请售后原因（201:商品质量问题，202:送错货，203:缺件少件，501:全部商品未收到，208:包装脏污有破损，207:缺斤少两，210:商家通知我缺货，303:实物与原图不符，402:不想要了，502:未在时效内送达）
         */
        private Integer questionTypeCid;
        /**
         *示例值:货物坏掉了
         *描述:问题描述（字数限制：400字）
         */
        private String questionDesc;
        /**
         *示例值:多个图片之间用英文逗号隔开，解析后，在图片链接前加http://img10.360buyimg.com/o2o/即可访问图片
         *描述:问题图片列表（最多5张）
         */
        private String questionPic;
        /**
         *示例值:SJ_24dm180e8aea0
         *描述:下单顾客账号（到家账号）
         */
        private String customerPin;
        /**
         *示例值:取香蕉
         *描述:取货信息（发错货时填写的取货信息）
         */
        private String pickupDetail;
        /**
         *示例值:陈宇
         *描述:客户姓名
         */
        private String customerName;
        /**
         *示例值:13990691000
         *描述:客户电话
         */
        private String customerMobilePhone;
        /**
         *示例值:北京市海淀区中关村东路18号
         *描述:客户地址
         */
        private String pickwareAddress;
        /**
         *示例值:9966
         *描述:承运商（9966：众包配送，2938：商家自送,1130:达达同城送）
         */
        private String carriersNo;
        /**
         *示例值:ON000628719523
         *描述:运单号（承运商为众包时的退货单才有；承运商为商家自送时没有）
         */
        private String deliveryNo;
        /**
         *示例值:10342
         *描述:商家门店编码
         */
        private String stationNumOutSystem;
        /**
         *示例值:10004047
         *描述:到家门店编码
         */
        private String stationId;
        /**
         *示例值:八达岭便利店
         *描述:到家门店名称
         */
        private String stationName;
        /**
         *示例值:1572594152000
         *描述:预计送达时间结束
         */
        private Long pickupEndTime;
        /**
         *示例值:1572594152000
         *描述:预计送达时间开始
         */
        private Long pickupStartTime;
        /**
         *示例值:120
         *描述:订单时效（分钟）
         */
        private Integer orderAging;
        /**
         *示例值:1200
         *描述:应退商品金额（单位为分，应退商品金额(实际用户支付金额)=售后单原始总金额-京豆金额-优惠总金额-到家积分金额）
         */
        private Long cashMoney;
        /**
         *示例值:4
         *描述:原订单支付方式（1:货到付款,2:邮局汇款,3:混合支付,4:在线支付,5:公司转账）
         */
        private Integer payType;
        /**
         *示例值:1300
         *描述:应退商品金额+应退积分金额（单位为分，售后单原始总金额-京豆金额-优惠总金额）
         */
        private Long afsMoney;
        /**
         *示例值:100
         *描述:售后单实际退用户京豆支付金额（单位为分）
         */
        private Long jdBeansMoney;
        /**
         *示例值:0
         *描述:售后单总优惠金额（单位为分，售后单维度的总优惠金额=SKU维度总优惠金额相加）
         */
        private Long virtualMoney;
        /**
         *示例值:40
         *描述:售后单类型（10:仅退款,30:直赔，40:退货退款），仅退款/退货退款由用户发起，直赔由商家发起
         */
        private String applyDeal;
        /**
         *示例值:1
         *描述:售后发起方（10:客服，20:用户APP，40:商家，50:用户H5，60:用户RN）
         */
        private Integer orderSource;
        /**
         *示例值:1
         *描述:责任承担方（1:京东到家，2:商家，3:物流，4:客户，5:其他）
         */
        private Integer dutyAssume;
        /**
         *示例值:1
         *描述:审核方（1到家客服，2商家，3商家超时自动审核，4自动取消，5平台自动审核，6客服超时自动审核）
         */
        private Integer approveType;
        /**
         *示例值:90000
         *描述:原订单状态，指原订单申请售后时的状态
         */
        private Integer orderStatus;
        /**
         *示例值:0
         *描述:运单状态，指众包配送的退货单的运单状态(0待抢单,-1取消,10已抢单,20配送中,30已妥投);
         */
        private String deliveryState;
        /**
         *示例值:张三
         *描述:配送员姓名，商家自送时没有
         */
        private String deliveryMan;
        /**
         *示例值:15001300000
         *描述:配送员电话，商家自送时没有
         */
        private String deliveryMobile;
        /**
         *示例值:ON000628719523
         *描述:配送员编号，商家自送时没有
         */
        private String deliveryManNo;
        /**
         *示例值:10000
         *描述:原订单类型（10000:从门店出的订单）
         */
        private String orderType;
        /**
         *示例值:
         *描述:商品列表
         */
        private List<AfsServiceDetail> afsDetailList;
        /**
         *示例值:1572594152000
         *描述:创建时间
         */
        private Long createTime;
        /**
         *示例值:1572594152000
         *描述:更新时间
         */
        private Long updateTime;
        /**
         *示例值:
         *描述:运费(退货取件费)；在售后单为达达同城送订单时，该字段为0
         */
        private Long afsFreight;
        /**
         *示例值:1
         *描述:应退运费总金额(用户实际支付的运费金额)
         */
        private Long orderFreightMoney;
        /**
         *示例值:5
         *描述:订单应收运费(未优惠前应付运费)
         */
        private Long orderReceivableFreight;
        /**
         *示例值:10
         *描述:应退到家积分金额
         */
        private Long platformIntegralDeductMoney;
        /**
         *示例值:10
         *描述:应退订单餐盒费
         */
        private Long mealBoxMoney;
        /**
         *示例值:50
         *描述:应退包装费金额
         */
        private Long packagingMoney;
        /**
         *示例值:500
         *描述:退货单产生的同城送费用
         */
        private Integer tongchengFreightMoney;
        /**
         *示例值:系统异常
         *描述:同城送失败转商家自送提示文案
         */
        private String changeDeliveryReason;
        /**
         *示例值:1
         *描述:是否是最后一单,1代表最后一次申请,可以根据这个字段判断是部分退款还是整单退款，1表示整单退完。
         * 是否是最后一单,1代表最后一次申请,可以根据这个字段判断是部分退款还是整单退款，1表示整单退完。
         * 判断是否整单退款，还需要结合afsServiceState中的状态，只有afsServiceState=32或114，且isLastApply=1才表示原订单整单退完。
         * 例如：一个10元订单，分三次退款，第一次3元，第二次3元，第三次4元，每个售后单都是部分退款，但是第三次退款完成后，isLastApply=1,表示正向订单已经彻底退完
         */
        private Integer isLastApply;
        /**
         *示例值:1
         *描述:订单业务类型(1：京东到家商超，2：京东到家美食，4：京东到家开放仓，5：哥伦布店内订单，6：货柜订单，8：轻松购订单，9：是自助收银，10：超级会员码)
         */
        private Integer businessType;
        /**
         *示例值:1
         *描述:用户责任：1用户责任0非用户责任
         */
        private Integer userDutyType;
        /**
         *示例值:1
         *描述:礼品卡抵扣-餐盒费
         */
        private Long giftCardMealBoxMoney;
        /**
         *示例值:1
         *描述:礼品卡货款
         */
        private Long giftCardProductMoney;
        /**
         *示例值:1
         *描述:礼品卡包装费
         */
        private Long giftCardPackagingMoney;
        /**
         *示例值:1
         *描述:礼品卡餐盒费
         */
        private Long giftCardFreightMoney;
        /**
         *示例值:1
         *描述:京东货款计费，json字符串，需要转换成Map，具体key：
         */
        private String jdProductBillInfo;
        /**
         *示例值:
         *描述:京东运费计费，json字符串，需要转换成Map，具体key:
         */
        private String jdFreightBillInfo;
        /**
         *示例值:
         *描述:售后申请来源：10：到家售后单，20：京东售后单
         */
        private Integer afsOrderType;
        /**
         *示例值:
         *描述:外部售后单号
         */
        private String externalServiceOrder;
        /**
         *示例值:
         *描述:外部订单号
         */
        private String externalOrderId;
        /**
         *示例值:1
         *描述:在线支付金额
         */
        private Long externalOnlinePayment;
        /**
         *示例值:1
         *描述:京券金额
         */
        private Long externalJingCoupon;
        /**
         *示例值:
         *描述:东券金额
         */
        private Long externalDongCoupon;
        /**
         *示例值:
         *描述:京豆金额
         */
        private Long externalJingDou;
        /**
         *示例值:
         *描述:其他支付金额
         */
        private Long externalOtherPayment;
        /**
         *示例值:
         *描述:退款总金额
         */
        private Long refundUserMmoney;

        private Date ts;

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getNewOrderId() {
            return newOrderId;
        }

        public void setNewOrderId(String newOrderId) {
            this.newOrderId = newOrderId;
        }

        public String getAfsServiceOrder() {
            return afsServiceOrder;
        }

        public void setAfsServiceOrder(String afsServiceOrder) {
            this.afsServiceOrder = afsServiceOrder;
        }

        public Integer getAfsServiceState() {
            return afsServiceState;
        }

        public void setAfsServiceState(Integer afsServiceState) {
            this.afsServiceState = afsServiceState;
        }

        public Long getApprovedDate() {
            return approvedDate;
        }

        public void setApprovedDate(Long approvedDate) {
            this.approvedDate = approvedDate;
        }

        public String getApprovePin() {
            return approvePin;
        }

        public void setApprovePin(String approvePin) {
            this.approvePin = approvePin;
        }

        public Integer getQuestionTypeCid() {
            return questionTypeCid;
        }

        public void setQuestionTypeCid(Integer questionTypeCid) {
            this.questionTypeCid = questionTypeCid;
        }

        public String getQuestionDesc() {
            return questionDesc;
        }

        public void setQuestionDesc(String questionDesc) {
            this.questionDesc = questionDesc;
        }

        public String getQuestionPic() {
            return questionPic;
        }

        public void setQuestionPic(String questionPic) {
            this.questionPic = questionPic;
        }

        public String getCustomerPin() {
            return customerPin;
        }

        public void setCustomerPin(String customerPin) {
            this.customerPin = customerPin;
        }

        public String getPickupDetail() {
            return pickupDetail;
        }

        public void setPickupDetail(String pickupDetail) {
            this.pickupDetail = pickupDetail;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public String getCustomerMobilePhone() {
            return customerMobilePhone;
        }

        public void setCustomerMobilePhone(String customerMobilePhone) {
            this.customerMobilePhone = customerMobilePhone;
        }

        public String getPickwareAddress() {
            return pickwareAddress;
        }

        public void setPickwareAddress(String pickwareAddress) {
            this.pickwareAddress = pickwareAddress;
        }

        public String getCarriersNo() {
            return carriersNo;
        }

        public void setCarriersNo(String carriersNo) {
            this.carriersNo = carriersNo;
        }

        public String getDeliveryNo() {
            return deliveryNo;
        }

        public void setDeliveryNo(String deliveryNo) {
            this.deliveryNo = deliveryNo;
        }

        public String getStationNumOutSystem() {
            return stationNumOutSystem;
        }

        public void setStationNumOutSystem(String stationNumOutSystem) {
            this.stationNumOutSystem = stationNumOutSystem;
        }

        public String getStationId() {
            return stationId;
        }

        public void setStationId(String stationId) {
            this.stationId = stationId;
        }

        public String getStationName() {
            return stationName;
        }

        public void setStationName(String stationName) {
            this.stationName = stationName;
        }

        public Long getPickupEndTime() {
            return pickupEndTime;
        }

        public void setPickupEndTime(Long pickupEndTime) {
            this.pickupEndTime = pickupEndTime;
        }

        public Long getPickupStartTime() {
            return pickupStartTime;
        }

        public void setPickupStartTime(Long pickupStartTime) {
            this.pickupStartTime = pickupStartTime;
        }

        public Integer getOrderAging() {
            return orderAging;
        }

        public void setOrderAging(Integer orderAging) {
            this.orderAging = orderAging;
        }

        public Long getCashMoney() {
            return cashMoney;
        }

        public void setCashMoney(Long cashMoney) {
            this.cashMoney = cashMoney;
        }

        public Integer getPayType() {
            return payType;
        }

        public void setPayType(Integer payType) {
            this.payType = payType;
        }

        public Long getAfsMoney() {
            return afsMoney;
        }

        public void setAfsMoney(Long afsMoney) {
            this.afsMoney = afsMoney;
        }

        public Long getJdBeansMoney() {
            return jdBeansMoney;
        }

        public void setJdBeansMoney(Long jdBeansMoney) {
            this.jdBeansMoney = jdBeansMoney;
        }

        public Long getVirtualMoney() {
            return virtualMoney;
        }

        public void setVirtualMoney(Long virtualMoney) {
            this.virtualMoney = virtualMoney;
        }

        public String getApplyDeal() {
            return applyDeal;
        }

        public void setApplyDeal(String applyDeal) {
            this.applyDeal = applyDeal;
        }

        public Integer getOrderSource() {
            return orderSource;
        }

        public void setOrderSource(Integer orderSource) {
            this.orderSource = orderSource;
        }

        public Integer getDutyAssume() {
            return dutyAssume;
        }

        public void setDutyAssume(Integer dutyAssume) {
            this.dutyAssume = dutyAssume;
        }

        public Integer getApproveType() {
            return approveType;
        }

        public void setApproveType(Integer approveType) {
            this.approveType = approveType;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getDeliveryState() {
            return deliveryState;
        }

        public void setDeliveryState(String deliveryState) {
            this.deliveryState = deliveryState;
        }

        public String getDeliveryMan() {
            return deliveryMan;
        }

        public void setDeliveryMan(String deliveryMan) {
            this.deliveryMan = deliveryMan;
        }

        public String getDeliveryMobile() {
            return deliveryMobile;
        }

        public void setDeliveryMobile(String deliveryMobile) {
            this.deliveryMobile = deliveryMobile;
        }

        public String getDeliveryManNo() {
            return deliveryManNo;
        }

        public void setDeliveryManNo(String deliveryManNo) {
            this.deliveryManNo = deliveryManNo;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public List<AfsServiceDetail> getAfsDetailList() {
            return afsDetailList;
        }

        public void setAfsDetailList(List<AfsServiceDetail> afsDetailList) {
            this.afsDetailList = afsDetailList;
        }

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        public Long getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Long updateTime) {
            this.updateTime = updateTime;
        }

        public Long getAfsFreight() {
            return afsFreight;
        }

        public void setAfsFreight(Long afsFreight) {
            this.afsFreight = afsFreight;
        }

        public Long getOrderFreightMoney() {
            return orderFreightMoney;
        }

        public void setOrderFreightMoney(Long orderFreightMoney) {
            this.orderFreightMoney = orderFreightMoney;
        }

        public Long getOrderReceivableFreight() {
            return orderReceivableFreight;
        }

        public void setOrderReceivableFreight(Long orderReceivableFreight) {
            this.orderReceivableFreight = orderReceivableFreight;
        }

        public Long getPlatformIntegralDeductMoney() {
            return platformIntegralDeductMoney;
        }

        public void setPlatformIntegralDeductMoney(Long platformIntegralDeductMoney) {
            this.platformIntegralDeductMoney = platformIntegralDeductMoney;
        }

        public Long getMealBoxMoney() {
            return mealBoxMoney;
        }

        public void setMealBoxMoney(Long mealBoxMoney) {
            this.mealBoxMoney = mealBoxMoney;
        }

        public Long getPackagingMoney() {
            return packagingMoney;
        }

        public void setPackagingMoney(Long packagingMoney) {
            this.packagingMoney = packagingMoney;
        }

        public Integer getTongchengFreightMoney() {
            return tongchengFreightMoney;
        }

        public void setTongchengFreightMoney(Integer tongchengFreightMoney) {
            this.tongchengFreightMoney = tongchengFreightMoney;
        }

        public String getChangeDeliveryReason() {
            return changeDeliveryReason;
        }

        public void setChangeDeliveryReason(String changeDeliveryReason) {
            this.changeDeliveryReason = changeDeliveryReason;
        }

        public Integer getIsLastApply() {
            return isLastApply;
        }

        public void setIsLastApply(Integer isLastApply) {
            this.isLastApply = isLastApply;
        }

        public Integer getBusinessType() {
            return businessType;
        }

        public void setBusinessType(Integer businessType) {
            this.businessType = businessType;
        }

        public Integer getUserDutyType() {
            return userDutyType;
        }

        public void setUserDutyType(Integer userDutyType) {
            this.userDutyType = userDutyType;
        }

        public Long getGiftCardMealBoxMoney() {
            return giftCardMealBoxMoney;
        }

        public void setGiftCardMealBoxMoney(Long giftCardMealBoxMoney) {
            this.giftCardMealBoxMoney = giftCardMealBoxMoney;
        }

        public Long getGiftCardProductMoney() {
            return giftCardProductMoney;
        }

        public void setGiftCardProductMoney(Long giftCardProductMoney) {
            this.giftCardProductMoney = giftCardProductMoney;
        }

        public Long getGiftCardPackagingMoney() {
            return giftCardPackagingMoney;
        }

        public void setGiftCardPackagingMoney(Long giftCardPackagingMoney) {
            this.giftCardPackagingMoney = giftCardPackagingMoney;
        }

        public Long getGiftCardFreightMoney() {
            return giftCardFreightMoney;
        }

        public void setGiftCardFreightMoney(Long giftCardFreightMoney) {
            this.giftCardFreightMoney = giftCardFreightMoney;
        }

        public String getJdProductBillInfo() {
            return jdProductBillInfo;
        }

        public void setJdProductBillInfo(String jdProductBillInfo) {
            this.jdProductBillInfo = jdProductBillInfo;
        }

        public String getJdFreightBillInfo() {
            return jdFreightBillInfo;
        }

        public void setJdFreightBillInfo(String jdFreightBillInfo) {
            this.jdFreightBillInfo = jdFreightBillInfo;
        }

        public Integer getAfsOrderType() {
            return afsOrderType;
        }

        public void setAfsOrderType(Integer afsOrderType) {
            this.afsOrderType = afsOrderType;
        }

        public String getExternalServiceOrder() {
            return externalServiceOrder;
        }

        public void setExternalServiceOrder(String externalServiceOrder) {
            this.externalServiceOrder = externalServiceOrder;
        }

        public String getExternalOrderId() {
            return externalOrderId;
        }

        public void setExternalOrderId(String externalOrderId) {
            this.externalOrderId = externalOrderId;
        }

        public Long getExternalOnlinePayment() {
            return externalOnlinePayment;
        }

        public void setExternalOnlinePayment(Long externalOnlinePayment) {
            this.externalOnlinePayment = externalOnlinePayment;
        }

        public Long getExternalJingCoupon() {
            return externalJingCoupon;
        }

        public void setExternalJingCoupon(Long externalJingCoupon) {
            this.externalJingCoupon = externalJingCoupon;
        }

        public Long getExternalDongCoupon() {
            return externalDongCoupon;
        }

        public void setExternalDongCoupon(Long externalDongCoupon) {
            this.externalDongCoupon = externalDongCoupon;
        }

        public Long getExternalJingDou() {
            return externalJingDou;
        }

        public void setExternalJingDou(Long externalJingDou) {
            this.externalJingDou = externalJingDou;
        }

        public Long getExternalOtherPayment() {
            return externalOtherPayment;
        }

        public void setExternalOtherPayment(Long externalOtherPayment) {
            this.externalOtherPayment = externalOtherPayment;
        }

        public Long getRefundUserMmoney() {
            return refundUserMmoney;
        }

        public void setRefundUserMmoney(Long refundUserMmoney) {
            this.refundUserMmoney = refundUserMmoney;
        }

        public Date getTs() {
            return ts;
        }

        public void setTs(Date ts) {
            this.ts = ts;
        }


    }

    /**
     * @author dengfengfeng
     * @date 2020-01-07
     */

    public static class AfsServiceDetail {
        /**
         *示例值:2001048759
         *描述:到家商品编码
         */
        private Long wareId ;
        /**
         *示例值:娃娃菜-罗鲜生-约750g/棵
         *描述:到家商品名称
         */
        private String wareName ;
        /**
         *示例值:深蓝色;XS
         *描述:商品规格，多规格之间用英文分号;分隔
         */
        private String skuSpecification ;
        /**
         *示例值:1001
         *描述:商家商品编码
         */
        private String skuIdIsv ;
        /**
         *示例值:2300445000008
         *描述:商家商品UPC码
         */
        private String upcCode ;
        /**
         *示例值:5
         *描述:单个商品重量
         */
        private Double weight ;
        /**
         *示例值:500
         *描述:单价（该SKU单价）
         */
        private Long payPrice ;
        /**
         *示例值:3
         *描述:申请数量（该SKU本次申请售后个数）
         */
        private Integer skuCount ;
        /**
         *示例值:1500
         *描述:该sku该售后单原始总金额（skuMoney=payPrice*skuCount）
         */
        private Long skuMoney ;
        /**
         *示例值:1500
         *描述:该商品餐盒费金额*申请数量
         */
        private Long mealBoxMoney ;
        /**
         *示例值:1500
         *描述:应退商品金额+应退积分金额（afsMoney=skuMoney-virtualMoney）
         */
        private Long afsMoney ;
        /**
         *示例值:1500
         *描述:应退商品金额（cashMoney=afsMoney-jdBeansMoney）
         */
        private Long cashMoney ;
        /**
         *示例值:0
         *描述:该sku此次售后实际退用户京豆支付金额
         */
        private Long jdBeansMoney ;
        /**
         *示例值:0
         *描述:该SKU实际售后个数总优惠金额，等于该sku维度满减优惠金额+首单优惠金额+优惠券/码优惠金额总和
         */
        private Long virtualMoney ;
        /**
         *示例值:0
         *描述:该sku此次售后实际退用户积分支付金额
         */
        private Long platformIntegralDeductMoney ;
        /**
         *示例值:1
         *描述:商品级别促销类型(1、无优惠;2、秒杀(已经下线);3、单品直降;4、限时抢购;1202、加价购;1203、满赠(标识商品);6、买赠(买A送B，标识B);9999、表示一个普通商品参与捆绑促销，设置的捆绑类型;9998、表示一个商品参与了捆绑促销，并且还参与了其他促销类型;9997、表示一个商品参与了捆绑促销，但是金额拆分不尽,9996:组合购,8001:商家会员价,8:第二件N折)
         */
        private Integer promotionType ;
        /**
         *示例值:100
         *描述:单品优惠商家承担金额
         */
        private Long venderPayMoney ;
        /**
         *示例值:0
         *描述:单品优惠平台承担金额
         */
        private Long platPayMoney ;
        /**
         *示例值:
         *描述:订单级优惠列表
         */
        private List<AfsSkuDiscount> afsSkuDiscountList ;

        public Long getWareId() {
            return wareId;
        }

        public void setWareId(Long wareId) {
            this.wareId = wareId;
        }

        public String getWareName() {
            return wareName;
        }

        public void setWareName(String wareName) {
            this.wareName = wareName;
        }

        public String getSkuSpecification() {
            return skuSpecification;
        }

        public void setSkuSpecification(String skuSpecification) {
            this.skuSpecification = skuSpecification;
        }

        public String getSkuIdIsv() {
            return skuIdIsv;
        }

        public void setSkuIdIsv(String skuIdIsv) {
            this.skuIdIsv = skuIdIsv;
        }

        public String getUpcCode() {
            return upcCode;
        }

        public void setUpcCode(String upcCode) {
            this.upcCode = upcCode;
        }

        public Double getWeight() {
            return weight;
        }

        public void setWeight(Double weight) {
            this.weight = weight;
        }

        public Long getPayPrice() {
            return payPrice;
        }

        public void setPayPrice(Long payPrice) {
            this.payPrice = payPrice;
        }

        public Integer getSkuCount() {
            return skuCount;
        }

        public void setSkuCount(Integer skuCount) {
            this.skuCount = skuCount;
        }

        public Long getSkuMoney() {
            return skuMoney;
        }

        public void setSkuMoney(Long skuMoney) {
            this.skuMoney = skuMoney;
        }

        public Long getMealBoxMoney() {
            return mealBoxMoney;
        }

        public void setMealBoxMoney(Long mealBoxMoney) {
            this.mealBoxMoney = mealBoxMoney;
        }

        public Long getAfsMoney() {
            return afsMoney;
        }

        public void setAfsMoney(Long afsMoney) {
            this.afsMoney = afsMoney;
        }

        public Long getCashMoney() {
            return cashMoney;
        }

        public void setCashMoney(Long cashMoney) {
            this.cashMoney = cashMoney;
        }

        public Long getJdBeansMoney() {
            return jdBeansMoney;
        }

        public void setJdBeansMoney(Long jdBeansMoney) {
            this.jdBeansMoney = jdBeansMoney;
        }

        public Long getVirtualMoney() {
            return virtualMoney;
        }

        public void setVirtualMoney(Long virtualMoney) {
            this.virtualMoney = virtualMoney;
        }

        public Long getPlatformIntegralDeductMoney() {
            return platformIntegralDeductMoney;
        }

        public void setPlatformIntegralDeductMoney(Long platformIntegralDeductMoney) {
            this.platformIntegralDeductMoney = platformIntegralDeductMoney;
        }

        public Integer getPromotionType() {
            return promotionType;
        }

        public void setPromotionType(Integer promotionType) {
            this.promotionType = promotionType;
        }

        public Long getVenderPayMoney() {
            return venderPayMoney;
        }

        public void setVenderPayMoney(Long venderPayMoney) {
            this.venderPayMoney = venderPayMoney;
        }

        public Long getPlatPayMoney() {
            return platPayMoney;
        }

        public void setPlatPayMoney(Long platPayMoney) {
            this.platPayMoney = platPayMoney;
        }

        public List<AfsSkuDiscount> getAfsSkuDiscountList() {
            return afsSkuDiscountList;
        }

        public void setAfsSkuDiscountList(List<AfsSkuDiscount> afsSkuDiscountList) {
            this.afsSkuDiscountList = afsSkuDiscountList;
        }
    }

    /**
     * @author dengfengfeng
     * @date 2020-01-07
     */

    public static class AfsSkuDiscount {
        /**
         *示例值:1
         *描述:一级优惠类型(1:优惠码;3:优惠劵;4:满减;5:满折;6:首单优惠;7:VIP免运费;8:商家满免运费;10:满件减;11:满件折;12:首单地推满免运费)
         */
        private Integer discountType ;
        /**
         *示例值:1
         *描述:二级优惠类型(优惠码(1:满减;2:立减;3:满折);优惠券(1:满减;2:立减;3:免运费劵;4:运费优惠N元;5满折);满件减(1206:满件减);满件折(1207:满件折))
         */
        private Integer detailDiscountType ;
        /**
         *示例值:100
         *描述:订单优惠商家承担金额
         */
        private Long venderPayMoney ;
        /**
         *示例值:0
         *描述:订单优惠平台承担金额
         */
        private Long platPayMoney ;

        public Integer getDiscountType() {
            return discountType;
        }

        public void setDiscountType(Integer discountType) {
            this.discountType = discountType;
        }

        public Integer getDetailDiscountType() {
            return detailDiscountType;
        }

        public void setDetailDiscountType(Integer detailDiscountType) {
            this.detailDiscountType = detailDiscountType;
        }

        public Long getVenderPayMoney() {
            return venderPayMoney;
        }

        public void setVenderPayMoney(Long venderPayMoney) {
            this.venderPayMoney = venderPayMoney;
        }

        public Long getPlatPayMoney() {
            return platPayMoney;
        }

        public void setPlatPayMoney(Long platPayMoney) {
            this.platPayMoney = platPayMoney;
        }
    }

}
