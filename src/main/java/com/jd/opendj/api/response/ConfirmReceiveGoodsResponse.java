package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class ConfirmReceiveGoodsResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.confirmReceiveGoodsData = gson.fromJson(data, ConfirmReceiveGoodsData.class);
    }

    private ConfirmReceiveGoodsData confirmReceiveGoodsData;

    public ConfirmReceiveGoodsData getConfirmReceiveGoodsData() {
        return confirmReceiveGoodsData;
    }

    public void setConfirmReceiveGoodsData(ConfirmReceiveGoodsData confirmReceiveGoodsData) {
        this.confirmReceiveGoodsData = confirmReceiveGoodsData;
    }

    public static class ConfirmReceiveGoodsData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:code，-1:失败0:成功
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:msg
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:detail
         */
        private String detail;

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        /**
         * 示例值:true
         * 描述:success
         */
        private String success;

        public String getSuccess() {
            return success;
        }

        public void setSuccess(String success) {
            this.success = success;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
