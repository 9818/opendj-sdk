package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.opendj.api.OpenDjResponse;
import com.jd.opendj.api.response.adapter.DateAdapter;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Date;
import java.util.List;

/**
 * @author dff
 * @date 2020-09-16
 */
public class PromotionInfoResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
		final Gson gson = new GsonBuilder()
				.registerTypeAdapter(Date.class,new DateAdapter())
				.create();
        this.promotionInfoData = gson.fromJson(data, PromotionInfoData.class);
    }

    private PromotionInfoData promotionInfoData;

    public PromotionInfoData getPromotionInfoData() {
        return promotionInfoData;
    }

    public void setPromotionInfoData(PromotionInfoData promotionInfoData) {
        this.promotionInfoData = promotionInfoData;
    }

    public static class PromotionInfoData implements Data<PromotionLspQueryInfoResult> {
        /**
         * 示例值:true
         * 描述:是否成功
         */
        private Boolean success;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        /**
         * 示例值:10000
         * 描述:状态码，0:成功，000001:失败，000002:警告，000003:系统错误，000004:参数错误，000005:属性拷贝错误，000006:skuList为空错误，000007:促销%s已结束，000008:参数%s为空或不合法，000010:操作过于频繁，请稍后重试，000011:获取商品价格失败，可能价格未维护，000012:系统id错误,或未维护当前系统id，000013:活动唯一码:%s错误，未查询到当前活动码，000014:商品数量超过限制数量%s，000015:未匹配到商品信息，000016:商品上传失败，000017:未查询到促销信息，000018:该账号无权限操作，000019:赠品信息已发生变化，000020:赠品数量超过限制个数[3]，000021:活动%s下没有有效商品信息，000022:活动%s下存在多条相同的商品
         */
        private String errorCode;

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        /**
         * 示例值:活动编码无效
         * 描述:失败描述信息
         */
        private String errorInfos;

        public String getErrorInfos() {
            return errorInfos;
        }

        public void setErrorInfos(String errorInfos) {
            this.errorInfos = errorInfos;
        }

        /**
         * 示例值:
         * 描述:返回结果
         */
        private PromotionLspQueryInfoResult promotionLspQueryInfoResult;

        public PromotionLspQueryInfoResult getPromotionLspQueryInfoResult() {
            return promotionLspQueryInfoResult;
        }

        public void setPromotionLspQueryInfoResult(PromotionLspQueryInfoResult promotionLspQueryInfoResult) {
            this.promotionLspQueryInfoResult = promotionLspQueryInfoResult;
        }

		@Override
		public PromotionLspQueryInfoResult extractData() {
			return promotionLspQueryInfoResult;
		}

		@Override
		public boolean isSuccess() {
			return BooleanUtils.toBoolean(success);
		}
	}

    /**
     * @author dff
     * @date 2020-09-16
     */
    public static class PromotionLspQueryInfoResult {
        /**
         * 示例值:8390074
         * 描述:活动编码
         */
        private Long promotionInfoId;

        public Long getPromotionInfoId() {
            return promotionInfoId;
        }

        public void setPromotionInfoId(Long promotionInfoId) {
            this.promotionInfoId = promotionInfoId;
        }

        /**
         * 示例值:5
         * 描述:活动状态1：未确认5：已确认6：已取消9:已结束
         */
        private Integer promotionState;

        public Integer getPromotionState() {
            return promotionState;
        }

        public void setPromotionState(Integer promotionState) {
            this.promotionState = promotionState;
        }

        /**
         * 示例值:2017-11-1010:00:00
         * 描述:开始时间
         */
        private Date beginTime;

        public Date getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(Date beginTime) {
            this.beginTime = beginTime;
        }

        /**
         * 示例值:2017-11-1110:00:00
         * 描述:结束时间
         */
        private Date endTime;

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        /**
         * 示例值:
         * 描述:促销创建来源
         */
        private String source;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        /**
         * 示例值:
         * 描述:商品列表
         */
        private List<PromotionLspQuerySkuResult> skuResultList;

        public List<PromotionLspQuerySkuResult> getSkuResultList() {
            return skuResultList;
        }

        public void setSkuResultList(List<PromotionLspQuerySkuResult> skuResultList) {
            this.skuResultList = skuResultList;
        }
    }

	public static class PromotionLspQuerySkuResult {
		/**
		 * 示例值:2278456
		 * 描述:商品编码
		 */
		private Long skuId;

		public Long getSkuId() {
			return skuId;
		}

		public void setSkuId(Long skuId) {
			this.skuId = skuId;
		}

		/**
		 * 示例值:100089
		 * 描述:门店编码
		 */
		private Long stationNo;

		public Long getStationNo() {
			return stationNo;
		}

		public void setStationNo(Long stationNo) {
			this.stationNo = stationNo;
		}

		/**
		 * 示例值:1
		 * 描述:活动状态5：已生效6：已取消9:已结束
		 */
		private Long promotionState;

		public Long getPromotionState() {
			return promotionState;
		}

		public void setPromotionState(Long promotionState) {
			this.promotionState = promotionState;
		}

		/**
		 * 示例值:
		 * 描述:开始时间
		 */
		private Date beginTime;

		public Date getBeginTime() {
			return beginTime;
		}

		public void setBeginTime(Date beginTime) {
			this.beginTime = beginTime;
		}

		/**
		 * 示例值:
		 * 描述:结束时间
		 */
		private Date endTime;

		public Date getEndTime() {
			return endTime;
		}

		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}

		/**
		 * 示例值:0
		 * 描述:1：限制0：不限
		 */
		private Integer limitPin;

		public Integer getLimitPin() {
			return limitPin;
		}

		public void setLimitPin(Integer limitPin) {
			this.limitPin = limitPin;
		}

		/**
		 * 示例值:0
		 * 描述:1：限制0：不限
		 */
		private Integer limitDevice;

		public Integer getLimitDevice() {
			return limitDevice;
		}

		public void setLimitDevice(Integer limitDevice) {
			this.limitDevice = limitDevice;
		}

		/**
		 * 示例值:0
		 * 描述:1：按日限购0：活动期间限购
		 */
		private Integer limitDaily;

		public Integer getLimitDaily() {
			return limitDaily;
		}

		public void setLimitDaily(Integer limitDaily) {
			this.limitDaily = limitDaily;
		}

		/**
		 * 示例值:0
		 * 描述:平台承担比例
		 */
		private Integer platformRatio;

		public Integer getPlatformRatio() {
			return platformRatio;
		}

		public void setPlatformRatio(Integer platformRatio) {
			this.platformRatio = platformRatio;
		}

		/**
		 * 示例值:100
		 * 描述:商家承担比例
		 */
		private Integer storeRatio;

		public Integer getStoreRatio() {
			return storeRatio;
		}

		public void setStoreRatio(Integer storeRatio) {
			this.storeRatio = storeRatio;
		}

		/**
		 * 示例值:990
		 * 描述:促销价(单位:分)
		 */
		private Long promotionPrice;

		public Long getPromotionPrice() {
			return promotionPrice;
		}

		public void setPromotionPrice(Long promotionPrice) {
			this.promotionPrice = promotionPrice;
		}

		/**
		 * 示例值:商家版来源
		 * 描述:促销创建来源
		 */
		private String source;

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		/**
		 * 示例值:3
		 * 描述:促销类型3:直降4:秒杀5:单品买赠6:新人专享7:第二件N折
		 */
		private String promotionType;

		public String getPromotionType() {
			return promotionType;
		}

		public void setPromotionType(String promotionType) {
			this.promotionType = promotionType;
		}
	}

} 
