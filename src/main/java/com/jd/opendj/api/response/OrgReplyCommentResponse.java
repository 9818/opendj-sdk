package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class OrgReplyCommentResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.orgReplyCommentData = gson.fromJson(data, OrgReplyCommentData.class);
    }

    private OrgReplyCommentData orgReplyCommentData;

    public OrgReplyCommentData getOrgReplyCommentData() {
        return orgReplyCommentData;
    }

    public void setOrgReplyCommentData(OrgReplyCommentData orgReplyCommentData) {
        this.orgReplyCommentData = orgReplyCommentData;
    }

    public static class OrgReplyCommentData implements Data<Void> {
        /**
         * 示例值:200
         * 描述:响应编码:200正常，400系统错误，500参数错误，600:业务异常，700:请求时间异常
         */
        private Integer code;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        /**
         * 示例值:操作成功
         * 描述:错误消息
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:返回对象
         */
        private String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return code!=null && code==0;
        }
    }
} 
