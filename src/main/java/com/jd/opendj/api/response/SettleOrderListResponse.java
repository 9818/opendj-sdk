package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dff
 * @date 2020-05-11
 */ 

public class SettleOrderListResponse extends OpenDjResponse {

	private SettleOrderListContentData settleOrderListContentData;

	public SettleOrderListContentData getSettleOrderListContentData() {
		return settleOrderListContentData;
	}

	public void setSettleOrderListContentData(SettleOrderListContentData settleOrderListContentData) {
		this.settleOrderListContentData = settleOrderListContentData;
	}

	@Override
	public void toJavaObject() {
		Gson gson = new Gson();
		this.settleOrderListContentData = gson.fromJson(data, SettleOrderListContentData.class);
	}

	public static class SettleOrderListContentData implements Data<SettleOrderListContent> {
		/**
		 *示例值:200
		 *描述:状态码
		 */
		private Integer code;
		/**
		 *示例值:正常
		 *描述:描述信息
		 */
		private String msg;
		/**
		 *示例值:
		 *描述:返回结果
		 */
		private SettleOrderListContent content;

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public SettleOrderListContent getContent() {
			return content;
		}

		public void setContent(SettleOrderListContent content) {
			this.content = content;
		}

		@Override
		public SettleOrderListContent extractData() {
			return content;
		}

		@Override
		public boolean isSuccess() {
			return code!=null && code==200;
		}
	}




	/**
	 * @author dff
	 * @date 2020-05-11
	 */

	public static class SettleOrder {
		/**
		 *示例值:122111
		 *描述:结算单ID
		 */
		private Long settleOrderId;
		/**
		 *示例值:4321
		 *描述:商家id
		 */
		private Long orgCode;
		/**
		 *示例值:沃尔玛
		 *描述:商家名称
		 */
		private String orgName;
		/**
		 *示例值:12
		 *描述:门店id
		 */
		private Long stationId;
		/**
		 *示例值:门店名称
		 *描述:北京店
		 */
		private String stationName;
		/**
		 *示例值:201701
		 *描述:账期开始时间时间戳
		 */
		private Long accountTimeStart;
		/**
		 *示例值:201701
		 *描述:账期结束时间
		 */
		private Long accountTimeEnd;
		/**
		 *示例值:12
		 *描述:结算金额（单位：元）
		 */
		private BigDecimal settleMoney;
		/**
		 *示例值:12
		 *描述:结算单类型
		 */
		private String settleType;
		/**
		 *示例值:1
		 *描述:结算单类型代码，2017年7月22日以前的类型：1货款2市场费;以后的类型有：5006新货款，包含了货款和市场费。
		 */
		private Integer settleTypeCode;
		/**
		 *示例值:结算
		 *描述:结算状态
		 */
		private String settleStatus;
		/**
		 *示例值:结算
		 *描述:结算状态代码待打款(20002)、结算成功(20003)、结算失败(20004,20011)、驳回(20005)、打款失败(20009)、冻结中（20014）、部分结清（20015）
		 */
		private Integer settleStatusCode;
		/**
		 *示例值:
		 *描述:失败信息包含打款失败信息
		 */
		private String memo;
		/**
		 *示例值:201701
		 *描述:结算完成时间
		 */
		private Long finishTime;
		/**
		 *示例值:201701
		 *描述:最近更新时间
		 */
		private Long updateTime;
		/**
		 *示例值:结算
		 *描述:打款方式
		 */
		private String payMethod;
		/**
		 *示例值:1
		 *描述:打款方式代码(1支付宝2银行卡3京东钱包)
		 */
		private Integer payMethodCode;
		/**
		 *示例值:1231
		 *描述:收付款单号
		 */
		private String outFlowId;
		/**
		 *示例值:
		 *描述:账户信息
		 */
		private SettleOrderAccountContent accountContent;

		public Long getSettleOrderId() {
			return settleOrderId;
		}

		public void setSettleOrderId(Long settleOrderId) {
			this.settleOrderId = settleOrderId;
		}

		public Long getOrgCode() {
			return orgCode;
		}

		public void setOrgCode(Long orgCode) {
			this.orgCode = orgCode;
		}

		public String getOrgName() {
			return orgName;
		}

		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}

		public Long getStationId() {
			return stationId;
		}

		public void setStationId(Long stationId) {
			this.stationId = stationId;
		}

		public String getStationName() {
			return stationName;
		}

		public void setStationName(String stationName) {
			this.stationName = stationName;
		}

		public Long getAccountTimeStart() {
			return accountTimeStart;
		}

		public void setAccountTimeStart(Long accountTimeStart) {
			this.accountTimeStart = accountTimeStart;
		}

		public Long getAccountTimeEnd() {
			return accountTimeEnd;
		}

		public void setAccountTimeEnd(Long accountTimeEnd) {
			this.accountTimeEnd = accountTimeEnd;
		}

		public BigDecimal getSettleMoney() {
			return settleMoney;
		}

		public void setSettleMoney(BigDecimal settleMoney) {
			this.settleMoney = settleMoney;
		}

		public String getSettleType() {
			return settleType;
		}

		public void setSettleType(String settleType) {
			this.settleType = settleType;
		}

		public Integer getSettleTypeCode() {
			return settleTypeCode;
		}

		public void setSettleTypeCode(Integer settleTypeCode) {
			this.settleTypeCode = settleTypeCode;
		}

		public String getSettleStatus() {
			return settleStatus;
		}

		public void setSettleStatus(String settleStatus) {
			this.settleStatus = settleStatus;
		}

		public Integer getSettleStatusCode() {
			return settleStatusCode;
		}

		public void setSettleStatusCode(Integer settleStatusCode) {
			this.settleStatusCode = settleStatusCode;
		}

		public String getMemo() {
			return memo;
		}

		public void setMemo(String memo) {
			this.memo = memo;
		}

		public Long getFinishTime() {
			return finishTime;
		}

		public void setFinishTime(Long finishTime) {
			this.finishTime = finishTime;
		}

		public Long getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(Long updateTime) {
			this.updateTime = updateTime;
		}

		public String getPayMethod() {
			return payMethod;
		}

		public void setPayMethod(String payMethod) {
			this.payMethod = payMethod;
		}

		public Integer getPayMethodCode() {
			return payMethodCode;
		}

		public void setPayMethodCode(Integer payMethodCode) {
			this.payMethodCode = payMethodCode;
		}

		public String getOutFlowId() {
			return outFlowId;
		}

		public void setOutFlowId(String outFlowId) {
			this.outFlowId = outFlowId;
		}

		public SettleOrderAccountContent getAccountContent() {
			return accountContent;
		}

		public void setAccountContent(SettleOrderAccountContent accountContent) {
			this.accountContent = accountContent;
		}
	}


	/**
	 * @author dff
	 * @date 2020-05-11
	 */

	public static class SettleOrderAccountContent {
		/**
		 *示例值:北京
		 *描述:银行所在城市
		 */
		private String bankCity;
		/**
		 *示例值:北京
		 *描述:银行所在省份
		 */
		private String bankProvince;
		/**
		 *示例值:北京银行
		 *描述:支行名称
		 */
		private String bankSubBranch;
		/**
		 *示例值:
		 *描述:银行类型
		 */
		private String bankType;
		/**
		 *示例值:1111222
		 *描述:银行联行号
		 */
		private String bankAssociatedCode;
		/**
		 *示例值:P
		 *描述:账户类型P-对私C-对公
		 */
		private String bankAccountType;
		/**
		 *示例值:jd111
		 *描述:账号
		 */
		private String accountNo;
		/**
		 *示例值:skr
		 *描述:收款人
		 */
		private String payeeName;

		public String getBankCity() {
			return bankCity;
		}

		public void setBankCity(String bankCity) {
			this.bankCity = bankCity;
		}

		public String getBankProvince() {
			return bankProvince;
		}

		public void setBankProvince(String bankProvince) {
			this.bankProvince = bankProvince;
		}

		public String getBankSubBranch() {
			return bankSubBranch;
		}

		public void setBankSubBranch(String bankSubBranch) {
			this.bankSubBranch = bankSubBranch;
		}

		public String getBankType() {
			return bankType;
		}

		public void setBankType(String bankType) {
			this.bankType = bankType;
		}

		public String getBankAssociatedCode() {
			return bankAssociatedCode;
		}

		public void setBankAssociatedCode(String bankAssociatedCode) {
			this.bankAssociatedCode = bankAssociatedCode;
		}

		public String getBankAccountType() {
			return bankAccountType;
		}

		public void setBankAccountType(String bankAccountType) {
			this.bankAccountType = bankAccountType;
		}

		public String getAccountNo() {
			return accountNo;
		}

		public void setAccountNo(String accountNo) {
			this.accountNo = accountNo;
		}

		public String getPayeeName() {
			return payeeName;
		}

		public void setPayeeName(String payeeName) {
			this.payeeName = payeeName;
		}
	}

	/**
	 * @author dff
	 * @date 2020-05-11
	 */

	public static class SettleOrderListContent {
		/**
		 *示例值:合同已过期
		 *描述:合同状态
		 */
		private String contractStatus;
		/**
		 *示例值:
		 *描述:合计金额（单位：元）
		 */
		private BigDecimal summaryDueAmount;
		/**
		 *示例值:0
		 *描述:当前页应结金额（单位：元）
		 */
		private BigDecimal currentPageDueAmount;
		/**
		 *示例值:
		 *描述:返回结果
		 */
		private SettleOrderPage settleOrderList;

		public String getContractStatus() {
			return contractStatus;
		}

		public void setContractStatus(String contractStatus) {
			this.contractStatus = contractStatus;
		}

		public BigDecimal getSummaryDueAmount() {
			return summaryDueAmount;
		}

		public void setSummaryDueAmount(BigDecimal summaryDueAmount) {
			this.summaryDueAmount = summaryDueAmount;
		}

		public BigDecimal getCurrentPageDueAmount() {
			return currentPageDueAmount;
		}

		public void setCurrentPageDueAmount(BigDecimal currentPageDueAmount) {
			this.currentPageDueAmount = currentPageDueAmount;
		}

		public SettleOrderPage getSettleOrderList() {
			return settleOrderList;
		}

		public void setSettleOrderList(SettleOrderPage settleOrderList) {
			this.settleOrderList = settleOrderList;
		}
	}

	/**
	 * @author dff
	 * @date 2020-05-11
	 */

	public static class SettleOrderPage {
		/**
		 *示例值:1
		 *描述:第几页
		 */
		private Long pageNum;
		/**
		 *示例值:10
		 *描述:每页条数
		 */
		private Integer pageSize;
		/**
		 *示例值:10
		 *描述:总条数
		 */
		private Long total;
		/**
		 *示例值:1
		 *描述:开始行
		 */
		private Integer startRow;
		/**
		 *示例值:10
		 *描述:结束行
		 */
		private Integer endRow;
		/**
		 *示例值:10
		 *描述:总页数
		 */
		private Integer pages;
		/**
		 *示例值:
		 *描述:结算单list
		 */
		private List<SettleOrder> result;

		public Long getPageNum() {
			return pageNum;
		}

		public void setPageNum(Long pageNum) {
			this.pageNum = pageNum;
		}

		public Integer getPageSize() {
			return pageSize;
		}

		public void setPageSize(Integer pageSize) {
			this.pageSize = pageSize;
		}

		public Long getTotal() {
			return total;
		}

		public void setTotal(Long total) {
			this.total = total;
		}

		public Integer getStartRow() {
			return startRow;
		}

		public void setStartRow(Integer startRow) {
			this.startRow = startRow;
		}

		public Integer getEndRow() {
			return endRow;
		}

		public void setEndRow(Integer endRow) {
			this.endRow = endRow;
		}

		public Integer getPages() {
			return pages;
		}

		public void setPages(Integer pages) {
			this.pages = pages;
		}

		public List<SettleOrder> getResult() {
			return result;
		}

		public void setResult(List<SettleOrder> result) {
			this.result = result;
		}
	}

}
