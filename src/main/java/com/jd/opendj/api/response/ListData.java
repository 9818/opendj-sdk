package com.jd.opendj.api.response;

import java.util.List;

/**
 * <p>DataObject</p>
 * description
 *
 * @author 邓峰峰
 * @date 2019/12/13 16:51
 */
public interface ListData<T> {

    /**
     * 提取数据
     * @return 提取出来的业务数据
     */
   List<T> extractListData();

    /**
     * 是否请求成功
     * @return 是否成功
     */
   boolean isSuccess();

}
