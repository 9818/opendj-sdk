package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * <p>VoidOpenDjResponse</p>
 * description
 *
 * @author 邓峰峰
 * @date 9/2/2020 8:40 PM
 */
public class AcceptOperateResponse extends OpenDjResponse {

    private AcceptOperateData acceptOperateData;

    public AcceptOperateData getAcceptOperateData() {
        return acceptOperateData;
    }

    public void setAcceptOperateData(AcceptOperateData acceptOperateData) {
        this.acceptOperateData = acceptOperateData;
    }

    public static class AcceptOperateData implements Data<Void> {
        private String code;
        private String msg;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("无法提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
    @Override
    public void toJavaObject() {
        final Gson gson = new Gson();
        this.acceptOperateData = gson.fromJson(data, AcceptOperateData.class);
    }
}
