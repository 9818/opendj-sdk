package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class HandleReportResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.handleReportData = gson.fromJson(data, HandleReportData.class);
    }

    private HandleReportData handleReportData;

    public HandleReportData getHandleReportData() {
        return handleReportData;
    }

    public void setHandleReportData(HandleReportData handleReportData) {
        this.handleReportData = handleReportData;
    }

    public static class HandleReportData implements Data<Void> {
        /**
         * 示例值:0
         * 描述:1：参数错误，4001003：越权操作订单，0：成功，-1:失败，-3：系统错误
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:
         * 描述:msg
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public Void extractData() {
            throw new UnsupportedOperationException("不支持提取数据");
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }
} 
