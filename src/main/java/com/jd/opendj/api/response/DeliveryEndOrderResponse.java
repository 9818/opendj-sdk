package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class DeliveryEndOrderResponse extends OpenDjResponse {
    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.deliveryEndOrderData = gson.fromJson(data, DeliveryEndOrderData.class);
    }

    private DeliveryEndOrderData deliveryEndOrderData;

    public DeliveryEndOrderData getDeliveryEndOrderData() {
        return deliveryEndOrderData;
    }

    public void setDeliveryEndOrderData(DeliveryEndOrderData deliveryEndOrderData) {
        this.deliveryEndOrderData = deliveryEndOrderData;
    }

    public static class DeliveryEndOrderData implements Data<OrderMainDTO> {
        /**
         * 示例值:0
         * 描述:code，0：成功-1:失败1:参数错误4：%s不存在11：无此操作权限
         */
        private String code;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        /**
         * 示例值:成功
         * 描述:msg
         */
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        /**
         * 示例值:
         * 描述:result
         */
        private OrderMainDTO result;

        public OrderMainDTO getResult() {
            return result;
        }

        public void setResult(OrderMainDTO result) {
            this.result = result;
        }

        @Override
        public OrderMainDTO extractData() {
            return result;
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }
    }

    public static class OrderMainDTO {
        /**
         *订单号
         */
        private Long orderId;
        /**
         * 订单状态（33060:已妥投）
         */
        private Integer orderStatus;

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }
    }
} 
