package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.util.List;

/** 
 * @author dengfengfeng
 * @date 2020-01-07
 */ 

public class OrderCommentResponse extends OpenDjResponse {

	private OrderCommentData orderCommentData;

	public OrderCommentData getOrderCommentData() {
		return orderCommentData;
	}

	public void setOrderCommentData(OrderCommentData orderCommentData) {
		this.orderCommentData = orderCommentData;
	}

	@Override
	public void toJavaObject() {
		Gson gson = new Gson();
		this.orderCommentData = gson.fromJson(data, OrderCommentData.class);
	}

	public static class OrderCommentData {
		/**
		 *示例值:711288991000141
		 *描述:订单编号
		 */
		private Long orderId ;
		/**
		 *示例值:81370
		 *描述:商家编码
		 */
		private Long orgCode ;
		/**
		 *示例值:商家名称
		 *描述:商家名称
		 */
		private String orgName ;
		/**
		 *示例值:11649778
		 *描述:到家门店编号
		 */
		private Long storeId ;
		/**
		 *示例值:xxx-朝外店
		 *描述:到家门店名称
		 */
		private String storeName ;
		/**
		 *示例值:1572594152000
		 *描述:妥投时间
		 */
		private Long deliveryConfirmTime ;
		/**
		 *示例值:10000
		 *描述:订单类型（10000：超市类订单；13000：美食订单）
		 */
		private Integer orderType ;
		/**
		 *示例值:9966
		 *描述:配送方式（2938：商家自送，9966：达达专送,9999：到店自提，9998：线下订单）
		 */
		private String deliveryCarrierNo ;
		/**
		 *示例值:13321119693
		 *描述:配送员编号
		 */
		private String deliveryManNo ;
		/**
		 *示例值:5
		 *描述:配送评分
		 */
		private Integer score3 ;
		/**
		 *示例值:
		 *描述:配送评分标签内容
		 */
		private String[] tags ;
		/**
		 *示例值:
		 *描述:配送评分标签key
		 */
		private String[] tagsKey ;
		/**
		 *示例值:送货快
		 *描述:配送评分文字内容
		 */
		private String score3Content ;
		/**
		 *示例值:1
		 *描述:商品服务评分
		 */
		private Integer score4 ;
		/**
		 *示例值:非常好用
		 *描述:商品服务评分文字内容
		 */
		private Integer score4Content ;
		/**
		 *示例值:
		 *描述:商品服务评分标签key
		 */
		private String[] venderTageKey ;
		/**
		 *示例值:
		 *描述:商品服务评分标签文字
		 */
		private String[] venderTags ;
		/**
		 *示例值:1572594152000
		 *描述:修改后的配送时间（没有修改为null）
		 */
		private Long deliveryConfirmTime2 ;
		/**
		 *示例值:1572594152000
		 *描述:商家回复时间
		 */
		private Long orgCommentTime ;
		/**
		 *示例值:回复内容
		 *描述:商家回复内容
		 */
		private String orgCommentContent ;
		/**
		 *示例值:0
		 *描述:商家回复内容审核状态1:未审核，2：通过，3：不通过。
		 */
		private Integer orgCommentStatus ;
		/**
		 *示例值:1572594152000
		 *描述:用户修改后的妥投时间
		 */
		private Long deliveryDifTime2 ;
		/**
		 *示例值:4.1.0
		 *描述:APP使用的版本号
		 */
		private String appVersion ;
		/**
		 *示例值:1
		 *描述:是否更新：1未更新，2已更新
		 */
		private Integer isUpdate ;
		/**
		 *示例值:1572594152000
		 *描述:订单评价创建时间，评价修改后就是新的评价时间
		 */
		private Long createTime ;
		/**
		 *示例值:1572594152000
		 *描述:订单妥投时间
		 */
		private Long updateTime ;
		/**
		 *示例值:
		 *描述:商品评价信息集合
		 */
		private List<OrderProdCommentVo> orderProdCommentVoList ;


		public Long getOrderId() {
			return orderId;
		}

		public void setOrderId(Long orderId) {
			this.orderId = orderId;
		}

		public Long getOrgCode() {
			return orgCode;
		}

		public void setOrgCode(Long orgCode) {
			this.orgCode = orgCode;
		}

		public String getOrgName() {
			return orgName;
		}

		public void setOrgName(String orgName) {
			this.orgName = orgName;
		}

		public Long getStoreId() {
			return storeId;
		}

		public void setStoreId(Long storeId) {
			this.storeId = storeId;
		}

		public String getStoreName() {
			return storeName;
		}

		public void setStoreName(String storeName) {
			this.storeName = storeName;
		}

		public Long getDeliveryConfirmTime() {
			return deliveryConfirmTime;
		}

		public void setDeliveryConfirmTime(Long deliveryConfirmTime) {
			this.deliveryConfirmTime = deliveryConfirmTime;
		}

		public Integer getOrderType() {
			return orderType;
		}

		public void setOrderType(Integer orderType) {
			this.orderType = orderType;
		}

		public String getDeliveryCarrierNo() {
			return deliveryCarrierNo;
		}

		public void setDeliveryCarrierNo(String deliveryCarrierNo) {
			this.deliveryCarrierNo = deliveryCarrierNo;
		}

		public String getDeliveryManNo() {
			return deliveryManNo;
		}

		public void setDeliveryManNo(String deliveryManNo) {
			this.deliveryManNo = deliveryManNo;
		}

		public Integer getScore3() {
			return score3;
		}

		public void setScore3(Integer score3) {
			this.score3 = score3;
		}

		public String[] getTags() {
			return tags;
		}

		public void setTags(String[] tags) {
			this.tags = tags;
		}

		public String[] getTagsKey() {
			return tagsKey;
		}

		public void setTagsKey(String[] tagsKey) {
			this.tagsKey = tagsKey;
		}

		public String getScore3Content() {
			return score3Content;
		}

		public void setScore3Content(String score3Content) {
			this.score3Content = score3Content;
		}

		public Integer getScore4() {
			return score4;
		}

		public void setScore4(Integer score4) {
			this.score4 = score4;
		}

		public Integer getScore4Content() {
			return score4Content;
		}

		public void setScore4Content(Integer score4Content) {
			this.score4Content = score4Content;
		}

		public String[] getVenderTageKey() {
			return venderTageKey;
		}

		public void setVenderTageKey(String[] venderTageKey) {
			this.venderTageKey = venderTageKey;
		}

		public String[] getVenderTags() {
			return venderTags;
		}

		public void setVenderTags(String[] venderTags) {
			this.venderTags = venderTags;
		}

		public Long getDeliveryConfirmTime2() {
			return deliveryConfirmTime2;
		}

		public void setDeliveryConfirmTime2(Long deliveryConfirmTime2) {
			this.deliveryConfirmTime2 = deliveryConfirmTime2;
		}

		public Long getOrgCommentTime() {
			return orgCommentTime;
		}

		public void setOrgCommentTime(Long orgCommentTime) {
			this.orgCommentTime = orgCommentTime;
		}

		public String getOrgCommentContent() {
			return orgCommentContent;
		}

		public void setOrgCommentContent(String orgCommentContent) {
			this.orgCommentContent = orgCommentContent;
		}

		public Integer getOrgCommentStatus() {
			return orgCommentStatus;
		}

		public void setOrgCommentStatus(Integer orgCommentStatus) {
			this.orgCommentStatus = orgCommentStatus;
		}

		public Long getDeliveryDifTime2() {
			return deliveryDifTime2;
		}

		public void setDeliveryDifTime2(Long deliveryDifTime2) {
			this.deliveryDifTime2 = deliveryDifTime2;
		}

		public String getAppVersion() {
			return appVersion;
		}

		public void setAppVersion(String appVersion) {
			this.appVersion = appVersion;
		}

		public Integer getIsUpdate() {
			return isUpdate;
		}

		public void setIsUpdate(Integer isUpdate) {
			this.isUpdate = isUpdate;
		}

		public Long getCreateTime() {
			return createTime;
		}

		public void setCreateTime(Long createTime) {
			this.createTime = createTime;
		}

		public Long getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(Long updateTime) {
			this.updateTime = updateTime;
		}

		public List<OrderProdCommentVo> getOrderProdCommentVoList() {
			return orderProdCommentVoList;
		}

		public void setOrderProdCommentVoList(List<OrderProdCommentVo> orderProdCommentVoList) {
			this.orderProdCommentVoList = orderProdCommentVoList;
		}

	}

	/**
	 * @author dengfengfeng
	 * @date 2020-01-07
	 */

	public static class OrderProdCommentVo {
		/**
		 *示例值:112233
		 *描述:到家商品编码
		 */
		private Long skuId ;
		/**
		 *示例值:商品
		 *描述:商品名称
		 */
		private String skuName ;
		/**
		 *示例值:2
		 *描述:1赞，2踩
		 */
		private Integer isPraise ;

		public Long getSkuId() {
			return skuId;
		}

		public void setSkuId(Long skuId) {
			this.skuId = skuId;
		}

		public String getSkuName() {
			return skuName;
		}

		public void setSkuName(String skuName) {
			this.skuName = skuName;
		}

		public Integer getIsPraise() {
			return isPraise;
		}

		public void setIsPraise(Integer isPraise) {
			this.isPraise = isPraise;
		}
	}

}
