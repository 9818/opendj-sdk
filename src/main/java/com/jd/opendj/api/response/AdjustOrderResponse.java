package com.jd.opendj.api.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jd.opendj.api.OpenDjResponse;

import java.util.List;

/**
 * <p>OrderAdjustResponse</p>
 * description
 *
 * @author 邓峰峰
 * @date 9/8/2020 3:41 PM
 */
public class AdjustOrderResponse extends OpenDjResponse {

    private OrderAdjustData orderAdjustData;

    public OrderAdjustData getOrderAdjustData() {
        return orderAdjustData;
    }

    public void setOrderAdjustData(OrderAdjustData orderAdjustData) {
        this.orderAdjustData = orderAdjustData;
    }

    @Override
    public void toJavaObject() {
        final Gson gson = new GsonBuilder()
                .create();
        this.orderAdjustData = gson.fromJson(data, OrderAdjustData.class);
    }

    public static class OrderAdjustData implements Data<OrderAdjustData.OrderAdjustRespDTO>{
        private String code;
        private String msg;
        private String detail;
        private OrderAdjustRespDTO result;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public OrderAdjustRespDTO getResult() {
            return result;
        }

        public void setResult(OrderAdjustRespDTO result) {
            this.result = result;
        }

        @Override
        public OrderAdjustRespDTO extractData() {
            return result;
        }

        @Override
        public boolean isSuccess() {
            return "0".equals(code);
        }

        /**
         * @author dff
         * @date 2020-09-08
         */
        public static class OrderAdjustRespDTO {
            /**
             *示例值:100001005592670
             *描述:订单号
             */
            private Long orderId;
            /**
             *示例值:2
             *描述:缺货商品总数
             */
            private Integer adjustCount;
            /**
             *示例值:10024282
             *描述:门店编码
             */
            private String ProduceStationNo;
            /**
             *示例值:7330
             *描述:调整后的订单总金额
             */
            private Long orderTotalMoney;
            /**
             *示例值:0
             *描述:调整后的订单优惠总金额
             */
            private Long orderDiscountMoney;
            /**
             *示例值:0
             *描述:调整后的订单运费总金额
             */
            private Long orderFreightMoney;
            /**
             *示例值:0
             *描述:调整后用户应付金额
             */
            private Long orderBuyerPayableMoney;
            /**
             *示例值:
             *描述:调整后商品List集合
             */
            private List<Entity> oaList;
            /**
             *示例值:2000085701
             *描述:调整后的到家商品编号
             */
            private Long skuId;
            /**
             *示例值:5
             *描述:调整后的商品数量
             */
            private Integer skuCount;
            /**
             *示例值:伊利味可滋香蕉牛奶240ml
             *描述:调整后的商品名称
             */
            private String skuName;
            /**
             *示例值:550
             *描述:到家商品销售价格
             */
            private Long skuJdPrice;
            /**
             *示例值:1102506
             *描述:商家商品编码
             */
            private String outSkuId;
            /**
             *示例值:6907992512587
             *描述:商品upc码
             */
            private String upc;

            public Long getOrderId() {
                return orderId;
            }

            public void setOrderId(Long orderId) {
                this.orderId = orderId;
            }

            public Integer getAdjustCount() {
                return adjustCount;
            }

            public void setAdjustCount(Integer adjustCount) {
                this.adjustCount = adjustCount;
            }

            public String getProduceStationNo() {
                return ProduceStationNo;
            }

            public void setProduceStationNo(String produceStationNo) {
                ProduceStationNo = produceStationNo;
            }

            public Long getOrderTotalMoney() {
                return orderTotalMoney;
            }

            public void setOrderTotalMoney(Long orderTotalMoney) {
                this.orderTotalMoney = orderTotalMoney;
            }

            public Long getOrderDiscountMoney() {
                return orderDiscountMoney;
            }

            public void setOrderDiscountMoney(Long orderDiscountMoney) {
                this.orderDiscountMoney = orderDiscountMoney;
            }

            public Long getOrderFreightMoney() {
                return orderFreightMoney;
            }

            public void setOrderFreightMoney(Long orderFreightMoney) {
                this.orderFreightMoney = orderFreightMoney;
            }

            public Long getOrderBuyerPayableMoney() {
                return orderBuyerPayableMoney;
            }

            public void setOrderBuyerPayableMoney(Long orderBuyerPayableMoney) {
                this.orderBuyerPayableMoney = orderBuyerPayableMoney;
            }

            public List<Entity> getOaList() {
                return oaList;
            }

            public void setOaList(List<Entity> oaList) {
                this.oaList = oaList;
            }

            public Long getSkuId() {
                return skuId;
            }

            public void setSkuId(Long skuId) {
                this.skuId = skuId;
            }

            public Integer getSkuCount() {
                return skuCount;
            }

            public void setSkuCount(Integer skuCount) {
                this.skuCount = skuCount;
            }

            public String getSkuName() {
                return skuName;
            }

            public void setSkuName(String skuName) {
                this.skuName = skuName;
            }

            public Long getSkuJdPrice() {
                return skuJdPrice;
            }

            public void setSkuJdPrice(Long skuJdPrice) {
                this.skuJdPrice = skuJdPrice;
            }

            public String getOutSkuId() {
                return outSkuId;
            }

            public void setOutSkuId(String outSkuId) {
                this.outSkuId = outSkuId;
            }

            public String getUpc() {
                return upc;
            }

            public void setUpc(String upc) {
                this.upc = upc;
            }


            /**
             * @author dff
             * @date 2020-09-08
             */
            public static class Entity {
                /**
                 *示例值:2000085701
                 *描述:调整后的到家商品编号
                 */
                private Long skuId;
                /**
                 *示例值:5
                 *描述:调整后的商品数量
                 */
                private Integer skuCount;
                /**
                 *示例值:伊利味可滋香蕉牛奶240ml
                 *描述:调整后的商品名称
                 */
                private String skuName;
                /**
                 *示例值:550
                 *描述:到家商品销售价格
                 */
                private Long skuJdPrice;
                /**
                 *示例值:1102506
                 *描述:商家商品编码
                 */
                private String outSkuId;
                /**
                 *示例值:6907992512587
                 *描述:商品upc码
                 */
                private String upc;

                public Long getSkuId() {
                    return skuId;
                }

                public void setSkuId(Long skuId) {
                    this.skuId = skuId;
                }

                public Integer getSkuCount() {
                    return skuCount;
                }

                public void setSkuCount(Integer skuCount) {
                    this.skuCount = skuCount;
                }

                public String getSkuName() {
                    return skuName;
                }

                public void setSkuName(String skuName) {
                    this.skuName = skuName;
                }

                public Long getSkuJdPrice() {
                    return skuJdPrice;
                }

                public void setSkuJdPrice(Long skuJdPrice) {
                    this.skuJdPrice = skuJdPrice;
                }

                public String getOutSkuId() {
                    return outSkuId;
                }

                public void setOutSkuId(String outSkuId) {
                    this.outSkuId = outSkuId;
                }

                public String getUpc() {
                    return upc;
                }

                public void setUpc(String upc) {
                    this.upc = upc;
                }
            }
        }

    }

}
