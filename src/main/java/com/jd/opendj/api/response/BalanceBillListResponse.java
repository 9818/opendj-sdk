package com.jd.opendj.api.response;


import com.google.gson.Gson;
import com.jd.opendj.api.OpenDjResponse;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author dff
 * @date 2020-05-14
 */

public class BalanceBillListResponse extends OpenDjResponse {

    private BalanceBillListData balanceBillListData;

    public BalanceBillListData getBalanceBillListData() {
        return balanceBillListData;
    }

    public void setBalanceBillListData(BalanceBillListData balanceBillListData) {
        this.balanceBillListData = balanceBillListData;
    }

    @Override
    public void toJavaObject() {
        Gson gson = new Gson();
        this.balanceBillListData = gson.fromJson(data, BalanceBillListData.class);
    }

    public static class BalanceBillListData implements Data<BalanceBillListContent> {
        /**
         * 示例值:200
         * 描述:状态码，200为成功其他均为失败
         */
        private Integer code;
        /**
         * 示例值:正常
         * 描述:描述信息
         */
        private String msg;
        /**
         * 示例值:
         * 描述:返回结果
         */
        private BalanceBillListContent content;

        @Override
        public BalanceBillListContent extractData() {
            return content;
        }

        @Override
        public boolean isSuccess() {
            return code != null && 200 == code;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public BalanceBillListContent getContent() {
            return content;
        }

        public void setContent(BalanceBillListContent content) {
            this.content = content;
        }
    }


    /**
     * @author dff
     * @date 2020-05-14
     */

    public static class BalanceBillListContent {
        /**
         * 示例值:10
         * 描述:总应结（单位：元）
         */
        private BigDecimal summaryDueAmount;
        /**
         * 示例值:0
         * 描述:总货款（单位：元）
         */
        private BigDecimal summaryGoodsBill;
        /**
         * 示例值:10
         * 描述:当前页应结（单位：元）
         */
        private BigDecimal currentDueAmount;
        /**
         * 示例值:0
         * 描述:当前页货款（单位：元）
         */
        private BigDecimal currentGoodsBill;
        /**
         * 示例值:
         * 描述:返回结果
         */
        private SettleOrderPage billList;

        public BigDecimal getSummaryDueAmount() {
            return summaryDueAmount;
        }

        public void setSummaryDueAmount(BigDecimal summaryDueAmount) {
            this.summaryDueAmount = summaryDueAmount;
        }

        public BigDecimal getSummaryGoodsBill() {
            return summaryGoodsBill;
        }

        public void setSummaryGoodsBill(BigDecimal summaryGoodsBill) {
            this.summaryGoodsBill = summaryGoodsBill;
        }

        public BigDecimal getCurrentDueAmount() {
            return currentDueAmount;
        }

        public void setCurrentDueAmount(BigDecimal currentDueAmount) {
            this.currentDueAmount = currentDueAmount;
        }

        public BigDecimal getCurrentGoodsBill() {
            return currentGoodsBill;
        }

        public void setCurrentGoodsBill(BigDecimal currentGoodsBill) {
            this.currentGoodsBill = currentGoodsBill;
        }

		public SettleOrderPage getBillList() {
			return billList;
		}

		public void setBillList(SettleOrderPage billList) {
			this.billList = billList;
		}
	}

    public static class SettleOrderPage {
        /**
         * 示例值:1
         * 描述:第几页
         */
        private Long pageNum;

        public Long getPageNum() {
            return pageNum;
        }

        public void setPageNum(Long pageNum) {
            this.pageNum = pageNum;
        }

        /**
         * 示例值:10
         * 描述:每页条数
         */
        private Integer pageSize;

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        /**
         * 示例值:10
         * 描述:总条数
         */
        private Long total;

        public Long getTotal() {
            return total;
        }

        public void setTotal(Long total) {
            this.total = total;
        }

        /**
         * 示例值:1
         * 描述:开始行
         */
        private Integer startRow;

        public Integer getStartRow() {
            return startRow;
        }

        public void setStartRow(Integer startRow) {
            this.startRow = startRow;
        }

        /**
         * 示例值:10
         * 描述:结束行
         */
        private Integer endRow;

        public Integer getEndRow() {
            return endRow;
        }

        public void setEndRow(Integer endRow) {
            this.endRow = endRow;
        }

        /**
         * 示例值:10
         * 描述:总页数
         */
        private Integer pages;

        public Integer getPages() {
            return pages;
        }

        public void setPages(Integer pages) {
            this.pages = pages;
        }

        /**
         * 示例值:
         * 描述:对账单分页列表
         */
        private List<BalanceBillDTO> result;

        public List<BalanceBillDTO> getResult() {
            return result;
        }

        public void setResult(List<BalanceBillDTO> result) {
            this.result = result;
        }
    }

    /**
     * @author dff
     * @date 2020-05-14
     */

    public static class BalanceBillDTO {
        /**
         * 示例值:100001
         * 描述:对账单id
         */
        private Integer id;
        /**
         * 示例值:100001
         * 描述:商家编号
         */
        private Long orgCode;
        /**
         * 示例值:沃尔玛
         * 描述:商家名称
         */
        private String orgName;
        /**
         * 示例值:100001
         * 描述:门店ID
         */
        private Long stationId;
        /**
         * 示例值:北京店
         * 描述:门店名称
         */
        private String stationName;
        /**
         * 示例值:1512111825000
         * 描述:下单时间
         */
        private Long businessStartTime;
        /**
         * 示例值:1512111825000
         * 描述:完成时间
         */
        private Long businessFinishTime;
        /**
         * 示例值:1000010
         * 描述:订单号
         */
        private Long orderId;
        /**
         * 示例值:100001
         * 描述:暂时全部认定为已完成
         */
        private Integer orderStatus;
        /**
         * 示例值:100
         * 描述:应付货款（单位：元）
         */
        private BigDecimal goodsBill;
        /**
         * 示例值:100
         * 描述:应付金额（单位：元）
         */
        private BigDecimal dueAmount;
        /**
         * 示例值:结算成功
         * 描述:结算状态:待打款(20002)、结算成功(20003)、结算失败(20004,20011)、驳回(20005)、打款失败(20009)、冻结中(20014)
         */
        private String settleStatus;
        /**
         * 示例值:1
         * 描述:结算状态码
         */
        private Integer settleStatusCode;
        /**
         * 示例值:1000010
         * 描述:结算单号如果还未结算，则没有单号
         */
        private Long settleOrderId;
        /**
         * 示例值:
         * 描述:结算单号的字符形式
         */
        private String settleOrderIdStr;
        /**
         * 示例值:100
         * 描述:运费（单位：元）
         */
        private BigDecimal freightBill;
        /**
         * 示例值:100
         * 描述:餐盒费（单位：元）
         */
        private BigDecimal packageBill;
        /**
         * 示例值:100
         * 描述:总佣金（单位：元）
         */
        private BigDecimal commission;
        /**
         * 示例值:申诉单
         * 描述:订单类型异常调整/正向订单/售后退货/售后退款/售后退货申述/售后退款申述/售后换新申述/售后换新/售后直赔
         */
        private String orderType;
        /**
         * 示例值:1001
         * 描述:异常调整=1;正向订单=1001;售后退货=1002;售后退款=1003;售后退货申述=1004;售后退款申述=1005;售后换新申述=1006;售后换新=1016;售后直赔=1017;
         */
        private Integer orderTypeCode;
        /**
         * 示例值:-10
         * 描述:商家承担补贴（单位：元）
         */
        private BigDecimal storeSubsidy;
        /**
         * 示例值:100
         * 描述:总补贴（单位：元）
         */
        private BigDecimal subsidy;
        /**
         * 示例值:201701
         * 描述:创建时间
         */
        private String createTime;
        /**
         * 示例值:
         * 描述:京东承担补贴（单位：元）
         */
        private BigDecimal marketBill;
        /**
         * 示例值:
         * 描述:礼品卡费用
         */
        private Long giftcardBill;
        /**
         * 示例值:
         * 描述:营销服务费用
         */
        private Long marketingServiceFee;
        /**
         * 示例值:
         * 描述:营销服务费用状态
         */
        private Integer marketingServiceFeeStatus;
        /**
         * 示例值:
         * 描述:订单原价（单位：元）
         */
        private BigDecimal originalAmount;
        /**
         * 示例值:
         * 描述:商家承担货款补贴（单位：元）
         */
        private BigDecimal paymentSubsidies;
        /**
         * 示例值:
         * 描述:商家承担运费补贴（单位：元）
         */
        private BigDecimal storeFreightSubsidy;
        /**
         * 示例值:
         * 描述:商家自送配送费（单位：元）
         */
        private BigDecimal storeFreightAmount;
        /**
         * 示例值:
         * 描述:取件服务费(开票)(正向单展示远距离运费;售后单则展示达达售后运费)（单位：元）
         */
        private BigDecimal pickupAmount;
        /**
         * 示例值:
         * 描述:商家承担小费（单位：元）
         */
        private BigDecimal pickupServiceAmount;
        /**
         * 示例值:
         * 描述:账期时间（如2018100600:00~2018100700:00）
         */
        private String accountTime;
        /**
         * 示例值:
         * 描述:结算完成时间(时间戳单位为秒)
         */
        private Long settleFinishTime;
        /**
         * 示例值:
         * 描述:配送方式(商家自送,达达配送等)
         */
        private String deliveryTypeStr;
        /**
         * 示例值:
         * 描述:收付款单号
         */
        private String withdrawId;
        /**
         * 示例值:
         * 描述:订单渠道(京东到家,到家h5,微信小程序等)
         */
        private String orderSourceName;
        /**
         * 示例值:
         * 描述:用户支付方式(货到付款,ApplePay,支付宝支付等)
         */
        private String payChannelName;
        /**
         * 示例值:
         * 描述:线下收款应结（单位：元）
         */
        private BigDecimal offlineSettlementAmount;
        /**
         * 示例值:
         * 描述:对账单类型，5006货款对账单5008市场费对账单
         */
        private Integer billOrderType;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Long getOrgCode() {
            return orgCode;
        }

        public void setOrgCode(Long orgCode) {
            this.orgCode = orgCode;
        }

        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        public Long getStationId() {
            return stationId;
        }

        public void setStationId(Long stationId) {
            this.stationId = stationId;
        }

        public String getStationName() {
            return stationName;
        }

        public void setStationName(String stationName) {
            this.stationName = stationName;
        }

        public Long getBusinessStartTime() {
            return businessStartTime;
        }

        public void setBusinessStartTime(Long businessStartTime) {
            this.businessStartTime = businessStartTime;
        }

        public Long getBusinessFinishTime() {
            return businessFinishTime;
        }

        public void setBusinessFinishTime(Long businessFinishTime) {
            this.businessFinishTime = businessFinishTime;
        }

        public Long getOrderId() {
            return orderId;
        }

        public void setOrderId(Long orderId) {
            this.orderId = orderId;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public BigDecimal getGoodsBill() {
            return goodsBill;
        }

        public void setGoodsBill(BigDecimal goodsBill) {
            this.goodsBill = goodsBill;
        }

        public BigDecimal getDueAmount() {
            return dueAmount;
        }

        public void setDueAmount(BigDecimal dueAmount) {
            this.dueAmount = dueAmount;
        }

        public String getSettleStatus() {
            return settleStatus;
        }

        public void setSettleStatus(String settleStatus) {
            this.settleStatus = settleStatus;
        }

        public Integer getSettleStatusCode() {
            return settleStatusCode;
        }

        public void setSettleStatusCode(Integer settleStatusCode) {
            this.settleStatusCode = settleStatusCode;
        }

        public Long getSettleOrderId() {
            return settleOrderId;
        }

        public void setSettleOrderId(Long settleOrderId) {
            this.settleOrderId = settleOrderId;
        }

        public String getSettleOrderIdStr() {
            return settleOrderIdStr;
        }

        public void setSettleOrderIdStr(String settleOrderIdStr) {
            this.settleOrderIdStr = settleOrderIdStr;
        }

        public BigDecimal getFreightBill() {
            return freightBill;
        }

        public void setFreightBill(BigDecimal freightBill) {
            this.freightBill = freightBill;
        }

        public BigDecimal getPackageBill() {
            return packageBill;
        }

        public void setPackageBill(BigDecimal packageBill) {
            this.packageBill = packageBill;
        }

        public BigDecimal getCommission() {
            return commission;
        }

        public void setCommission(BigDecimal commission) {
            this.commission = commission;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public Integer getOrderTypeCode() {
            return orderTypeCode;
        }

        public void setOrderTypeCode(Integer orderTypeCode) {
            this.orderTypeCode = orderTypeCode;
        }

        public BigDecimal getStoreSubsidy() {
            return storeSubsidy;
        }

        public void setStoreSubsidy(BigDecimal storeSubsidy) {
            this.storeSubsidy = storeSubsidy;
        }

        public BigDecimal getSubsidy() {
            return subsidy;
        }

        public void setSubsidy(BigDecimal subsidy) {
            this.subsidy = subsidy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public BigDecimal getMarketBill() {
            return marketBill;
        }

        public void setMarketBill(BigDecimal marketBill) {
            this.marketBill = marketBill;
        }

        public Long getGiftcardBill() {
            return giftcardBill;
        }

        public void setGiftcardBill(Long giftcardBill) {
            this.giftcardBill = giftcardBill;
        }

        public Long getMarketingServiceFee() {
            return marketingServiceFee;
        }

        public void setMarketingServiceFee(Long marketingServiceFee) {
            this.marketingServiceFee = marketingServiceFee;
        }

        public Integer getMarketingServiceFeeStatus() {
            return marketingServiceFeeStatus;
        }

        public void setMarketingServiceFeeStatus(Integer marketingServiceFeeStatus) {
            this.marketingServiceFeeStatus = marketingServiceFeeStatus;
        }

        public BigDecimal getOriginalAmount() {
            return originalAmount;
        }

        public void setOriginalAmount(BigDecimal originalAmount) {
            this.originalAmount = originalAmount;
        }

        public BigDecimal getPaymentSubsidies() {
            return paymentSubsidies;
        }

        public void setPaymentSubsidies(BigDecimal paymentSubsidies) {
            this.paymentSubsidies = paymentSubsidies;
        }

        public BigDecimal getStoreFreightSubsidy() {
            return storeFreightSubsidy;
        }

        public void setStoreFreightSubsidy(BigDecimal storeFreightSubsidy) {
            this.storeFreightSubsidy = storeFreightSubsidy;
        }

        public BigDecimal getStoreFreightAmount() {
            return storeFreightAmount;
        }

        public void setStoreFreightAmount(BigDecimal storeFreightAmount) {
            this.storeFreightAmount = storeFreightAmount;
        }

        public BigDecimal getPickupAmount() {
            return pickupAmount;
        }

        public void setPickupAmount(BigDecimal pickupAmount) {
            this.pickupAmount = pickupAmount;
        }

        public BigDecimal getPickupServiceAmount() {
            return pickupServiceAmount;
        }

        public void setPickupServiceAmount(BigDecimal pickupServiceAmount) {
            this.pickupServiceAmount = pickupServiceAmount;
        }

        public String getAccountTime() {
            return accountTime;
        }

        public void setAccountTime(String accountTime) {
            this.accountTime = accountTime;
        }

        public Long getSettleFinishTime() {
            return settleFinishTime;
        }

        public void setSettleFinishTime(Long settleFinishTime) {
            this.settleFinishTime = settleFinishTime;
        }

        public String getDeliveryTypeStr() {
            return deliveryTypeStr;
        }

        public void setDeliveryTypeStr(String deliveryTypeStr) {
            this.deliveryTypeStr = deliveryTypeStr;
        }

        public String getWithdrawId() {
            return withdrawId;
        }

        public void setWithdrawId(String withdrawId) {
            this.withdrawId = withdrawId;
        }

        public String getOrderSourceName() {
            return orderSourceName;
        }

        public void setOrderSourceName(String orderSourceName) {
            this.orderSourceName = orderSourceName;
        }

        public String getPayChannelName() {
            return payChannelName;
        }

        public void setPayChannelName(String payChannelName) {
            this.payChannelName = payChannelName;
        }

        public BigDecimal getOfflineSettlementAmount() {
            return offlineSettlementAmount;
        }

        public void setOfflineSettlementAmount(BigDecimal offlineSettlementAmount) {
            this.offlineSettlementAmount = offlineSettlementAmount;
        }

        public Integer getBillOrderType() {
            return billOrderType;
        }

        public void setBillOrderType(Integer billOrderType) {
            this.billOrderType = billOrderType;
        }
    }

}
