package com.jd.opendj.api.response.adapter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeAdapter implements JsonDeserializer<LocalDateTime> {
    private final static DateTimeFormatter dtf = DateTimeFormatter .ofPattern("yyyy-MM-dd HH:mm:ss");
    public LocalDateTime deserialize(JsonElement arg0, Type arg1,
                            JsonDeserializationContext arg2) throws JsonParseException {
        return LocalDateTime.parse(arg0.getAsString(),dtf);
    }
}