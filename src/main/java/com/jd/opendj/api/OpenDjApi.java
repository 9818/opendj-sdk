package com.jd.opendj.api;



/**
 * <p>JddjOpenApi</p>
 * description
 *
 * @author 邓峰峰
 * @date 2020/1/19 17:37
 */


public enum OpenDjApi {

    /**
     * 查询售后单详情接口
     */
    getAfsService("https://openapi.jddj.com/djapi/afs/getAfsService"),
    /**
     * 申请售后单审核接口
     */
    afsOpenApprove("https://openapi.jddj.com/djapi/afs/afsOpenApprove"),


    /**
     * 新版订单金额拆分接口
     */
    queryOassBussMoney("https://openapi.jddj.com/djapi/oassBussService/queryOassBussMoney"),
    /**
     * 查询订单计费明细接口
     */
    checkBill("https://openapi.jddj.com/djapi/bill/checkBill"),
    /**
     * 售后单计费明细
     */
    checkAfsBill("https://openapi.jddj.com/djapi/bill/checkAfsBill"),


    /**
     * 订单列表查询接口
     */
    queryOrders("https://openapi.jddj.com/djapi/order/es/query"),
    /**
     * 商家确认接单接口
     */
    orderAcceptOperate("https://openapi.jddj.com/djapi/ocs/orderAcceptOperate"),
    /**
     * 拣货完成且众包配送接口
     */
    orderJDZBDelivery("https://openapi.jddj.com/djapi/bm/open/api/order/OrderJDZBDelivery"),
    /**
     * 	根据订单号查询订单跟踪接口
     */
    getByOrderNoForOaosNew("https://openapi.jddj.com/djapi/orderTrace/getByOrderNoForOaosNew"),
    /**
     * 	订单调整接口
     */
    adjustOrder("https://openapi.jddj.com/djapi/orderAdjust/adjustOrder"),
    /**
     * 根据到家活动ID查询订单级活动明细接口
     */
    queryActivityInfoById("https://openapi.jddj.com/djapi/orderdiscount/queryActivityInfoById"),
    /**
     * 根据到家活动ID查询单品级促销活动接口
     */
    queryPromotionInfo("https://openapi.jddj.com/djapi/singlePromote/queryPromotionInfo"),

    /**
     * 	根据订单号查询商家门店评价信息接口
     */
    getCommentByOrderId("https://openapi.jddj.com/djapi/commentOutApi/getCommentByOrderId"),
    /**
     * 	商家门店评价信息回复接口
     */
    orgReplyComment("https://openapi.jddj.com/djapi/commentOutApi/orgReplyComment"),

    /**
     *查询商家完结售后单列表接口
     */
    getFinalAfsServiceListByConditon("https://openapi.jddj.com/djapi/afs/getFinalAfsServiceListByConditon"),


    /**
     * 根据订单号查看配送员取货异常上报订单处理详情接口
     */
    getHandleReportRecord("https://openapi.jddj.com/djapi/order/getHandleReportRecord"),

    /**
     * 订单应结接口
     */
    orderShoudSettlementService("https://openapi.jddj.com/djapi/bill/orderShoudSettlementService"),

    /**
     * 分页查询结算单接口
     */
    getSettleOrderList("https://openapi.jddj.com/djapi/settle/getSettleOrderList"),
    /**
     * 查询结算单明细接口
     */
    getSettleOrderDetail("https://openapi.jddj.com/djapi/settle/getSettleOrderDetail"),


    /**
     * 分页查询对账单接口
     */
    getBalanceBillList("https://openapi.jddj.com/djapi/balance/getBalanceBillList"),

    /**
     * 订单已打印接口
     */
    printOrder("https://openapi.jddj.com/djapi/bm/open/api/order/printOrder"),

    /**
     * 商家审核用户取消申请接口
     * 应用场景:
     * 用户发起“订单取消申请”，商家需调此接口进行审核（同意或驳回）
     * 注：用户申请取消订单后，商家需在15分钟内审核，超时系统默认同意申请
     */
    orderCancelOperate("https://openapi.jddj.com/djapi/ocs/orderCancelOperate"),

    /**
     * 拣货完成且商家自送接口
     * 商家自送订单模式下，商家配送发货时调用此接口，将orderStatus（订单状态）由（32000:等待出库）变为（33040:配送中）。
     */
    orderSerllerDelivery("https://openapi.jddj.com/djapi/bm/open/api/order/OrderSerllerDelivery"),


    /**
     * 承运商方式为：（9999:到店自提），商家拣货完成时需调用此接口，变更订单状态。
     * 拣货完成且顾客自提接口
     */
    orderSelfMention("https://openapi.jddj.com/djapi/bm/open/api/order/OrderSelfMention"),


    /**
     * 订单达达配送转商家自送接口
     * 订单由“达达配送”转“商家自送”，运单状态为“待抢单”且超时10分钟，可以调用此接口转“商家自送”。
     *
     */
    modifySellerDelivery("https://openapi.jddj.com/djapi/order/modifySellerDelivery"),

    /**
     * 商家审核配送员取货失败接口
     */
    receiveFailedAudit("https://openapi.jddj.com/djapi/order/receiveFailedAudit"),

    /**
     * 商家确认收到拒收退回（或取消）的商品接口
     * ①拣货完成前，订单被风控；②达达配送的订单，顾客拒收；
     * 以上两种情况：订单状态均会变为锁定。商家需要调此接口确认已收到退回的商品，完成订单取消流程。
     * 注：24小时无响应，自动确认取消订单。
     */
    confirmReceiveGoods("https://openapi.jddj.com/djapi/order/confirmReceiveGoods"),

    /**
     * 取货失败后催配送员抢单接口
     * 商家审核达达取货失败同意后，在收到“订单运单状态消息”中字段：deliveryStatus（配送状态）为“25 取货失败”时，需要在“门店或商品”准备就绪后调用此接口。该订单会重新进入达达抢单池等待达达接单。
     */
    urgeDispatching("https://openapi.jddj.com/djapi/bm/urgeDispatching"),

    /**
     *订单自提码核验接口
     * 用户自提订单业务，在订单自提码核验完成后会自动调用开始配送，妥投相关接口，保持订单状态的完整性。
     */
    checkSelfPickCode("https://openapi.jddj.com/djapi/ocs/checkSelfPickCode"),

    /**
     * 订单妥投接口
     * 商家自送订单模式操作妥投，订单由商家自配送完成时，调用此接口，变更订单状态（33060:已妥投）
     * 注：在配送流程中，若用户拒收，需调用“订单取消且退款接口”商家可进行取消。
     */
    deliveryEndOrder("https://openapi.jddj.com/djapi/ocs/deliveryEndOrder"),

    /**
     * 商家自主发起售后接口
     * 适用于用户拿货去线下退款（业务类型为商超/美食/开放仓订单），由商家代客申请；用户与商家电话沟通，由商家发起售后。配送方式为达达配送时，商家只能发起商家原因的售后单，且只能发起退款类型的售后，并且商家发起的售后不需要审核，发起后直接自动审核通过，给用户退款；配送方式为商家自送时，商家可发起商家及物流原因的售后单
     */
    afsSubmit("https://openapi.jddj.com/djapi/afs/submit"),

    /**
     * 售后单确认收货接口
     * 售后退货的单子，配送或者商家把旧货取回后，商家操作确认收货，调用此接口，售后单状态变更为已收货，超过24小时系统自动确认收货。 达达转送的-退货中 101/退货中 111/货成功1111 才可以确认收货； 商家自送- 待退货110、送货成功101 可以确认收货
     */
    confirmReceipt("https://openapi.jddj.com/djapi/afs/confirmReceipt"),

    /**
     * 图片上传接口
     * 图片上传京东到家服务器
     * 此接口无需对接，可直接调用。仅支持：POST、入参传递使用（content-type:application/x-www-form-urlencoded），需设置http请求头（对参数进行urlEncode操作）
     * 图片上传大小限制2M，图片上传格式仅支持：png/jpg/jpeg
     */
    imageUpload("https://opentool.jddj.com/toolapi/imageUpload"),

    /**
     * 商家处理配送员取货异常上报接口
     * 接收“订单运单状态消息”deliveryStatus（运单配送状态）为“28骑手异常上报”时，需要调用此接口，上传订单小票、商品照片URL 举证；
     * 处理“28骑手异常上报”的取货异常情况，failType（失败类型）：20066包装不规范/雪糕类无保温袋、GTF_2商家未拣货、DTF_6缺件/少件），处理后deliveryStatus（运单配送状态）为：29异常上报已处理；
     * 如果商家10分钟内未处理则自动审核为骑手取货失败，deliveryStatus（运单配送状态）为：25 取货失败。”。
     */
    handleReport("https://openapi.jddj.com/djapi/order/handleReport"),

    ;

    private final String url;

    public String getUrl() {
        return url;
    }


    OpenDjApi(String url) {
        this.url = url;
    }


}
