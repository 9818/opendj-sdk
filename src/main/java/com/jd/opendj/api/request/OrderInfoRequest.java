package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrderInfoResponse;

import java.util.Date;
import java.util.Set;

/**
 * @author dengfengfeng
 * @date 2019-12-13
 */



public class OrderInfoRequest implements OpenDjRequest<OrderInfoResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.queryOrders;
	}

	@Override
	public Class<OrderInfoResponse> getResponseClass() {
		return OrderInfoResponse.class;
	}

	public OrderInfoRequest() {
	}

	public OrderInfoRequest(Long orderId) {
		this.orderId = orderId;
	}

	public OrderInfoRequest(String orderId) {
		this.orderId = Long.valueOf(orderId);
	}

	/**
	 *是否必须:否
	 *示例值:2
	 *描述:当前页数,默认：1
	 */
	private Long pageNo ;
	/**
	 *是否必须:否
	 *示例值:30
	 *描述:每页条数,默认：20，最大值100
	 */
	private Integer pageSize ;
	/**
	 *是否必须:否
	 *示例值:100001036354906
	 *描述:订单号
	 */
	private Long orderId ;
	/**
	 *是否必须:否
	 *示例值:张三
	 *描述:客户名
	 */
	private String buyerFullName ;
	/**
	 *是否必须:否
	 *示例值:张
	 *描述:客户名（模糊查询）
	 */
	private String buyerFullName_like ;
	/**
	 *是否必须:否
	 *示例值:15600000000
	 *描述:手机号
	 */
	private String buyerMobile ;
	/**
	 *是否必须:否
	 *示例值:4
	 *描述:订单支付类型（1：货到付款，4：在线支付）
	 */
	private Integer orderPayType ;
	/**
	 *是否必须:否
	 *示例值:JD_2aeh208df8789
	 *描述:买家账号
	 */
	private String buyerPin ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0500:00:00
	 *描述:订单开始时间(开始)
	 */
	private Date orderStartTime_begin ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0823:00:00
	 *描述:订单开始时间(结束)
	 */
	private Date orderStartTime_end ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0500:00:00
	 *描述:购买成交时间-支付(开始)
	 */
	private Date orderPurchaseTime_begin ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0823:00:00
	 *描述:购买成交时间-支付(结束)
	 */
	private Date orderPurchaseTime_end ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0800:00:00
	 *描述:妥投时间(开始)
	 */
	private Date deliveryConfirmTime_begin ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0823:00:00
	 *描述:妥投时间(结束)
	 */
	private Date deliveryConfirmTime_end ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0800:00:00
	 *描述:订单关闭时间(开始)
	 */
	private Date orderCloseTime_begin ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0823:00:00
	 *描述:订单关闭时间(结束)
	 */
	private Date orderCloseTime_end ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0800:00:00
	 *描述:订单取消时间(开始)
	 */
	private Date orderCancelTime_begin ;
	/**
	 *是否必须:否
	 *示例值:2016-05-0823:00:00
	 *描述:订单取消时间(结束)
	 */
	private Date orderCancelTime_end ;
	/**
	 *是否必须:否
	 *示例值:32000
	 *描述:订单状态（20010:锁定，20020:订单取消，20030:订单取消申请，20040:超时未支付系统取消，0050:暂停，31000:等待付款，31020:已付款，41000:待处理，32000:等待出库，33040:配送中，33060:已妥投，90000:订单完成）
	 */
	private Integer orderStatus ;
	/**
	 *是否必须:否
	 *示例值:
	 *描述:订单状态复选条件
	 */
	private Set<Integer> orderStatus_list ;
	/**
	 *是否必须:否
	 *示例值:
	 *描述:城市复选条件
	 */
	private Set<String> buyerCity_list ;
	/**
	 *是否必须:否
	 *示例值:1000001
	 *描述:承运单号，通常情况下和订单号一致
	 */
	private String deliveryBillNo ;
	/**
	 *是否必须:否
	 *示例值:1,2
	 *描述:业务类型（1:京东到家商超,2:京东到家美食,3:京东到家精品有约,4:京东到家开放仓,5:哥伦布店内订单,6:货柜项目订单,7:智能货柜项目订单,8:轻松购订单,9:自助收银订单,10:超级会员码），当多个业务类型时，是以逗号分隔的数值串。
	 */
	private Integer[] businessType_list ;
	/**
	 *是否必须:否
	 *示例值:10000
	 *描述:订单类型10000:从门店出的订单
	 */
	private Integer orderType ;
	/**
	 *是否必须:否
	 *示例值:eRowg
	 *描述:订单自提码，当该字段有值时，要求到家配送门店编码必填。
	 */
	private String orderTakeSelfCode ;
	/**
	 *是否必须:否
	 *示例值:1000001
	 *描述:到家门店编码
	 */
	private String deliveryStationNo ;
	/**
	 *是否必须:否
	 *示例值:2000001
	 *描述:商家门店编码
	 */
	private String deliveryStationNoIsv ;

	/**
	 *是否必须:否
	 *示例值:12356856884
	 *描述:订单来源系统(比如京东订单号)
	 */
	private String srcOrderId;

	/**
	 *是否必须:否
	 *示例值:字段在下面返回结果(data.result.resultList中)查看。可以按标准数组方式传值，也可以按字符串传值。例如：
	 * "orderId,discount,product"
	 * 或者
	 * ["orderId","discount","product"]
	 *描述:设置返回结果(data.result.resultList中)的字段，字段间用英文逗号隔开，不传返回全部字段。
	 * 强烈建议升级为可定制化查询模式，提升接口整体查询性能（减少无用字段的消耗）
	 */
	private String[] returnedFields;

	public Long getPageNo() {
		return pageNo;
	}

	public void setPageNo(Long pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getBuyerFullName() {
		return buyerFullName;
	}

	public void setBuyerFullName(String buyerFullName) {
		this.buyerFullName = buyerFullName;
	}

	public String getBuyerFullName_like() {
		return buyerFullName_like;
	}

	public void setBuyerFullName_like(String buyerFullName_like) {
		this.buyerFullName_like = buyerFullName_like;
	}

	public String getBuyerMobile() {
		return buyerMobile;
	}

	public void setBuyerMobile(String buyerMobile) {
		this.buyerMobile = buyerMobile;
	}

	public Integer getOrderPayType() {
		return orderPayType;
	}

	public void setOrderPayType(Integer orderPayType) {
		this.orderPayType = orderPayType;
	}

	public String getBuyerPin() {
		return buyerPin;
	}

	public void setBuyerPin(String buyerPin) {
		this.buyerPin = buyerPin;
	}

	public Date getOrderStartTime_begin() {
		return orderStartTime_begin;
	}

	public void setOrderStartTime_begin(Date orderStartTime_begin) {
		this.orderStartTime_begin = orderStartTime_begin;
	}

	public Date getOrderStartTime_end() {
		return orderStartTime_end;
	}

	public void setOrderStartTime_end(Date orderStartTime_end) {
		this.orderStartTime_end = orderStartTime_end;
	}

	public Date getOrderPurchaseTime_begin() {
		return orderPurchaseTime_begin;
	}

	public void setOrderPurchaseTime_begin(Date orderPurchaseTime_begin) {
		this.orderPurchaseTime_begin = orderPurchaseTime_begin;
	}

	public Date getOrderPurchaseTime_end() {
		return orderPurchaseTime_end;
	}

	public void setOrderPurchaseTime_end(Date orderPurchaseTime_end) {
		this.orderPurchaseTime_end = orderPurchaseTime_end;
	}

	public Date getDeliveryConfirmTime_begin() {
		return deliveryConfirmTime_begin;
	}

	public void setDeliveryConfirmTime_begin(Date deliveryConfirmTime_begin) {
		this.deliveryConfirmTime_begin = deliveryConfirmTime_begin;
	}

	public Date getDeliveryConfirmTime_end() {
		return deliveryConfirmTime_end;
	}

	public void setDeliveryConfirmTime_end(Date deliveryConfirmTime_end) {
		this.deliveryConfirmTime_end = deliveryConfirmTime_end;
	}

	public Date getOrderCloseTime_begin() {
		return orderCloseTime_begin;
	}

	public void setOrderCloseTime_begin(Date orderCloseTime_begin) {
		this.orderCloseTime_begin = orderCloseTime_begin;
	}

	public Date getOrderCloseTime_end() {
		return orderCloseTime_end;
	}

	public void setOrderCloseTime_end(Date orderCloseTime_end) {
		this.orderCloseTime_end = orderCloseTime_end;
	}

	public Date getOrderCancelTime_begin() {
		return orderCancelTime_begin;
	}

	public void setOrderCancelTime_begin(Date orderCancelTime_begin) {
		this.orderCancelTime_begin = orderCancelTime_begin;
	}

	public Date getOrderCancelTime_end() {
		return orderCancelTime_end;
	}

	public void setOrderCancelTime_end(Date orderCancelTime_end) {
		this.orderCancelTime_end = orderCancelTime_end;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Set<Integer> getOrderStatus_list() {
		return orderStatus_list;
	}

	public void setOrderStatus_list(Set<Integer> orderStatus_list) {
		this.orderStatus_list = orderStatus_list;
	}

	public Set<String> getBuyerCity_list() {
		return buyerCity_list;
	}

	public void setBuyerCity_list(Set<String> buyerCity_list) {
		this.buyerCity_list = buyerCity_list;
	}

	public String getDeliveryBillNo() {
		return deliveryBillNo;
	}

	public void setDeliveryBillNo(String deliveryBillNo) {
		this.deliveryBillNo = deliveryBillNo;
	}

	public Integer[] getBusinessType_list() {
		return businessType_list;
	}

	public void setBusinessType_list(Integer[] businessType_list) {
		this.businessType_list = businessType_list;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public String getOrderTakeSelfCode() {
		return orderTakeSelfCode;
	}

	public void setOrderTakeSelfCode(String orderTakeSelfCode) {
		this.orderTakeSelfCode = orderTakeSelfCode;
	}

	public String getDeliveryStationNo() {
		return deliveryStationNo;
	}

	public void setDeliveryStationNo(String deliveryStationNo) {
		this.deliveryStationNo = deliveryStationNo;
	}

	public String getDeliveryStationNoIsv() {
		return deliveryStationNoIsv;
	}

	public void setDeliveryStationNoIsv(String deliveryStationNoIsv) {
		this.deliveryStationNoIsv = deliveryStationNoIsv;
	}

	public String getSrcOrderId() {
		return srcOrderId;
	}

	public void setSrcOrderId(String srcOrderId) {
		this.srcOrderId = srcOrderId;
	}

	public String[] getReturnedFields() {
		return returnedFields;
	}

	public void setReturnedFields(String[] returnedFields) {
		this.returnedFields = returnedFields;
	}

}
