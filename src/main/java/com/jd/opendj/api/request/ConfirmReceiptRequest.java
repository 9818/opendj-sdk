package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.ConfirmReceiptResponse;

/** 
 * @author dff
 * @date 2020-09-17
 */ 
public class ConfirmReceiptRequest  implements OpenDjRequest<ConfirmReceiptResponse> {
	/** 
	*是否必须:是 
	*示例值:20043683 
	*描述:服务单号 
	*/ 
	private String afsServiceOrder;
	public String getAfsServiceOrder() { 
		return afsServiceOrder;
	}
	public void setAfsServiceOrder(String afsServiceOrder) {
		this.afsServiceOrder=afsServiceOrder;
	}
	/** 
	*是否必须:是 
	*示例值:Bjzhouxx 
	*描述:操作人 
	*/ 
	private String pin;
	public String getPin() { 
		return pin;
	}
	public void setPin(String pin) {
		this.pin=pin;
	}

	@Override
	public OpenDjApi api() {
		return OpenDjApi.confirmReceipt;
	}

	@Override
	 public Class<ConfirmReceiptResponse> getResponseClass() {
		return ConfirmReceiptResponse.class;
	}
} 
