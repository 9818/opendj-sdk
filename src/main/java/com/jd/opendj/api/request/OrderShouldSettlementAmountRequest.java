package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrderShouldSettlementAmountResponse;

/**
 * @author dff
 * @date 2020-04-15
 */ 



public class OrderShouldSettlementAmountRequest implements OpenDjRequest<OrderShouldSettlementAmountResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.orderShoudSettlementService;
	}

	@Override
	public Class<OrderShouldSettlementAmountResponse> getResponseClass() {
		return OrderShouldSettlementAmountResponse.class;
	}

	/**
	*示例值:800000001 
	*描述:计费成功的订单编号 
	*/ 
	private Long orderId;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
}
