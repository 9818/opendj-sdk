package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.FinalAfsServiceListResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class FinalAfsServiceListRequest implements OpenDjRequest<FinalAfsServiceListResponse> {
    /**
     * 是否必须:否
     * 示例值:
     * 描述:门店编号
     */
    private String stationId;

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    /**
     * 是否必须:是
     * 示例值:2016-05-0500:00:00
     * 描述:完结开始时间
     */
    private String finishStartTime;

    public String getFinishStartTime() {
        return finishStartTime;
    }

    public void setFinishStartTime(String finishStartTime) {
        this.finishStartTime = finishStartTime;
    }

    /**
     * 是否必须:是
     * 示例值:2016-05-0500:00:00
     * 描述:完结结束时间
     */
    private String finishEndTime;

    public String getFinishEndTime() {
        return finishEndTime;
    }

    public void setFinishEndTime(String finishEndTime) {
        this.finishEndTime = finishEndTime;
    }

    /**
     * 是否必须:否
     * 示例值:3
     * 描述:售后单类型（1：线上订单、2：线下订单、3：全部）
     */
    private Integer afsType;

    public Integer getAfsType() {
        return afsType;
    }

    public void setAfsType(Integer afsType) {
        this.afsType = afsType;
    }

    /**
     * 是否必须:是
     * 示例值:50
     * 描述:每页大小（分页size）（从1-200间选值）
     */
    private Integer pageSize;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 是否必须:是
     * 示例值:1
     * 描述:当前页（第几页）
     */
    private Integer currentPage;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.getFinalAfsServiceListByConditon;
    }

    @Override
    public Class<FinalAfsServiceListResponse> getResponseClass() {
        return FinalAfsServiceListResponse.class;
    }
} 
