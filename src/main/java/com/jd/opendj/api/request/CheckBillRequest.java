package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.CheckBillResponse;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/** 
 * @author dengfengfeng
 * @date 2019-12-13
 */ 



public class CheckBillRequest implements OpenDjRequest<CheckBillResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.checkBill;
	}

	@Override
	public Class<CheckBillResponse> getResponseClass() {
		return CheckBillResponse.class;
	}

	public CheckBillRequest() {
	}

	public CheckBillRequest(String orderId) {
		this.orderIds = Collections.singletonList(Long.valueOf(orderId));
	}

	public CheckBillRequest(Long orderId) {
		this.orderIds = Collections.singletonList(orderId);
	}

	public CheckBillRequest(List<String> orderIds){
		this.orderIds = orderIds.stream().map(Long::valueOf).collect(Collectors.toList());
	}


	/** 
	*sample value:否 
	* 
	*/ 
	private List<Long> orderIds ;
	/** 
	*sample value:否 
	*1234567890 
	*/ 
	private Long startTime ; 
	/** 
	*sample value:否 
	*9876543210 
	*/ 
	private Long endTime ; 
	/** 
	*sample value:否 
	*1 
	*/ 
	private Integer pageNo ; 
	/** 
	*sample value:否 
	*20 
	*/ 
	private Integer pageSize ;

	public List<Long> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(List<Long> orderIds) {
		this.orderIds = orderIds;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
