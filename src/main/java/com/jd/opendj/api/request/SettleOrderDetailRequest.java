package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.SettleOrderDetailResponse;

/**
 * @author dff
 * @date 2020-05-11
 */ 


public class SettleOrderDetailRequest implements OpenDjRequest<SettleOrderDetailResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.getSettleOrderDetail;
	}

	@Override
	public Class<SettleOrderDetailResponse> getResponseClass() {
		return SettleOrderDetailResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:1000000001 
	*描述:结算单编码1000000001 
	*/ 
	private Long settleOrderId;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:页码不传默认第一页（针对于settleDetails信息的分页） 
	*/ 
	private Integer pageNum;
	/** 
	*是否必须:否 
	*示例值:10 
	*描述:每页条数，默认10（针对于settleDetails信息的分页，推荐设置50，最大设置100） 
	*/ 
	private Integer pageSize;

	public Long getSettleOrderId() {
		return settleOrderId;
	}

	public void setSettleOrderId(Long settleOrderId) {
		this.settleOrderId = settleOrderId;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
