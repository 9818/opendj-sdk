package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.ImageUploadResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class ImageUploadRequest implements OpenDjRequest<ImageUploadResponse> {
    /**
     * 是否必须:是
     * 示例值:
     * 描述:上传的图片base64编码字符串（要求包含前缀“data:image/**;base64,”）
     */
    private String imageBase64;

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.imageUpload;
    }

    @Override
    public Class<ImageUploadResponse> getResponseClass() {
        return ImageUploadResponse.class;
    }
} 
