package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OassBussMoneyResponse;

/**
 * @author dengfengfeng
 *
 * 场景描述：查询订单优惠明细、分摊比例等信息。此接口返回值只包含商品优惠，虚拟金额拆分信息，不包含运费以及运费优惠信息，不包含订单明细。
 * 计算全部的平台承担金额：[订单级别促销优惠的平台承担金额(data.discountlist.costMoney,集合遍历累加) + 平台积分（鲜豆）金额(data.platformIntegralDeductMoney) + 单品级优惠的平台承担(data.costMoney*skuCount)] (data集合遍历累加)，商品不同价格的问题,(比如A单品第一件优惠，顾客购买超过1件以外的商品就会用非优惠价格购买，比如用户买了5件，此时data中会出现两个同样skuId的商品，但是数量不同分别是1和4，承担信息也不相同，仍然使用上面公式即可)
 * 注:用户取消或撤销的订单不能获取金额拆分明细；订单金额拆分信息有效期3个月，过期后归档！
 * @date 2019-12-13
 */



public class OassBussMoneyRequest implements OpenDjRequest<OassBussMoneyResponse> {

    @Override
    public OpenDjApi api() {
        return OpenDjApi.queryOassBussMoney;
    }

    @Override
    public Class<OassBussMoneyResponse> getResponseClass() {
        return OassBussMoneyResponse.class;
    }

    public OassBussMoneyRequest() {
    }

    public OassBussMoneyRequest(Long orderId) {
        this.orderId = orderId;
    }

    public OassBussMoneyRequest(String orderId) {
        this.orderId = Long.valueOf(orderId);
    }

    /**
     * 订单号
     */
    private Long orderId;
    /**
     * 用户pin
     */
    private String userPin;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getUserPin() {
        return userPin;
    }

    public void setUserPin(String userPin) {
        this.userPin = userPin;
    }
}
