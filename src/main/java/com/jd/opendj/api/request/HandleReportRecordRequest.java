package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.HandleReportRecordResponse;

/**
 * @author dff
 * @date 2020-09-17
 */
public class HandleReportRecordRequest implements OpenDjRequest<HandleReportRecordResponse> {
    /**
     * 是否必须:是
     * 示例值:2001326756
     * 描述:订单号
     */
    private Long orderId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:否
     * 示例值:
     * 描述:操作人
     */
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.getHandleReportRecord;
    }

    @Override
    public Class<HandleReportRecordResponse> getResponseClass() {
        return HandleReportRecordResponse.class;
    }
} 
