package com.jd.opendj.api.request;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.AcceptOperateResponse;

/** 
 * @author dengfengfeng
 * @date 2020-01-07
 */ 
public class AcceptOperateRequest implements OpenDjRequest<AcceptOperateResponse> {
	/** 
	*是否必须:是 
	*示例值:100001016163464 
	*描述:订单号 
	*/ 
	private String orderId;
	/** 
	*是否必须:是 
	*示例值:true 
	*描述:操作类型true接单false不接单 
	*/ 
	private Boolean isAgreed;
	/** 
	*是否必须:是 
	*示例值:test 
	*描述:操作人 
	*/ 
	private String operator;

	/**
	 * 解决 isAgreed转json会变成 agreed的问题
	 * @return
	 */
	@Override
	public String toJsonString() {
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("orderId",orderId);
		jsonObject.addProperty("isAgreed",isAgreed);
		jsonObject.addProperty("operator",operator);
		return new Gson().toJson(jsonObject);
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Boolean getAgreed() {
		return isAgreed;
	}

	public void setAgreed(Boolean agreed) {
		isAgreed = agreed;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	@Override
	public OpenDjApi api() {
		return OpenDjApi.orderAcceptOperate;
	}

	@Override
	public Class<AcceptOperateResponse> getResponseClass() {
		return AcceptOperateResponse.class;
	}
}
