package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.CheckAfsBillResponse;

import java.util.List;

/** 
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class CheckAfsBillRequest implements OpenDjRequest<CheckAfsBillResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.checkAfsBill;
	}

	@Override
	public Class<CheckAfsBillResponse> getResponseClass() {
		return CheckAfsBillResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:9876543210 
	*描述:订单号（如果下面的参数售后来源和售后单号不传值，则返回该订单所有售后计费） 
	*/ 
	private Long orderId;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:售后来源1售后2售后申诉 
	*/ 
	private Integer afsSource;
	/** 
	*是否必须:否 
	*示例值: 
	*描述:售后单号（和售后来源必须一起传值） 
	*/ 
	private Long afsId;

	/**
	 *sample value:否
	 *
	 */
	private List<Long> orderIds ;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Integer getAfsSource() {
		return afsSource;
	}

	public void setAfsSource(Integer afsSource) {
		this.afsSource = afsSource;
	}

	public Long getAfsId() {
		return afsId;
	}

	public void setAfsId(Long afsId) {
		this.afsId = afsId;
	}

	public List<Long> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(List<Long> orderIds) {
		this.orderIds = orderIds;
	}
}
