package com.jd.opendj.api.request;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.ReceiveFailedAuditResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class ReceiveFailedAuditRequest implements OpenDjRequest<ReceiveFailedAuditResponse> {
    /**
     * 是否必须:是
     * 示例值:766434523452
     * 描述:订单编码
     */
    private long orderId;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:true
     * 描述:是否同意
     */
    private boolean isAgreed;

    public boolean isAgreed() {
        return isAgreed;
    }

    public void setAgreed(boolean agreed) {
        isAgreed = agreed;
    }

    /**
     * 是否必须:是
     * 示例值:张三
     * 描述:操作人
     */
    private String operator;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * 是否必须:否
     * 示例值:同意取消
     * 描述:备注
     */
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.receiveFailedAudit;
    }

    @Override
    public Class<ReceiveFailedAuditResponse> getResponseClass() {
        return ReceiveFailedAuditResponse.class;
    }

    @Override
    public String toJsonString() {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("orderId",orderId);
        jsonObject.addProperty("isAgreed",isAgreed);
        jsonObject.addProperty("operator",operator);
        jsonObject.addProperty("remark",remark);
        return new Gson().toJson(jsonObject);
    }
} 
