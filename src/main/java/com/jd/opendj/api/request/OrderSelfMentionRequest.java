package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrderSelfMentionResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class OrderSelfMentionRequest implements OpenDjRequest<OrderSelfMentionResponse> {
    /**
     * 是否必须:是
     * 示例值:100001020895180
     * 描述:订单编码
     */
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:张三
     * 描述:操作人
     */
    private String operator;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.orderSelfMention;
    }

    @Override
    public Class<OrderSelfMentionResponse> getResponseClass() {
        return OrderSelfMentionResponse.class;
    }
} 
