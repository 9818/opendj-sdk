package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrderNoForOaosNewResponse;

/**
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class OrderNoForOaosNewRequest implements OpenDjRequest<OrderNoForOaosNewResponse> {
	@Override
	public OpenDjApi api() {
		return OpenDjApi.getByOrderNoForOaosNew;
	}

	@Override
	public Class<OrderNoForOaosNewResponse> getResponseClass() {
		return OrderNoForOaosNewResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:100001020895180 
	*描述:订单Id 
	*/ 
	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
