package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.AdjustOrderResponse;

import java.util.List;

/** 
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class AdjustOrderRequest implements OpenDjRequest<AdjustOrderResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.adjustOrder;
	}

	@Override
	public Class<AdjustOrderResponse> getResponseClass() {
		return AdjustOrderResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:100001005592670 
	*描述:订单号 
	*/ 
	private Long orderId;
	/** 
	*是否必须:是 
	*示例值:张三 
	*描述:操作人 
	*/ 
	private String operPin;
	/** 
	*是否必须:是 
	*示例值:客户要求更换商品 
	*描述:操作备注 
	*/ 
	private String remark;
	/** 
	*是否必须:是 
	*示例值: 
	*描述:订单调整后的所有商品明细（含未调整的商品），如果某商品数量调整为0时，商品明细中不能包含该商品；如果全部商品数量调整为0时，需联系顾客沟通确认后，由顾客取消订单。 
	*/ 
	private List<OAOSAdjustDTO> oaosAdjustDTOList;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOperPin() {
		return operPin;
	}

	public void setOperPin(String operPin) {
		this.operPin = operPin;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<OAOSAdjustDTO> getOaosAdjustDTOList() {
		return oaosAdjustDTOList;
	}

	/**
	 * @author dengfengfeng
	 * @date 2020-01-07
	 */

	public static class OAOSAdjustDTO {
		/**
		 *是否必须:否
		 *示例值:
		 *描述:到家商品编码(skuId与outSkuId至少填一个)
		 */
		private Long skuId;
		/**
		 *是否必须:否
		 *示例值:
		 *描述:商家商品编码(skuId与outSkuId至少填一个)
		 */
		private String outSkuId;
		/**
		 *是否必须:是
		 *示例值:
		 *描述:商品数量
		 */
		private Integer skuCount;

		public Long getSkuId() {
			return skuId;
		}

		public void setSkuId(Long skuId) {
			this.skuId = skuId;
		}

		public String getOutSkuId() {
			return outSkuId;
		}

		public void setOutSkuId(String outSkuId) {
			this.outSkuId = outSkuId;
		}

		public Integer getSkuCount() {
			return skuCount;
		}

		public void setSkuCount(Integer skuCount) {
			this.skuCount = skuCount;
		}
	}


	public void setOaosAdjustDTOList(List<OAOSAdjustDTO> oaosAdjustDTOList) {
		this.oaosAdjustDTOList = oaosAdjustDTOList;
	}
}
