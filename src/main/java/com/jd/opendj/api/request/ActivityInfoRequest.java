package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.ActivityInfoResponse;

/**
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class ActivityInfoRequest implements OpenDjRequest<ActivityInfoResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.queryActivityInfoById;
	}

	@Override
	public Class<ActivityInfoResponse> getResponseClass() {
		return ActivityInfoResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:123 
	*描述:活动ID 
	*/ 
	private Long activityId;
	/** 
	*是否必须:是 
	*示例值:1 
	*描述:1:满减，1206:满件减，1207：满件折，1208：捆绑，2210：每满减，1209套装 
	*/ 
	private Integer type;
	/** 
	*是否必须:否 
	*示例值:105 
	*描述:活动状态：(101待开始，102：进行中,103:结束,104:取消,全部：105) 
	*/ 
	private Integer state;
	/** 
	*是否必须:否 
	*示例值: 
	*描述:请求IP 
	*/ 
	private String ip;
	/** 
	*是否必须:是 
	*示例值:4128 
	*描述:请求追踪ID 
	*/ 
	private String traceId;
	/** 
	*是否必须:否 
	*示例值:1486365634198 
	*描述:长整型，单位毫秒 
	*/ 
	private Long requestTime;
	/** 
	*是否必须:是 
	*示例值:liyang 
	*描述:操作人/创建人 
	*/ 
	private String operator;

	public Long getActivityId() {
		return activityId;
	}

	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public Long getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Long requestTime) {
		this.requestTime = requestTime;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}
