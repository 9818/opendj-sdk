package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrgReplyCommentResponse;

/**
 * @author dengfengfeng
 * @date 2020-01-07
 */ 



public class OrgReplyCommentRequest implements OpenDjRequest<OrgReplyCommentResponse> {
	/** 
	*是否必须:是 
	*示例值:65265000023526 
	*描述:订单号 
	*/ 
	private String orderId ;
	/** 
	*是否必须:是 
	*示例值:10000001 
	*描述:到家门店编号 
	*/ 
	private String storeId ; 
	/** 
	*是否必须:是 
	*示例值: 
	*描述:回复内容 
	*/ 
	private String content ; 
	/** 
	*是否必须:是 
	*示例值:京东到家 
	*描述:回复人 
	*/ 
	private String replyPin ;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReplyPin() {
		return replyPin;
	}

	public void setReplyPin(String replyPin) {
		this.replyPin = replyPin;
	}

	@Override
	public OpenDjApi api() {
		return OpenDjApi.orgReplyComment;
	}

	@Override
	public Class<OrgReplyCommentResponse> getResponseClass() {
		return OrgReplyCommentResponse.class;
	}
}
