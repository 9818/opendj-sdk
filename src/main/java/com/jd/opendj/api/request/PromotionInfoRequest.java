package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.PromotionInfoResponse;

import java.util.Date;

/** 
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class PromotionInfoRequest implements OpenDjRequest<PromotionInfoResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.queryPromotionInfo;
	}

	@Override
	public Class<PromotionInfoResponse> getResponseClass() {
		return PromotionInfoResponse.class;
	}

	/**
	*是否必须:是
	*示例值:123
	*描述:活动编码
	*/
	private Long promotionInfoId;
	/**
	*是否必须:否
	*示例值:4128 
	*描述:服务号
	*/ 
	private String traceId;
	/**
	 *是否必须:否
	 *示例值:2018-01-01 00:00:00
	 *描述:时间戳
	 */
	private Date timeStamp;

	public Long getPromotionInfoId() {
		return promotionInfoId;
	}

	public void setPromotionInfoId(Long promotionInfoId) {
		this.promotionInfoId = promotionInfoId;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
}
