package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.CheckSelfPickCodeResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class CheckSelfPickCodeRequest implements OpenDjRequest<CheckSelfPickCodeResponse> {
    /**
     * 是否必须:是
     * 示例值:C3498SD
     * 描述:自提码
     */
    private String selfPickCode;

    public String getSelfPickCode() {
        return selfPickCode;
    }

    public void setSelfPickCode(String selfPickCode) {
        this.selfPickCode = selfPickCode;
    }

    /**
     * 是否必须:是
     * 示例值:914120722000242
     * 描述:订单编号
     */
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:jddj
     * 描述:操作人
     */
    private String operPin;

    public String getOperPin() {
        return operPin;
    }

    public void setOperPin(String operPin) {
        this.operPin = operPin;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.checkSelfPickCode;
    }

    @Override
    public Class<CheckSelfPickCodeResponse> getResponseClass() {
        return CheckSelfPickCodeResponse.class;
    }
} 
