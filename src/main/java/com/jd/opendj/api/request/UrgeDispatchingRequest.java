package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.UrgeDispatchingResponse;

/**
 * @author dff
 * @date 2020-09-16
 */
public class UrgeDispatchingRequest implements OpenDjRequest<UrgeDispatchingResponse> {
    /**
     * 是否必须:是
     * 示例值:914120724000242
     * 描述:订单编号
     */
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:jddj
     * 描述:操作人
     */
    private String updatePin;

    public String getUpdatePin() {
        return updatePin;
    }

    public void setUpdatePin(String updatePin) {
        this.updatePin = updatePin;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.urgeDispatching;
    }

    @Override
    public Class<UrgeDispatchingResponse> getResponseClass() {
        return UrgeDispatchingResponse.class;
    }
} 
