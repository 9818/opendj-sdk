package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.HandleReportResponse;

import java.util.List;

/**
 * @author dff
 * @date 2020-09-17
 */
public class HandleReportRequest implements OpenDjRequest<HandleReportResponse> {
    /**
     * 是否必须:是
     * 示例值:2001326756
     * 描述:订单号
     */
    private Long orderId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:
     * 描述:上传的图片url列表（多个图片，用英文逗号分隔）
     */
    private List<String> imgs;

    public List<String> getImgs() {
        return imgs;
    }

    public void setImgs(List<String> imgs) {
        this.imgs = imgs;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.handleReport;
    }

    @Override
    public Class<HandleReportResponse> getResponseClass() {
        return HandleReportResponse.class;
    }
} 
