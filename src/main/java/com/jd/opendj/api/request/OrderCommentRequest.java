package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrderCommentResponse;

/**
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class OrderCommentRequest implements OpenDjRequest<OrderCommentResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.getCommentByOrderId;
	}

	@Override
	public Class<OrderCommentResponse> getResponseClass() {
		return OrderCommentResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:634568900001 
	*描述:订单号 
	*/ 
	private Long orderId ; 
	/** 
	*是否必须:否 
	*示例值:10000001 
	*描述:到家门店编号 
	*/ 
	private String storeId ;

	public OrderCommentRequest() {
	}

	public OrderCommentRequest(Long orderId) {
		this.orderId = orderId;
	}

	public OrderCommentRequest(String orderId) {
		this.orderId = Long.valueOf(orderId);
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
}
