package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.AfsOpenApproveResponse;

/**
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class AfsOpenApproveRequest implements OpenDjRequest<AfsOpenApproveResponse> {
	/** 
	*是否必须:是 
	*示例值:4561234 
	*描述:售后服务单号 
	*/ 
	private String serviceOrder ; 
	/** 
	*是否必须:是 
	*示例值:1 
	*描述:审核结果类型（1：退款2：退货3：驳回） 
	*/ 
	private Integer approveType ; 
	/** 
	*是否必须:否 
	*示例值: 
	*描述:驳回原因(审核为驳回时必须，审核为退货或退款时不传) 
	*/ 
	private String rejectReason ; 
	/** 
	*是否必须:是 
	*示例值:test 
	*描述:操作人 
	*/ 
	private String optPin ;

	@Override
	public OpenDjApi api() {
		return OpenDjApi.afsOpenApprove;
	}

	@Override
	public Class<AfsOpenApproveResponse> getResponseClass() {
		return AfsOpenApproveResponse.class;
	}

	public String getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(String serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Integer getApproveType() {
		return approveType;
	}

	public void setApproveType(Integer approveType) {
		this.approveType = approveType;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getOptPin() {
		return optPin;
	}

	public void setOptPin(String optPin) {
		this.optPin = optPin;
	}
}
