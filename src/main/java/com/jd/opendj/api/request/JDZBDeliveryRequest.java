package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.JDZBDeliveryResponse;

/**
 * @author dengfengfeng
 * @date 2020-01-07
 */ 


public class JDZBDeliveryRequest implements OpenDjRequest<JDZBDeliveryResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.orderJDZBDelivery;
	}

	@Override
	public Class<JDZBDeliveryResponse> getResponseClass() {
		return JDZBDeliveryResponse.class;
	}

	/**
	*是否必须:是 
	*示例值:100001020895180 
	*描述:订单编码 
	*/ 
	private String orderId;
	/** 
	*是否必须:是 
	*示例值:张三 
	*描述:操作人 
	*/ 
	private String operator;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}
