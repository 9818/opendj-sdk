package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.SettleOrderListResponse;

/**
 * 分页查询结算单查询条件
 * @author dff
 * @date 2020-05-11
 */ 


public class SettleOrderListRequest implements OpenDjRequest<SettleOrderListResponse> {
	@Override
	public OpenDjApi api() {
		return OpenDjApi.getSettleOrderList;
	}

	@Override
	public Class<SettleOrderListResponse> getResponseClass() {
		return SettleOrderListResponse.class;
	}

	/**
	*是否必须:否 
	*示例值:100001 
	*描述:结算单id 
	*/ 
	private Long settleOrderId;
	/** 
	*是否必须:否 
	*示例值:5006 
	*描述:结算类型（货款5006）、市场费结算（5008） 
	*/ 
	private Integer settleType;
	/** 
	*是否必须:否 
	*示例值:10010 
	*描述:门店编号 
	*/ 
	private Long shopId;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:打款方式(1支付宝2银行卡3京东钱包) 
	*/ 
	private Integer accountType;
	/** 
	*是否必须:否 
	*示例值:20170707 
	*描述:账期开始时间:订单妥投时间的当天零时时间,诸如20170707这样的8位字符 
	*/ 
	private Long accountTimeStart;
	/** 
	*是否必须:否 
	*示例值:20170707 
	*描述:账期结束时间:订单妥投时间的当天零时时间,诸如20170707这样的8位字符 
	*/ 
	private Long accountTimeEnd;
	/** 
	*是否必须:否 
	*示例值:20170707 
	*描述:结算时间开始诸如20170707这样的8位字符 
	*/ 
	private Long finishTimeStart;
	/** 
	*是否必须:否 
	*示例值:20170707 
	*描述:结算完成开始诸如20170707这样的8位字符 
	*/ 
	private Long finishTimeEnd;
	/** 
	*是否必须:否 
	*示例值:20003 
	*描述:结算状态待打款(20002)、结算成功(20003)、结算失败(20004,20011)、驳回(20005)、打款失败(20009)、冻结中（20014）、部分结清（20015） 
	*/ 
	private Integer settleStatus;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:页码不传默认第一页 
	*/ 
	private Integer pageNum;
	/** 
	*是否必须:否 
	*示例值:10 
	*描述:每页条数，默认10 
	*/ 
	private Integer pageSize;

	public Long getSettleOrderId() {
		return settleOrderId;
	}

	public void setSettleOrderId(Long settleOrderId) {
		this.settleOrderId = settleOrderId;
	}

	public Integer getSettleType() {
		return settleType;
	}

	public void setSettleType(Integer settleType) {
		this.settleType = settleType;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getAccountType() {
		return accountType;
	}

	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}

	public Long getAccountTimeStart() {
		return accountTimeStart;
	}

	public void setAccountTimeStart(Long accountTimeStart) {
		this.accountTimeStart = accountTimeStart;
	}

	public Long getAccountTimeEnd() {
		return accountTimeEnd;
	}

	public void setAccountTimeEnd(Long accountTimeEnd) {
		this.accountTimeEnd = accountTimeEnd;
	}

	public Long getFinishTimeStart() {
		return finishTimeStart;
	}

	public void setFinishTimeStart(Long finishTimeStart) {
		this.finishTimeStart = finishTimeStart;
	}

	public Long getFinishTimeEnd() {
		return finishTimeEnd;
	}

	public void setFinishTimeEnd(Long finishTimeEnd) {
		this.finishTimeEnd = finishTimeEnd;
	}

	public Integer getSettleStatus() {
		return settleStatus;
	}

	public void setSettleStatus(Integer settleStatus) {
		this.settleStatus = settleStatus;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
