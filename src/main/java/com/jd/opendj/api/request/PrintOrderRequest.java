package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.PrintOrderResponse;

import static com.jd.opendj.api.OpenDjApi.printOrder;

/**
 * @author dff
 * @date 2020-09-16
 */
public class PrintOrderRequest implements OpenDjRequest<PrintOrderResponse> {

    /**
     * 是否必须:是
     * 示例值:2001326756
     * 描述:订单号
     */
    private Long orderId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public OpenDjApi api() {
        return printOrder;
    }

    @Override
    public Class<PrintOrderResponse> getResponseClass() {
        return PrintOrderResponse.class;
    }


} 
