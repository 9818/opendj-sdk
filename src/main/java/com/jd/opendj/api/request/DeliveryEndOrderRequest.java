package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.DeliveryEndOrderResponse;

import java.util.Date;

/**
 * @author dff
 * @date 2020-09-17
 */
public class DeliveryEndOrderRequest implements OpenDjRequest<DeliveryEndOrderResponse> {
    /**
     * 是否必须:是
     * 示例值:100001020895180
     * 描述:订单号
     */
    private Long orderId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:张三
     * 描述:操作人
     */
    private String operPin;

    public String getOperPin() {
        return operPin;
    }

    public void setOperPin(String operPin) {
        this.operPin = operPin;
    }

    /**
     * 是否必须:是
     * 示例值:2016-01-07 09:40:00
     * 描述:操作时间，日期格式的字符串
     */
    private Date operTime;

    public Date getOperTime() {
        return operTime;
    }

    public void setOperTime(Date operTime) {
        this.operTime = operTime;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.deliveryEndOrder;
    }

    @Override
    public Class<DeliveryEndOrderResponse> getResponseClass() {
        return DeliveryEndOrderResponse.class;
    }
} 
