package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.ConfirmReceiveGoodsResponse;

import java.util.Date;

/**
 * @author dff
 * @date 2020-09-16
 */
public class ConfirmReceiveGoodsRequest implements OpenDjRequest<ConfirmReceiveGoodsResponse> {
    /**
     * 是否必须:是
     * 示例值:2001326756
     * 描述:订单号
     */
    private Long orderId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:2016-01-1511:59:18
     * 描述:操作时间
     */
    private Date operateTime;

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.confirmReceiveGoods;
    }

    @Override
    public Class<ConfirmReceiveGoodsResponse> getResponseClass() {
        return ConfirmReceiveGoodsResponse.class;
    }
} 
