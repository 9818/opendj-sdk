package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.AfsServiceResponse;

/** 
 * @author dff
 * @date 2020-09-16
 */ 
public class AfsServiceRequest implements OpenDjRequest<AfsServiceResponse> {
	/** 
	*是否必须:是 
	*示例值:20043683 
	*描述:售后单号 
	*/ 
	private String afsServiceOrder;
	public String getAfsServiceOrder() { 
		return afsServiceOrder;
	}
	public void setAfsServiceOrder(String afsServiceOrder) {
		this.afsServiceOrder=afsServiceOrder;
	}

	@Override
	public OpenDjApi api() {
		return OpenDjApi.getAfsService;
	}

	@Override
	 public Class<AfsServiceResponse> getResponseClass() {
		return AfsServiceResponse.class;
	}
} 
