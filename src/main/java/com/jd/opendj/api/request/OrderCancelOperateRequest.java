package com.jd.opendj.api.request;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.OrderCancelOperateResponse;

/** 
 * @author dff
 * @date 2020-09-16
 */ 
public class OrderCancelOperateRequest  implements OpenDjRequest<OrderCancelOperateResponse> {
	/** 
	*是否必须:是 
	*示例值:100001016163464 
	*描述:订单号 
	*/ 
	private String orderId;
	/** 
	*是否必须:是 
	*示例值:true 
	*描述:操作类型true同意false驳回 
	*/ 
	private Boolean isAgreed;
	/** 
	*是否必须:是 
	*示例值:test 
	*描述:操作人 
	*/ 
	private String operator;
	/** 
	*是否必须:否 
	*示例值:取消备注 
	*描述:操作备注(isAgreed=false时此字段必填，isAgreed=true时可不填) 
	*/ 
	private String remark;

	@Override
	public String toJsonString() {
		final JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("orderId",orderId);
		jsonObject.addProperty("isAgreed",isAgreed);
		jsonObject.addProperty("operator",operator);
		jsonObject.addProperty("remark",remark);
		return new Gson().toJson(jsonObject);
	}

	@Override
	public OpenDjApi api() {
		return OpenDjApi.orderCancelOperate;
	}

	@Override
	public Class<OrderCancelOperateResponse> getResponseClass() {
		return OrderCancelOperateResponse.class;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Boolean getAgreed() {
		return isAgreed;
	}

	public void setAgreed(Boolean agreed) {
		isAgreed = agreed;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
