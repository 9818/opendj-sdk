package com.jd.opendj.api.request;

import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.AfsSubmitResponse;

import java.util.List;

/**
 * @author dff
 * @date 2020-09-17
 */
public class AfsSubmitRequest implements OpenDjRequest<AfsSubmitResponse> {
    /**
     * 是否必须:是
     * 示例值:1234567890
     * 描述:订单号
     */
    private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * 是否必须:是
     * 示例值:45378
     * 描述:操作人pin
     */
    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * 是否必须:是
     * 示例值:9863421
     * 描述:售后原因id（201:商品质量问题，202:送错货，203:缺件少件，501:全部商品未收到，208:包装脏污有破损，207:缺斤少两，210:商家通知我缺货，303:实物与原图不符，502:未在时效内送达）
     */
    private String questionTypeCode;

    public String getQuestionTypeCode() {
        return questionTypeCode;
    }

    public void setQuestionTypeCode(String questionTypeCode) {
        this.questionTypeCode = questionTypeCode;
    }

    /**
     * 是否必须:否
     * 示例值:测试描述
     * 描述:问题描述
     */
    private String questionDesc;

    public String getQuestionDesc() {
        return questionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        this.questionDesc = questionDesc;
    }

    /**
     * 是否必须:否
     * 示例值:
     * 描述:上传图片url逗号隔开
     */
    private String questionPic;

    public String getQuestionPic() {
        return questionPic;
    }

    public void setQuestionPic(String questionPic) {
        this.questionPic = questionPic;
    }

    /**
     * 是否必须:否
     * 示例值:测试名称
     * 描述:客户名称
     */
    private String customerName;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * 是否必须:否
     * 示例值:1234567
     * 描述:客户电话
     */
    private String customerMobilePhone;

    public String getCustomerMobilePhone() {
        return customerMobilePhone;
    }

    public void setCustomerMobilePhone(String customerMobilePhone) {
        this.customerMobilePhone = customerMobilePhone;
    }

    /**
     * 是否必须:否
     * 示例值:测试地址
     * 描述:客户详细地址
     */
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 是否必须:是
     * 示例值:
     * 描述:商品信息
     */
    private List<VenderAfsSkuDTO> skuList;

    public List<VenderAfsSkuDTO> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<VenderAfsSkuDTO> skuList) {
        this.skuList = skuList;
    }

    @Override
    public OpenDjApi api() {
        return OpenDjApi.afsSubmit;
    }

    @Override
    public Class<AfsSubmitResponse> getResponseClass() {
        return AfsSubmitResponse.class;
    }

    public static class VenderAfsSkuDTO {
        /**
         * 是否必须:是
         * 示例值:123456789
         * 描述:到家商品id
         */
        private Long skuId;

        public Long getSkuId() {
            return skuId;
        }

        public void setSkuId(Long skuId) {
            this.skuId = skuId;
        }

        /**
         * 是否必须:是
         * 示例值:5
         * 描述:商品数量
         */
        private Integer skuCount;

        public Integer getSkuCount() {
            return skuCount;
        }

        public void setSkuCount(Integer skuCount) {
            this.skuCount = skuCount;
        }

        /**
         * 是否必须:是
         * 示例值:1203
         * 描述:商品促销类型（1正品，2秒杀促销，3单品直降促销，4限时抢购促销，6买赠，7新人专享，8第二件N折，9拼团购，1202加价购促销，1203满赠促销，8001轻松购会员价，9996组合购,9997捆绑除不尽的类型，9998捆绑参与其他单品，9999捆绑，10009单品预售，9000称重品最小促销类型，9040称重品会员价起始类型，9050称重品最大促销类型）
         */
        private Integer promotionType;

        public Integer getPromotionType() {
            return promotionType;
        }

        public void setPromotionType(Integer promotionType) {
            this.promotionType = promotionType;
        }

    }
} 
