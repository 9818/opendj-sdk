package com.jd.opendj.api.request;


import com.jd.opendj.api.OpenDjApi;
import com.jd.opendj.api.OpenDjRequest;
import com.jd.opendj.api.response.BalanceBillListResponse;

import java.util.List;

/** 
 * @author dff
 * @date 2020-05-12
 */ 


public class BalanceBillListRequest implements OpenDjRequest<BalanceBillListResponse> {

	@Override
	public OpenDjApi api() {
		return OpenDjApi.getBalanceBillList;
	}

	@Override
	public Class<BalanceBillListResponse> getResponseClass() {
		return BalanceBillListResponse.class;
	}

	/**
	*是否必须:否 
	*示例值: 
	*描述:门店id 
	*/ 
	private List<Long> shopIds;
	/** 
	*是否必须:否 
	*示例值: 
	*描述:订单号 
	*/ 
	private List<String> orderIds;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:订单类型，不填写为全部，1:异常调整,1001:正向订单,1002:售后退货,1003:售后退款,1004:售后退货申诉,1005:售后退款申诉,1006:售后换新申诉,1016:售后换新,1017:售后直赔 
	*/ 
	private Integer orderType;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:订单状态1:已完成 
	*/ 
	private Integer orderStatus;
	/** 
	*是否必须:否 
	*示例值: 
	*描述:结算状态，待打款(20002)、结算成功(20003)、结算失败(20004,20011)、驳回(20005)、打款失败(20009)、冻结中(20014) 
	*/ 
	private Integer settleStatus;
	/** 
	*是否必须:否 
	*示例值:20170706 
	*描述:下单开始时间（20170706）与完成订单开始时间至少填写一个 
	*/ 
	private String orderStartTime;
	/** 
	*是否必须:否 
	*示例值:20170706 
	*描述:下单截止时间（20170706）与完成订单截止时间至少填写一个 
	*/ 
	private String orderEndTime;
	/** 
	*是否必须:否 
	*示例值:20170706 
	*描述:完成订单开始时间（20170706）与下单开始时间至少填写一个 
	*/ 
	private String finishStartTime;
	/** 
	*是否必须:否 
	*示例值:20170706 
	*描述:完成订单截止时间（20170706）与下单截止时间至少填写一个 
	*/ 
	private String finishEndTime;
	/** 
	*是否必须:否 
	*示例值:1 
	*描述:页码不传默认第一页 
	*/ 
	private Integer pageNum;
	/** 
	*是否必须:否 
	*示例值:10 
	*描述:每页条数，默认10 
	*/ 
	private Integer pageSize;
	/** 
	*是否必须:否 
	*示例值:true 
	*描述:是否查询京东融合的对账单，需要查询京东融合对账单时填true，不传默认false 
	*/ 
	private Boolean jdFusion;

	public List<Long> getShopIds() {
		return shopIds;
	}

	public void setShopIds(List<Long> shopIds) {
		this.shopIds = shopIds;
	}

	public List<String> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(List<String> orderIds) {
		this.orderIds = orderIds;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Integer getSettleStatus() {
		return settleStatus;
	}

	public void setSettleStatus(Integer settleStatus) {
		this.settleStatus = settleStatus;
	}

	public String getOrderStartTime() {
		return orderStartTime;
	}

	public void setOrderStartTime(String orderStartTime) {
		this.orderStartTime = orderStartTime;
	}

	public String getOrderEndTime() {
		return orderEndTime;
	}

	public void setOrderEndTime(String orderEndTime) {
		this.orderEndTime = orderEndTime;
	}

	public String getFinishStartTime() {
		return finishStartTime;
	}

	public void setFinishStartTime(String finishStartTime) {
		this.finishStartTime = finishStartTime;
	}

	public String getFinishEndTime() {
		return finishEndTime;
	}

	public void setFinishEndTime(String finishEndTime) {
		this.finishEndTime = finishEndTime;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Boolean getJdFusion() {
		return jdFusion;
	}

	public void setJdFusion(Boolean jdFusion) {
		this.jdFusion = jdFusion;
	}
}
