package com.jd.opendj.api;

import com.jd.opendj.api.exception.FailedRequestException;

/**
 * <p>OpendjClient</p>
 * description
 *
 * @author 邓峰峰
 * @date 9/2/2020 7:39 PM
 */
public interface OpenDjClient {
    /**
     * 执行公开API请求。
     *
     * @param <T> 具体的API响应类
     * @param request 具体的API请求类
     * @return 具体的API响应
     * @throws FailedRequestException 请求失败的异常
     */
    <T extends OpenDjResponse> T execute(OpenDjRequest<T> request) throws FailedRequestException;

}
