package com.jd.opendj.api.client;

import com.jd.opendj.api.OpenDjResponse;
import com.jd.opendj.api.client.feature.AbstractAutoRetryFeature;
import com.jd.opendj.api.exception.FailedRequestException;
import com.jd.opendj.api.OpenDjRequest;

import java.util.Set;

/**
 * <p>AutoRetryOpenDjClient</p>
 * 可以自动重试的京东到家客户端
 *
 * @author 邓峰峰
 * @date 9/8/2020 5:46 PM
 */
public class AutoRetryOpenDjClient extends DefaultOpenDjClient {

    /**
     * 默认的重试Feature
     */
    private AbstractAutoRetryFeature retryFeature = new AbstractAutoRetryFeature() {
        @Override
        public  <T extends OpenDjResponse> T clientExecute(OpenDjRequest<T> request) throws FailedRequestException {
            return AutoRetryOpenDjClient.super.execute(request);
        }
    };

    public AutoRetryOpenDjClient(String appKey, String appSecret, String token) {
        super(appKey, appSecret, token);
    }

    public AutoRetryOpenDjClient(String appKey, String appSecret, String token, AbstractAutoRetryFeature retryFeature) {
        super(appKey, appSecret, token);
        this.retryFeature = retryFeature;
    }

    public void setMaxRetryCount(int maxRetryCount) {
        retryFeature.setMaxRetryCount(maxRetryCount);
    }

    public void setRetryWaitTime(long retryWaitTime) {
        retryFeature.setRetryWaitTime(retryWaitTime);
    }

    public void setThrowIfOverMaxRetry(boolean throwIfOverMaxRetry) {
        retryFeature.setThrowIfOverMaxRetry(throwIfOverMaxRetry);
    }

    public void setRetryErrorCodes(Set<String> retryErrorCodes) {
        retryFeature.setRetryErrorCodes(retryErrorCodes);
    }

    public void setLimitCode(String limitCode) {
        retryFeature.setLimitCode(limitCode);
    }

    public void setLimitRetryWaitTime(long limitRetryWaitTime) {
        retryFeature.setLimitRetryWaitTime(limitRetryWaitTime);
    }


    @Override
    public <T extends OpenDjResponse> T execute(OpenDjRequest<T> request) throws FailedRequestException {
        return retryFeature.execute(request);
    }
}
