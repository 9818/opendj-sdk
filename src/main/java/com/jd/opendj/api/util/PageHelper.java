package com.jd.opendj.api.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页助手
 * @author 邓峰峰
 * @date 2019/11/15 9:35
 * <p></p>
 */
public class PageHelper {

    /**
     * 把一个集合拆分为多个小集合
     * @param collection 要被拆分的集合
     * @param pageSize 每个集合最多多少个元素
     * @param <T> 集合类型
     * @return
     */
    public static <T> List<List<T>> splitList(List<T> collection, int pageSize){
        if(pageSize<=0){
            throw new IllegalArgumentException("pageSize必须大于0");
        }
        final int size = collection.size();
        int x = size/pageSize+(size%pageSize==0?0:1);
        List<List<T>> result = new ArrayList<>();
        for (int i = 0; i < x; i++) {
            int fromIndex = i*pageSize;
            int toIndex = x-i==1?size:(i+1)*pageSize;
            result.add(collection.subList(fromIndex,toIndex));
        }
        return result;
    }
}
