package com.jd.opendj.api.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * HTTP工具类
 * ClassName HttpUtil </br>
 * 2015年4月27日 下午7:15:59 </br>
 * @author zhoudeming </br>
 * @version 1.0.0
 */
public class HttpUtil {

	private static HttpEntity buildEntity(Map<String, String> param) {
		HttpEntity entity = null;
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		if (param != null && param.size() > 0) {
			for (Map.Entry<String, String> entry : param.entrySet()) {
				if(entry.getValue()!=null){
					params.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
				}
			}
			entity = new UrlEncodedFormEntity(params, StandardCharsets.UTF_8);
		}
		return entity;
	}

	private static String executeHttpPost(String url,Map<String, String> param) throws Exception {
		HttpPost httpPost = new HttpPost();
		httpPost.setURI(new URI(url));
		httpPost.setEntity(buildEntity(param));
		return execute(httpPost);
	}

	private static String executeHttpGet(String uri) throws Exception {
		HttpGet httpGet = new HttpGet();
		httpGet.setURI(new URI(uri));
		return execute(httpGet);
	}

	private static String execute(HttpRequestBase req) throws Exception {
		String result;
		CloseableHttpResponse response;
		try {
			response = HttpClientPool.execute(req);
			result = EntityUtils.toString(response.getEntity());
		} finally {
			req.releaseConnection();
		}
		return result;
	}

//	private static String execute(HttpEntityEnclosingRequestBase req) throws Exception {
//		String result;
//		CloseableHttpResponse response;
//		try {
//			response = HttpClientPool.execute(req);
//			result = EntityUtils.toString(response.getEntity());
//		} finally {
//			req.releaseConnection();
//		}
//		return result;
//	}


	/**
	 * GET方式发送HTTP请求
	 * sendSimpleGetRequest </br>
	 * @param uri
	 * @return
	 * @throws Exception </br>
	 * String
	 */
	public static String sendSimpleGetRequest(String uri) {
		String result;
		try {
			result = executeHttpGet(uri);
		} catch (ConnectTimeoutException e) {
			result = "{\"code\":\"-1\", \"msg\":\"请求连接超时\"}";
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			result = "{\"code\":\"-1\", \"msg\":\"请求响应超时\"}";
			e.printStackTrace();
		} catch (IOException e) {
			result = "{\"code\":\"-1\", \"msg\":\"请求响应超时\"}";
			e.printStackTrace();
		} catch (Exception e) {
			result = "{\"code\":\"-1\", \"msg\":\"服务开小差，系统努力修复中\"}";
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * POST方式发送HTTP请求
	 * sendSimplePostRequest </br>
	 * @param uri
	 * @return
	 * @throws Exception </br>
	 * String
	 */
	public static String sendSimplePostRequest(String uri,Map<String, String> param) {
		String result;
		try {
			result = executeHttpPost(uri,param);
		} catch (ConnectTimeoutException e) {
			result = "{\"code\":\"-1\", \"msg\":\"请求连接超时\"}";
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			result = "{\"code\":\"-1\", \"msg\":\"请求响应超时\"}";
			e.printStackTrace();
		} catch (IOException e) {
			result = "{\"code\":\"-1\", \"msg\":\"请求响应超时\"}";
			e.printStackTrace();
		} catch (Exception e) {
			result = "{\"code\":\"-1\", \"msg\":\"服务开小差，系统努力修复中\"}";
			e.printStackTrace();
		}
		return result;
	}

}  