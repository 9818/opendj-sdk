import com.jd.opendj.api.client.AutoRetryOpenDjClient;
import com.jd.opendj.api.client.DefaultOpenDjClient;
import com.jd.opendj.api.request.*;
import com.jd.opendj.api.response.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;

/**
 * @author by DFF
 * @date 8/19/2020 12:20 AM
 */
public class SdkTest {


    private DefaultOpenDjClient createDefaultClient() {
        return new DefaultOpenDjClient("xxx", "xxx", "xxx");
    }
    private AutoRetryOpenDjClient createAutoRetryClient() {
        return new AutoRetryOpenDjClient("xxx", "xxx", "xxx");
    }


    @Test
    public void test_query_order_request(){
        final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
        final OrderInfoRequest orderInfoRequest = new OrderInfoRequest();
        orderInfoRequest.setOrderId(2021724106000531L);
        final OrderInfoResponse execute = defaultOpenDjClient.execute(orderInfoRequest);
        final OrderInfoResponse.OrderInfoListData orderInfoListData = execute.getOrderInfoListData();
        final OrderInfoResponse.Page page = orderInfoListData.extractData();
        System.out.println(page);
    }

    @Test
    public void test_100_times_query_order_request(){

        List<OrderInfoResponse.Page> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
            final OrderInfoRequest orderInfoRequest = new OrderInfoRequest();
            orderInfoRequest.setOrderId(2021724106000531L);
            final OrderInfoResponse execute = defaultOpenDjClient.execute(orderInfoRequest);
            final OrderInfoResponse.OrderInfoListData orderInfoListData = execute.getOrderInfoListData();
            final OrderInfoResponse.Page page = orderInfoListData.extractData();
            list.add(page);
        }
        Assert.assertEquals("xxx", 100, list.size());

    }

    @Test
    public void test_1000_thread_query_order_request() throws InterruptedException {
        Vector<OrderInfoResponse.Page> vector = new Vector<>();
        final int count = 1000;
        CountDownLatch latch = new CountDownLatch(count);
        Runnable runnable = ()->{
            final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
            final OrderInfoRequest orderInfoRequest = new OrderInfoRequest();
            orderInfoRequest.setOrderId(2021724106000531L);
            final OrderInfoResponse execute = defaultOpenDjClient.execute(orderInfoRequest);
            final OrderInfoResponse.OrderInfoDTO first = execute.findFirst();
            System.out.println(first);
            final OrderInfoResponse.OrderInfoListData orderInfoListData = execute.getOrderInfoListData();
            final OrderInfoResponse.Page page = orderInfoListData.extractData();
            vector.add(page);
            latch.countDown();
        };

        for (int i = 0; i < count; i++) {
            final Thread thread = new Thread(runnable);
            thread.start();
        }

        latch.await();
        Assert.assertEquals("xxx", count, vector.size());
    }

    @Test
    public void test_auto_retry_query_order_request(){
        final AutoRetryOpenDjClient autoRetryOpenDjClient = createAutoRetryClient();;
        final OrderInfoRequest orderInfoRequest = new OrderInfoRequest();
        orderInfoRequest.setOrderId(2021724106000531L);
        final OrderInfoResponse execute = autoRetryOpenDjClient.execute(orderInfoRequest);
        final OrderInfoResponse.OrderInfoListData orderInfoListData = execute.getOrderInfoListData();
        final OrderInfoResponse.Page page = orderInfoListData.extractData();
        System.out.println(page);
    }

    @Test
    public void test_accept_order_request(){
        final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
        final AcceptOperateRequest acceptOperateRequest = new AcceptOperateRequest();
        acceptOperateRequest.setOrderId("2021724106000531");
        acceptOperateRequest.setAgreed(true);
        acceptOperateRequest.setOperator("test sdk");
        final AcceptOperateResponse execute = defaultOpenDjClient.execute(acceptOperateRequest);
        System.out.println(execute.getMsg());
        System.out.println(execute);
    }

    @Test
    public void test_activity_request(){
        final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
        final ActivityInfoRequest activityInfoRequest = new ActivityInfoRequest();
        activityInfoRequest.setType(1);
        activityInfoRequest.setActivityId(1234L);
        activityInfoRequest.setOperator("test sdk");
        activityInfoRequest.setTraceId("test");
        final ActivityInfoResponse execute = defaultOpenDjClient.execute(activityInfoRequest);
        System.out.println(execute.getMsg());
        final ActivityInfoResponse.OpenPlatActivityQResponseData openPlatActivityQResponse = execute.getOpenPlatActivityQResponseData();
        System.out.println(openPlatActivityQResponse);
        System.out.println(execute);
    }

    @Test
    public void test_adjust_order_request(){
        final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
        final AdjustOrderRequest adjustOrderRequest = new AdjustOrderRequest();

        adjustOrderRequest.setOrderId(2021724106000531L);
        adjustOrderRequest.setOperPin("pin sdk");
        adjustOrderRequest.setRemark("sdk test");

        List<AdjustOrderRequest.OAOSAdjustDTO> oaosAdjustDTOList = new ArrayList<>();
        AdjustOrderRequest.OAOSAdjustDTO oaosAdjustDTO = new AdjustOrderRequest.OAOSAdjustDTO();
        oaosAdjustDTO.setSkuCount(1);
        oaosAdjustDTOList.add(oaosAdjustDTO);
        adjustOrderRequest.setOaosAdjustDTOList(oaosAdjustDTOList);

        final AdjustOrderResponse execute = defaultOpenDjClient.execute(adjustOrderRequest);

        System.out.println(execute);
    }

    @Test
    public void test_afs_open_approve_request(){
        final DefaultOpenDjClient defaultOpenDjClient = createDefaultClient();;
        final AfsOpenApproveRequest adjustOrderRequest = new AfsOpenApproveRequest();

        adjustOrderRequest.setServiceOrder("123");
        adjustOrderRequest.setApproveType(1);
        adjustOrderRequest.setOptPin("123");

        final AfsOpenApproveResponse execute = defaultOpenDjClient.execute(adjustOrderRequest);

        System.out.println(execute);
    }

    @Test
    public void test_ActivityInfoRequest(){
        final DefaultOpenDjClient client = createDefaultClient();
        ActivityInfoRequest request = new ActivityInfoRequest();
        request.setType(1);
        request.setTraceId("1");
        request.setOperator("tester");
        request.setIp("111.111.111.111");
        final ActivityInfoResponse execute = client.execute(request);
        System.out.println(execute);
    }

}
